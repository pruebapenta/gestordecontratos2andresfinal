﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestorDeContratos.ServiceLayer;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Conversiones conv = new Conversiones();
            
            string num;

            Console.WriteLine();
            num = "1234,01"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "0,02"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "0,05"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "0,09"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "0,10"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "13,8"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "13,08"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "0,10"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "0,01"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "123,00"; Console.WriteLine(num + " => " + conv.enletras(num));

            Console.WriteLine();
            num = "13.292.130"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.000"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.001"; Console.WriteLine(num + " => " + conv.enletras(num));

            Console.WriteLine();
            num = "1";Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.000"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.000.000"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.000.000.000"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.000.000.000.000"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.234.567.890.123.456"; Console.WriteLine(num + " => " + conv.enletras(num));

            Console.WriteLine();
            num = "1.000.000,00"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.000,01"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.000,10"; Console.WriteLine(num + " => " + conv.enletras(num));
            num = "1.000.000,81"; Console.WriteLine(num + " => " + conv.enletras(num));

            Console.WriteLine();
            Random random = new Random();
            double randomNumber;
            for (int x = 1; x < 10; x++)
            {
                randomNumber = random.NextDouble() * (1000000 - 1) + 1;
                randomNumber = Math.Round(randomNumber);
                num = randomNumber.ToString(); Console.WriteLine(string.Format("{0:N0}", decimal.Parse(num)) + " => " + conv.enletras(num));
            }
            for (int x = 1; x < 10; x++)
            {
                randomNumber = random.NextDouble() * (100000000-1)+1;
                randomNumber = Math.Round(randomNumber);
                num = randomNumber.ToString(); Console.WriteLine( string.Format("{0:N0}", decimal.Parse(num)) + " => " + conv.enletras(num));
            }
            for (int x = 1; x < 10; x++)
            {
                randomNumber = random.NextDouble() * (100000000000 - 1) + 1;
                randomNumber = Math.Round(randomNumber);
                num = randomNumber.ToString(); Console.WriteLine(string.Format("{0:N0}", decimal.Parse(num)) + " => " + conv.enletras(num));
            }

            Console.ReadKey();
        }
    }
}
