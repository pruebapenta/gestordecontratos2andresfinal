﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestorDeContratos.DAL.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=srvapltest;Initial Catalog=BD_GESTORDECONTRATOS;User ID=usr_prueba;Pa" +
            "ssword=123456")]
        public string BD_GESTORDECONTRATOSConnectionString {
            get {
                return ((string)(this["BD_GESTORDECONTRATOSConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=srvapltest;Initial Catalog=BD_GESTORDECONTRATOS;User ID=usr_prueba")]
        public string BD_GESTORDECONTRATOSConnectionString1 {
            get {
                return ((string)(this["BD_GESTORDECONTRATOSConnectionString1"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SRVAPLTEST;Initial Catalog=BD_GESTORDECONTRATOS;User ID=usr_prueba;Pa" +
            "ssword=123456")]
        public string BD_GESTORDECONTRATOSConnectionString2 {
            get {
                return ((string)(this["BD_GESTORDECONTRATOSConnectionString2"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SRVAPLPROD;Initial Catalog=BD_GESTORDECONTRATOS;User ID=usr_web;Passw" +
            "ord=Pent@Financiero")]
        public string BD_GESTORDECONTRATOSConnectionString3 {
            get {
                return ((string)(this["BD_GESTORDECONTRATOSConnectionString3"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SrvBBDD;Initial Catalog=BD_GESTORDECONTRATOS;User ID=sa;Password=P3nt" +
            "a2019")]
        public string BD_GESTORDECONTRATOSConnectionString4 {
            get {
                return ((string)(this["BD_GESTORDECONTRATOSConnectionString4"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SRVBBDD;Initial Catalog=BD_GESTORDECONTRATOS;User ID=usr_appweb;Passw" +
            "ord=*W3bServ#PF")]
        public string BD_GESTORDECONTRATOSConnectionString5 {
            get {
                return ((string)(this["BD_GESTORDECONTRATOSConnectionString5"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=SrvBBDD;Initial Catalog=BD_GESTORDECONTRATOS;Integrated Security=True" +
            "")]
        public string BD_GESTORDECONTRATOSConnectionString6 {
            get {
                return ((string)(this["BD_GESTORDECONTRATOSConnectionString6"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=172.16.32.193;Initial Catalog=BD_GESTORDECONTRATOS;Persist Security I" +
            "nfo=True;User ID=usr_prueba;Password=Pf123456")]
        public string BD_GESTORDECONTRATOSConnectionString7 {
            get {
                return ((string)(this["BD_GESTORDECONTRATOSConnectionString7"]));
            }
        }
    }
}
