﻿using GestorDeContratos.DAL.Core.IDataService;
using GestorDeContratos.DAL.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.DAL
{
    public class DataAccessGestorDeContratos : IGestorDeContratosDataService
    {
        private readonly GestorDeContratosDataContext _dataContext;

        public DataAccessGestorDeContratos()
        {
            string connString = ConfigurationManager.AppSettings["connStringBDGestorDeContratos"];
            _dataContext = new GestorDeContratosDataContext(connString);
        }

        public IEnumerable<usp_Cliente_GetListResult> obtenerClientes()
        {
            return _dataContext.usp_Cliente_GetList();
        }

        public IEnumerable<usp_BancoCliente_GetDetailResult> obtenerBancoCliente(int rutCliente)
        {
            return _dataContext.usp_BancoCliente_GetDetail(rutCliente);
        }

        public IEnumerable<usp_Cliente_GetDetailResult> obtenerCliente(int rut)
        {
            return _dataContext.usp_Cliente_GetDetail(rut);
        }

        public IEnumerable<usp_Sucursal_GetListResult> obtenerSucursales()
        {
            return _dataContext.usp_Sucursal_GetList();
        }

        public IEnumerable<usp_Sucursal_GetDetailResult> obtenerSucursal(int idSucursal)
        {
            return _dataContext.usp_Sucursal_GetDetail(idSucursal);
        }

        public IEnumerable<usp_Notaria_GetListResult> obtenerNotarias()
        {
            return _dataContext.usp_Notaria_GetList();
        }

        public IEnumerable<usp_Notaria_GetDetailResult> obtenerNotaria(int idNotario)
        {
            return _dataContext.usp_Notaria_GetDetail(idNotario);
        }

        public IEnumerable<usp_EstadoCivil_GetListResult> obtenerEstadosCiviles()
        {
            return _dataContext.usp_EstadoCivil_GetList();
        }

        public IEnumerable<usp_RepresentanteCliente_GetByResult> obtenerRepresentantesCliente(int rutCliente)
        {
            return _dataContext.usp_RepresentanteCliente_GetBy(rutCliente);
        }

        public IEnumerable<usp_RepresentanteCliente_GetDetailResult> obtenerRepresentanteCliente(int rutCliente, int? rutRepresentante)
        {
            return _dataContext.usp_RepresentanteCliente_GetDetail(rutCliente, rutRepresentante);
        }

        public IEnumerable<usp_RepresentantePenta_GetDetailResult> obtenerRepresentantePenta(int rutRepresentantePenta)
        {
            return _dataContext.usp_RepresentantePenta_GetDetail(rutRepresentantePenta);
        }

        public IEnumerable<usp_RepresentantePenta_GetListResult> obtenerRepresentantesPenta()
        {
            return _dataContext.usp_RepresentantePenta_GetList();
        }

        //public IEnumerable<usp_TipoDocumento_GetByResult> obtenerTiposDocumentos(int? id)
        //{
        //    return _dataContext.usp_TipoDocumento_GetBy(id);
        //}

        public IEnumerable<usp_TextoCanal_GetListResult> obtenerTextoCanalList()
        {
            return _dataContext.usp_TextoCanal_GetList();
        }

        public IEnumerable<usp_TextoCanalVersion_GetDetailResult> obtenerTextoCanalVersionDetail(int idTexto)
        {
            return _dataContext.usp_TextoCanalVersion_GetDetail(idTexto);
        }

        public IEnumerable<usp_TextoCanalVersion_GetListResult> obtenerTextoCanalVersionList(int idTexto)
        {
            return _dataContext.usp_TextoCanalVersion_GetList(idTexto);
        }

        public void actualizarCliente(int rut, char dv, string rutPalabra, string razonSocial, string nombreCalle, string numeroCalle, string numeroCallePalabra, string letraCalle, string comuna,
            string ciudad, string autorizaCartaPoder, string email1, string email2, DateTime? fechaFirmaContrato, string repertorio, int idNotaria, string autorizaFirmarNombre, string autorizaFirmarRut,
            bool esPersonaNatural, string nacionalidadPersonaNatural, string profesionPersonaNatural, string telefonoPersonaNatural, int? idEstadoCivilPersonaNatural, int? rutConyuge, char? dvConyuge,
            string nombreConyuge, string nacionalidadConyuge, int idSucursal)
        {
            _dataContext.usp_Cliente_Update(rut, dv, rutPalabra, razonSocial, nombreCalle, numeroCalle, numeroCallePalabra, letraCalle, comuna, ciudad, autorizaCartaPoder, email1, email2,
                fechaFirmaContrato, repertorio, idNotaria, autorizaFirmarNombre, autorizaFirmarRut, esPersonaNatural, nacionalidadPersonaNatural, profesionPersonaNatural, telefonoPersonaNatural, idEstadoCivilPersonaNatural, rutConyuge, 
                dvConyuge, nombreConyuge, nacionalidadConyuge, idSucursal);
        }

        public void agregarRepresentanteCliente(int rutCliente, int rutRepresentante, string dvRepresentante, string nombre, string nacionalidad, int idEstadoCivil, 
            string profesion, string nombreCalle, string numeroCalle, string letraCalle, string comuna, string ciudad, string email, bool fiador, DateTime? fechaCtoFact, string notariaPersoneria, 
            DateTime? fechaPersoneria, string telefono, int? rutConyuge, string dvConyuge, string nombreConyuge, string nacionalidadConyuge, string profesionConyuge)
        {
            _dataContext.usp_RepresentanteCliente_Add(rutCliente, rutRepresentante, dvRepresentante, nombre, nacionalidad, idEstadoCivil, profesion, nombreCalle, numeroCalle, letraCalle, comuna, ciudad, email, fiador, fechaCtoFact, notariaPersoneria, fechaPersoneria, telefono, rutConyuge, dvConyuge, nombreConyuge, nacionalidadConyuge, profesionConyuge);
        }

        public void actualizarRepresentanteCliente(int rutCliente, int rutRepresentante, string dvRepresentante, string nombre, string nacionalidad, int idEstadoCivil, 
            string profesion, string nombreCalle, string numeroCalle, string letraCalle, string comuna, string ciudad, string email, bool fiador, DateTime? fechaCtoFact, string notariaPersoneria, 
            DateTime? fechaPersoneria, string telefono, int? rutConyuge, string dvConyuge, string nombreConyuge, string nacionalidadConyuge, string profesionConyuge)
        {
            _dataContext.usp_RepresentanteCliente_Update(rutCliente, rutRepresentante, dvRepresentante, nombre, nacionalidad, idEstadoCivil, profesion, nombreCalle, numeroCalle, letraCalle, comuna, ciudad, email, fiador, fechaCtoFact, notariaPersoneria, fechaPersoneria, telefono, rutConyuge, dvConyuge, nombreConyuge, nacionalidadConyuge, profesionConyuge);
        }

        public void actualizarSucursal(int idSucursal, string nombre, string direccion, string direccionPalabras, string comuna, string ciudad, bool vigente, int idNotaria, int rutRepresentantePenta1, int? rutRepresentantePenta2)
        {
            _dataContext.usp_Sucursal_Update(idSucursal, nombre, direccion, direccionPalabras, comuna, ciudad, vigente, idNotaria, rutRepresentantePenta1, rutRepresentantePenta2);
        }

        public void actualizarRepresentantePenta(int idRepresentantePenta, int rut, string dv, string nombres, string apPaterno, string apMaterno, string estadoCivil, string profesion, string nacionalidad, bool vigente)
        {
            _dataContext.usp_RepresentantePenta_Update(idRepresentantePenta, rut, dv, nombres, apPaterno, apMaterno, estadoCivil, profesion, nacionalidad, vigente);
        }

        public void agregarSucursal(string nombre, string direccion, string direccionPalabras, string comuna, string ciudad, bool vigente, int idNotaria, int rutRepresentantePenta1, int? rutRepresentantePenta2)
        {
            _dataContext.usp_Sucursal_Add(nombre, direccion, direccionPalabras, comuna, ciudad, vigente, idNotaria, rutRepresentantePenta1, rutRepresentantePenta2);
        }

        public void agregarRepresentantePenta(int rut, string dv, string nombres, string apPaterno, string apMaterno, string estadoCivil, string profesion, string nacionalidad, bool vigente)
        {
            _dataContext.usp_RepresentantePenta_Add(rut, dv, nombres, apPaterno, apMaterno, estadoCivil, profesion, nacionalidad, vigente);
        }

        public void agregarTextoCanalVersion(int idTexto, string texto)
        {
            _dataContext.usp_TextoCanalVersion_Update(idTexto, texto);
        }

        public IEnumerable<usp_ContratoEnviado_AddResult> agregarContratoEnviado(int idSimulaOper, int idOperacionTipo, int idContratoTipo, string usuario, int rutCliente, string contratoBase64, int paginasContrato, DateTime fechaOperacion, string nombreCliente, int cantidadDocumentos, decimal montoDocumentos, decimal montoFinanciado)
        {
            return _dataContext.usp_ContratoEnviado_Add(idSimulaOper, idOperacionTipo, idContratoTipo, usuario, rutCliente, contratoBase64, paginasContrato, fechaOperacion, nombreCliente, cantidadDocumentos, montoDocumentos, montoFinanciado);
        }

        public void agregarContratoEnviadoFirma(int idContratoEnviado, int rutFirmante, int idPersonaTipo)
        {
            _dataContext.usp_ContratoEnviadoFirma_Add(idContratoEnviado, rutFirmante, idPersonaTipo);
        }

        public void eliminarRepresentanteCliente(int rutCliente, int rutRepresentante)
        {
            _dataContext.usp_RepresentanteCliente_Delete(rutCliente, rutRepresentante);
        }

        public void eliminarSucursal(int idSucursal)
        {
            _dataContext.usp_Sucursal_Delete(idSucursal);
        }

        public void eliminarRepresentantePenta(int idRepresentantePenta)
        {
            try
            {
                _dataContext.usp_RepresentantePenta_Delete(idRepresentantePenta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void eliminarContratoPorFirmar(int idContratoEnviado)
        {
            _dataContext.usp_ContratosPorFirmar_Delete(idContratoEnviado);
        }


        public IEnumerable<usp_Operacion_GetResumenResult> obtenerOperacionResumen(int idSimulaOperacion)
        {
            return _dataContext.usp_Operacion_GetResumen(idSimulaOperacion);
        }

        public IEnumerable<usp_Operacion_GetDetailResult> obtenerOperacionDetalle(int idSimulaOperacion)
        {
            return _dataContext.usp_Operacion_GetDetail(idSimulaOperacion);
        }

        public IEnumerable<usp_Operacion_GetListResult> obtenerOperaciones(int tipoOperacion, int rutCliente)
        {
            return _dataContext.usp_Operacion_GetList(tipoOperacion, rutCliente);
        }

        public IEnumerable<usp_LogContrato_GetListResult> obtenerLogContratos()
        {
            return _dataContext.usp_LogContrato_GetList();
        }
        public IEnumerable<usp_LogContrato_GetByResult> obtenerLogContrato(int rutCliente)
        {
            return _dataContext.usp_LogContrato_GetBy(rutCliente);
        }

        public IEnumerable<usp_ContratoEnviado_DetailResult> obtenerContratoEnviadoDetalle(int idContratoEnviado)
        {
            return _dataContext.usp_ContratoEnviado_Detail(idContratoEnviado);
        }

        public IEnumerable<usp_ContratosFEGeneradosResult> obtenerContratosFEGenerados(int rutCliente)
        {
            return _dataContext.usp_ContratosFEGenerados(rutCliente);
        }

        public IEnumerable<usp_ContratosFirmadosResult> obtenerContratosFirmados()
        {
            return _dataContext.usp_ContratosFirmados();
        }

        public IEnumerable<usp_ContratosPorFirmarResult> obtenerContratosPorFirmar()
        {
            return _dataContext.usp_ContratosPorFirmar();
        }

        public IEnumerable<usp_ContratosPorFirmar_FirmantesResult> obtenerContratosPorFirmar_Firmantes(int idContratoEnviado)
        {
            return _dataContext.usp_ContratosPorFirmar_Firmantes(idContratoEnviado);
        }

        public void agregarLogContrato(string usuario, int rutCliente, string contratoGenerado)
        {
            _dataContext.usp_LogContrato_Add(usuario, rutCliente, contratoGenerado);
        }

        public IEnumerable<usp_OperacionCursada_GetListResult> obtenerOperacionesCursadas(int rutCliente)
        {
            return _dataContext.usp_OperacionCursada_GetList(rutCliente);
        }

        public IEnumerable<usp_ParrafosVariables_GetResult> obtenerParrafosVariables(int idContrato, int idTipoVariable, string tagParrafo)
        {
            return _dataContext.usp_ParrafosVariables_Get(idContrato, idTipoVariable, tagParrafo);
        }


        public IEnumerable<usp_DatosOperacion_GetResult> ObtenerDatosOperacion(int idSimulaOper)
        {
            return _dataContext.usp_DatosOperacion_Get(idSimulaOper);
        }


        public IEnumerable<usp_RelacionInstanciaSimulacionWS_ADDResult> InsertaRelacionInstaciaOperacion(int idInstancia, int idSimulaOper, int idTipoContratoGestor, int idTipoContratoWS, int rutCliente)
        {
            return _dataContext.usp_RelacionInstanciaSimulacionWS_ADD(idInstancia, idSimulaOper, idTipoContratoGestor, idTipoContratoWS, rutCliente);
        }



        public IEnumerable<usp_ContratoEnviado_UpdateResult> MarcarContratoFirmado(int idInstancia)
        {
            return _dataContext.usp_ContratoEnviado_Update(idInstancia);
        }


    }
}
