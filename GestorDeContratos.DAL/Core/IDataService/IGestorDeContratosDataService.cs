﻿using GestorDeContratos.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.DAL.Core.IDataService
{
    interface IGestorDeContratosDataService
    {
        IEnumerable<usp_Cliente_GetListResult> obtenerClientes();
        
        //IEnumerable<usp_Cliente_GetByResult> obtenerCliente_GetBy(int rut, string nombre);

        IEnumerable<usp_BancoCliente_GetDetailResult> obtenerBancoCliente(int rutCliente);

        IEnumerable<usp_Cliente_GetDetailResult> obtenerCliente(int rut);

        IEnumerable<usp_Sucursal_GetListResult> obtenerSucursales();

        IEnumerable<usp_Sucursal_GetDetailResult> obtenerSucursal(int idSucursal);

        IEnumerable<usp_Notaria_GetListResult> obtenerNotarias();

        IEnumerable<usp_Notaria_GetDetailResult> obtenerNotaria(int idNotario);

        IEnumerable<usp_EstadoCivil_GetListResult> obtenerEstadosCiviles();

        IEnumerable<usp_RepresentanteCliente_GetByResult> obtenerRepresentantesCliente(int rutCliente);

        IEnumerable<usp_RepresentanteCliente_GetDetailResult> obtenerRepresentanteCliente(int rutCliente, int? rutRepresentante);

        IEnumerable<usp_RepresentantePenta_GetDetailResult> obtenerRepresentantePenta(int rutRepresentantePenta);

        IEnumerable<usp_RepresentantePenta_GetListResult> obtenerRepresentantesPenta();

        IEnumerable<usp_TextoCanal_GetListResult> obtenerTextoCanalList();

        IEnumerable<usp_TextoCanalVersion_GetDetailResult> obtenerTextoCanalVersionDetail(int idTexto);

        IEnumerable<usp_TextoCanalVersion_GetListResult> obtenerTextoCanalVersionList(int idTexto);

        //IEnumerable<usp_TipoDocumento_GetByResult> obtenerTiposDocumentos(int? id);

        void actualizarCliente(int rut, char dv, string rutPalabra, string razonSocial, string nombreCalle, string numeroCalle, string numeroCallePalabra, string letraCalle, 
            string comuna, string ciudad, string autorizaCartaPoder, string email1, string email2, DateTime? fechaFirmaContrato, string repertorio, int idNotaria, string autorizaFirmarNombre, 
            string autorizaFirmarRut, bool esPersonaNatural, string nacionalidadPersonaNatural, string profesionPersonaNatural, string telefonoPersonaNatural, int? idEstadoCivilPersonaNatural, 
            int? rutConyuge, char? dvConyuge, string nombreConyuge, string nacionalidadConyuge, int idSucursal);

        void agregarRepresentanteCliente(int rutCliente, int rutRepresentante, string dvRepresentante, string nombre, string nacionalidad, int idEstadoCivil,
            string profesion, string nombreCalle, string numeroCalle, string letraCalle, string comuna, string ciudad, string email, bool fiador, DateTime? fechaCtoFact,
            string notariaPersoneria, DateTime? fechaPersoneria, string telefono, int? rutConyuge, string dvConyuge, string nombreConyuge, string nacionalidadConyuge, 
            string profesionConyuge);

        void actualizarRepresentanteCliente(int rutCliente, int rutRepresentante, string dvRepresentante, string nombre, string nacionalidad, int idEstadoCivil,
            string profesion, string nombreCalle, string numeroCalle, string letraCalle, string comuna, string ciudad, string email, bool fiador, DateTime? fechaCtoFact,
            string notariaPersoneria, DateTime? fechaPersoneria, string telefono, int? rutConyuge, string dvConyuge, string nombreConyuge, string nacionalidadConyuge,
            string profesionConyuge);

        void actualizarSucursal(int idSucursal, string nombre, string direccion, string direccionPalabras, string comuna, string ciudad, bool vigente, int idNotaria, int rutRepresentantePenta1, 
            int? rutRepresentantePenta2);

        void actualizarRepresentantePenta(int idRepresentantePenta, int rut, string dv, string nombres, string apPaterno, string apMaterno, string estadoCivil, string profesion,
            string nacionalidad, bool vigente);

        void eliminarRepresentanteCliente(int rutCliente, int rutRepresentante);

        void eliminarSucursal(int idSucursal);

        void eliminarRepresentantePenta(int idRepresentantePenta);

        void eliminarContratoPorFirmar(int idContratoEnviado);

        IEnumerable<usp_Operacion_GetResumenResult> obtenerOperacionResumen(int idSimulaOperacion);

        IEnumerable<usp_Operacion_GetDetailResult> obtenerOperacionDetalle(int idSimulaOperacion);

        IEnumerable<usp_Operacion_GetListResult> obtenerOperaciones(int tipoOperacion, int rutCliente);

        IEnumerable<usp_LogContrato_GetListResult> obtenerLogContratos();

        IEnumerable<usp_LogContrato_GetByResult> obtenerLogContrato(int rutCliente);

        IEnumerable<usp_ContratoEnviado_DetailResult> obtenerContratoEnviadoDetalle(int idContratoEnviado);

        IEnumerable<usp_ContratosFEGeneradosResult> obtenerContratosFEGenerados(int rutCliente);
        
        IEnumerable<usp_ContratosFirmadosResult> obtenerContratosFirmados();
        
        IEnumerable<usp_ContratosPorFirmarResult> obtenerContratosPorFirmar();

        IEnumerable<usp_ContratosPorFirmar_FirmantesResult> obtenerContratosPorFirmar_Firmantes(int idContratoEnviado);

        IEnumerable<usp_OperacionCursada_GetListResult> obtenerOperacionesCursadas(int rutCliente);

        void agregarLogContrato(string usuario, int rutCliente, string contratoGenerado);

        void agregarSucursal(string nombre, string direccion, string direccionPalabras, string comuna, string ciudad, bool vigente, int idNotaria, int rutRepresentantePenta1,
            int? rutRepresentantePenta2);

        void agregarRepresentantePenta(int rut, string dv, string nombres, string apPaterno, string apMaterno, string estadoCivil, string profesion, string nacionalidad, bool vigente);

        void agregarTextoCanalVersion(int idTexto, string texto);

        IEnumerable<usp_ContratoEnviado_AddResult> agregarContratoEnviado(int idSimulaOper, int idOperacionTipo, int idContratoTipo, string usuario, int rutCliente, string contratoBase64, int paginasContrato, DateTime fechaOperacion, string nombreCliente, int cantidadDocumentos, decimal montoDocumentos, decimal montoFinanciado);

        void agregarContratoEnviadoFirma(int idContratoEnviado, int rutFirmante, int idPersonaTipo);

        IEnumerable<usp_DatosOperacion_GetResult> ObtenerDatosOperacion(int idSimulaOper);

        IEnumerable<usp_RelacionInstanciaSimulacionWS_ADDResult> InsertaRelacionInstaciaOperacion(int idInstancia, int idSimulaOper, int idTipoContratoGestor, int idTipoContratoWS, int rutCliente);

        IEnumerable<usp_ContratoEnviado_UpdateResult> MarcarContratoFirmado(int idInstancia);
    }
}
