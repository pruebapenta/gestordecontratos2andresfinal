﻿using GestorDeContratos.ServiceLayer.ViewsDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.Service
{
    public interface IGestorDeContratosSvcImpl
    {
        /// <summary>
        ///
        /// </summary>
        IEnumerable<BancoClienteDTO> obtenerBancoCliente(int rutCliente);

        /// <summary>
        /// obtener Clientes de PF.
        /// </summary>
        IEnumerable<ClienteDTO> obtenerClientes(string buscar);
        
        /// <summary>
        /// obtener Cliente
        /// </summary>
        IEnumerable<ClienteDTO> obtenerCliente(int rut);

        /// <summary>
        /// obtener Sucursales
        /// </summary>
        IEnumerable<SucursalDTO> obtenerSucursales();

        /// <summary>
        /// obtener detalles de Sucursal
        /// </summary>
        IEnumerable<SucursalDTO> obtenerSucursal(int idSucursal);

        /// <summary>
        /// obtener Notarios
        /// </summary>
        IEnumerable<NotariaDTO> obtenerNotarias();

        /// <summary>
        /// obtener detalles del Notario
        /// </summary>
        IEnumerable<NotariaDTO> obtenerNotaria(int idNotario);

        /// <summary>
        /// obtener Sucursales
        /// </summary>
        IEnumerable<EstadoCivilDTO> obtenerEstadosCiviles();

        /// <summary>
        /// obtener Contratos
        /// </summary>
        //IEnumerable<TipoDocumentoDTO> obtenerTiposDocumentos(int? id);

        /// <summary>
        /// obtener Representantes del Cliente
        /// </summary>
        IEnumerable<RepresentanteClienteDTO> obtenerRepresentantesCliente(int rutCliente);

        /// <summary>
        /// obtener el representante de un cliente especifico
        /// </summary>
        IEnumerable<RepresentanteClienteDTO> obtenerRepresentanteCliente(int rutCliente, int? rutRepresentante);

        /// <summary>
        /// obtener el representante de Penta
        /// </summary>
        IEnumerable<RepresentantePentaDTO> obtenerRepresentantePenta(int rutRepresentantePenta);

        /// <summary>
        /// obtener los representantes de Penta
        /// </summary>
        IEnumerable<RepresentantePentaDTO> obtenerRepresentantesPenta();

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<TextoCanalDTO> obtenerTextoCanalList();

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<TextoCanalVersionDTO> obtenerTextoCanalVersionDetalle(int idTexto);

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<TextoCanalVersionDTO> obtenerTextoCanalVersionList(int idTexto);

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<ContratoFEGeneradoDTO> obtenerContratosFEGenerados(int rutCliente);

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<ContratoFirmadoDTO> obtenerContratosFirmados();

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<ContratoPorFirmarDTO> obtenerContratosPorFirmar();

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<ContratoEnviadoDTO> obtenerContratoEnviadoDetalle(int idContratoEnviado);

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<ContratoPorFirmarFirmanteDTO> obtenerContratosPorFirmar_Firmantes(int idContratoEnviado);

        /// <summary>
        /// Actualiza Cliente
        /// </summary>
        void actualizarCliente(int rut, char dv, string rutPalabra,string razonSocial, string nombreCalle, string numeroCalle, string numeroCallePalabra, string letraCalle, string comuna, 
            string ciudad, string autorizaCartaPoder, string email1, string email2, DateTime? fechaFirmaContrato, string repertorio, int idNotaria, string autorizaFirmarNombre, string autorizaFirmarRut,
            bool esPersonaNatural, string nacionalidadPersonaNatural, string profesionPersonaNatural, string telefonoPersonaNatural, int? idEstadoCivilPersonaNatural, int? rutConyuge, char? dvConyuge, 
            string nombreConyuge, string nacionalidadConyuge, int idSucursal);

        /// <summary>
        /// Agrega un Representante de un Cliente
        /// </summary>
        void agregarRepresentanteCliente(int rutCliente, int rutRepresentante, string dvRepresentante, string nombre, string nacionalidad, int idEstadoCivil, string profesion, 
            string nombreCalle, string numeroCalle, string letraCalle, string comuna, string ciudad, string email, bool fiador, DateTime? fechaCtoFact, string notariaPersoneria,
            DateTime? fechaPersoneria, string telefono, int? rutConyuge, string dvConyuge, string nombreConyuge, string nacionalidadConyuge, string profesionConyuge);

        /// <summary>
        /// Actualiza un Representante de un Cliente
        /// </summary>
        void actualizarRepresentanteCliente(int rutCliente, int rutRepresentante, string dvRepresentante, string nombre, string nacionalidad, int idEstadoCivil, string profesion, 
            string nombreCalle, string numeroCalle, string letraCalle, string comuna, string ciudad, string email, bool fiador, DateTime? fechaCtoFact, string notariaPersoneria,
            DateTime? fechaPersoneria, string telefono, int? rutConyuge, string dvConyuge, string nombreConyuge, string nacionalidadConyuge, string profesionConyuge);

        /// <summary>
        /// Actualiza Sucursal
        /// </summary>
        void actualizarSucursal(int idSucursal, string nombre, string direccion, string direccionPalabras, string comuna, string ciudad, bool vigente, int idNotaria, int rutRepresentantePenta1,
                    int? rutRepresentantePenta2);

        /// <summary>
        /// Actualiza Representante de Penta
        /// </summary>
        void actualizarRepresentantePenta(int idRepresentantePenta, int rut, string dv, string nombres, string apPaterno, string apMaterno, string estadoCivil, string profesion, string nacionalidad, bool vigente);


        /// <summary>
        /// Eliminar un Representante de un Cliente
        /// </summary>
        void eliminarRepresentanteCliente(int rutCliente, int rutRepresentante);

        /// <summary>
        /// Eliminar un Representante de un Cliente
        /// </summary>
        void eliminarSucursal(int idSucursal);

        /// <summary>
        /// Eliminar un Representante de Penta
        /// </summary>
        void eliminarRepresentantePenta(int idRepresentantePenta);

        /// <summary>
        ///
        /// </summary>
        void eliminarContratoPorFirmar(int idContratoEnviado);

        ///// <summary>
        ///// obtener ContratoDatosCliente
        ///// </summary>
        //IEnumerable<ClienteDTO> obtenerContratoDatosCliente(int rutCliente, int rutRepresentante1, int? rutRepresentante2, int? rutRepresentante3, int? rutRepresentante4);

        /// <summary>
        /// obtener ContratoOtorgamientoResumen
        /// </summary>
        IEnumerable<OperacionResumenDTO> obtenerOperacionResumen(int IdSimulaOperacion);

        /// <summary>
        /// obtener ContratoOtorgamientoDetalle
        /// </summary>
        IEnumerable<OperacionDetalleDTO> obtenerOperacionDetalle(int IdSimulaOperacion);

        /// <summary>
        /// obtener Operaciones
        /// </summary>
        IEnumerable<OperacionListDTO> obtenerOperaciones(int tipoOperacion, int rutCliente);

        /// <summary>
        /// Obtener el log de contratos generados.
        /// </summary>
        IEnumerable<LogContratoDTO> obtenerLogContratos();

        /// <summary>
        /// Obtener el log de contratos generados de un cliente.
        /// </summary>
        IEnumerable<LogContratoDTO> obtenerLogContrato(int rutCliente);

        /// <summary>
        /// obtener Operaciones Cursadas en PFSA.
        /// </summary>
        IEnumerable<OperacionCursadaDTO> obtenerOperacionesCursadas(int rutCliente);

        /// <summary>
        /// Agrega el Log del Contrato
        /// </summary>
        void agregarLogContrato(string usuario, int rutCliente, string contrato);

        /// <summary>
        /// Agrega Sucursal
        /// </summary>
        void agregarSucursal(string nombre, string direccion, string direccionPalabras, string comuna, string ciudad, bool vigente, int idNotaria, int rutRepresentantePenta1, int? rutRepresentantePenta2);

        /// <summary>
        /// Agrega Representante de Penta
        /// </summary>
        void agregarRepresentantePenta(int rut, string dv, string nombres, string apPaterno, string apMaterno, string estadoCivil, string profesion, string nacionalidad, bool vigente);

        /// <summary>
        ///
        /// </summary>
        IEnumerable<ContratoEnviadoDTO> agregarContratoEnviado(int idSimulaOper, int idOperacionTipo, int idContratoTipo, string usuario, int rutCliente, string contratoBase64, int paginasContrato, DateTime fechaOperacion, string nombreCliente, int cantidadDocumentos, decimal montoDocumentos, decimal montoFinanciado);

        /// <summary>
        ///
        /// </summary>
        void agregarContratoEnviadoFirma(int idContratoEnviado, int rutFirmante, int idPersonaTipo);

        /// <summary>
        ///
        /// </summary>
        void agregarTextoCanalVersion(int idTexto, string texto);

        string ObtenerParrafoVariable(int idContrato, int idTipoVariable, string tagParrafo);

        IEnumerable<DatosOperacionContratoWS> ObtenerDatosOperacion(int idSimulaOper);

        void InsertaRelacionInstaciaOperacion(int idInstancia, int idSimulaOper, int idTipoContratoGestor, int idTipoContratoWS, int rutCliente);

        void MarcarContratoFirmado(int idInstancia);
    }
}
