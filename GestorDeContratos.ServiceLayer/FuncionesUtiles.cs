﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Factoring.Utiles;

namespace GestorDeContratos.ServiceLayer
{
    public class FuncionesUtiles
    {
        public string convertirRutEnPalabras(int rut, string dv)
        {
            int dvInt;
            string rutPalabras = "";
            string dvPalabras = "";

            LetraToNumero letraToNumero = new LetraToNumero();

            rutPalabras += letraToNumero.Convertir(rut.ToString(), false).Trim();

            //Corrige el un por uno
            if (rutPalabras == "un") rutPalabras = "uno";

            //Corrige el diecinuevemillones
            rutPalabras = Regex.Replace(rutPalabras, "^diecinuevemillones", "diecinueve millones");

            //if (dv is int)
            bool result = int.TryParse(dv, out dvInt);
            if(result)
            {
                dvPalabras = letraToNumero.Convertir(dv, false).Trim();
            }
            else
            {
                dvPalabras = dv.ToLower().Trim();
            }

            //Corrige el un por uno
            if (dvPalabras == "un") dvPalabras = "uno";
            
            rutPalabras += " guión " + dvPalabras.Trim();

            return rutPalabras;
        }

        //public string convertirNumeroEnPalabras(int numero)
        public string convertirNumeroEnPalabras(decimal numero)
        {
            string numeroEnPalabras = "";
            /*
            LetraToNumero letraToNumero = new LetraToNumero();

            numeroEnPalabras = letraToNumero.Convertir(numero.ToString(), false);
            numeroEnPalabras = Regex.Replace(numeroEnPalabras, "^un mil ", "mil");
            */

            Conversiones conv = new Conversiones();
            numeroEnPalabras = conv.enletras(numero.ToString());

            return numeroEnPalabras;
        }

        public string convertirNumeroEnPalabras(string numero)
        {
            string numeroEnPalabras = "";
            /*
            if(string.IsNullOrEmpty(numero)) return "";

            LetraToNumero letraToNumero = new LetraToNumero();

            numeroEnPalabras = letraToNumero.Convertir(numero.ToString(), false);
            numeroEnPalabras = Regex.Replace(numeroEnPalabras, "^un mil", "mil");

            if (numero.ToString().Substring(0, 1) == "0") numeroEnPalabras = "cero " + numeroEnPalabras;
            */

            if (string.IsNullOrEmpty(numero)) return "";
            Conversiones conv = new Conversiones();
            numeroEnPalabras = conv.enletras(numero);

            return numeroEnPalabras;
        }

        public string convertirFechaEnPalabras(DateTime fecha)
        {
            string diaPalabras = "";
            string mesPalabras = "";
            string anoPalabras = "";

            diaPalabras = fecha.ToString("dd");
            mesPalabras = fecha.ToString("MMMM");
            anoPalabras = fecha.ToString("yyyy");

            LetraToNumero letraToNumero = new LetraToNumero();
            diaPalabras = letraToNumero.Convertir(diaPalabras, false).Trim();
            //Corrige el un por uno
            if (diaPalabras == "un") diaPalabras = "uno";
            //if (diaPalabras == "un") diaPalabras = "";

            anoPalabras = letraToNumero.Convertir(anoPalabras, false).Trim();
            //Corrige el comienzo del año en palabras "un ", eliminandolo.
            anoPalabras = Regex.Replace(anoPalabras, "^un mil", "mil");

            return diaPalabras + " de " + mesPalabras + " de " + anoPalabras;
        }
    }
}
