﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class RepresentanteClienteDTO : IDisposable
    {
        public int rutCliente { get; set; }

        [DisplayName("Rut")]
        [Required]
        public int rutRepresentante { get; set; }

        [DisplayName("dv")]
        [Required]
        public string dvRepresentante { get; set; }
        
        public string rutPalabra { get; set; }

        [DisplayName("Nombre")]
        [Required]
        public string nombre { get; set; }

        [DisplayName("Nacionalidad")]
        public string nacionalidad { get; set; }

        [DisplayName("Estado Civil")]
        public int idEstadoCivil { get; set; }

        [DisplayName("Nombre Estado Civil")]
        public string estadoCivil { get; set; }

        [DisplayName("Profesión")]
        public string profesion { get; set; }

        [DisplayName("Calle/Avenida")]
        public string nombreCalle { get; set; }

        [DisplayName("Número Calle/Avenida")]
        public string numeroCalle { get; set; }

        [DisplayName("Letra Calle/Avenida")]
        public string letraCalle { get; set; }

        [DisplayName("Comuna")]
        public string comuna { get; set; }

        [DisplayName("Ciudad")]
        public string ciudad { get; set; }

        [DisplayName("Email")]
        public string email { get; set; }

        [DisplayName("Es Fiador")]
        public bool fiador { get; set; }

        [DisplayName("Fech.Contra.Fact.")]
        public DateTime? fechaCtoFact { get; set; }

        public string fechaCtoFactStr { get; set; }

        [DisplayName("Notaría Personería")]
        public string notariaPersoneria { get; set; }

        [DisplayName("Fech.Personería")]
        public DateTime? fechaPersoneria { get; set; }

        [DisplayName("Teléfono")]
        public string telefono { get; set; }

        [DisplayName("Rut Conyuge")]
        public int? rutConyuge { get; set; }

        [DisplayName("DV Conyuge")]
        public string dvConyuge { get; set; }

        public string rutConyugePalabra { get; set; }

        [DisplayName("Nombre Conyuge")]
        public string nombreConyuge { get; set; }

        [DisplayName("Nacionalidad Conyuge")]
        public string nacionalidadConyuge { get; set; }

        [DisplayName("Profesión Conyuge")]
        public string profesionConyuge { get; set; }

        
        private bool disposed = false;

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // TO DO: clean up managed objects
                }

                // TO DO: clean up unmanaged objects
                disposed = true;
            }
        }    
    }
}
