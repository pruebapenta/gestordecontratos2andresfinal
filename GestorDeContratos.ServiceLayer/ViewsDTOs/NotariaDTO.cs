﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class NotariaDTO
    {
        public int idNotaria { get; set; }
        public string notaria { get; set; }
        public string ciudadNotaria { get; set; }
        public int? rutNotario { get; set; }
        public string dvNotario { get; set; }
        public string nombreNotario { get; set; }
        public string emailNotario { get; set; }
        public string nombreCompleto { get; set; }
    }
}
