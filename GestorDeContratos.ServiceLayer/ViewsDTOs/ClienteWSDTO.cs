﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class ClienteWSDTO
    {
        public string nombre;
        public string tipoPersona;
        public int idTipoPersona;
        public string nacionalidad;
        public string estadoCivil;
        public int idEstadoCivil;
        public string profesion;
        public string rut;
        public DireccionWSDTO domicilioCliente;
        public DireccionWSDTO domicilioSucursal;
        public string nombreSucursal;
        public string autorizaCartaPoder;
        public string emailAutorizaCartaPoder;
        public DateTime fechaFirmaContrato;
        public BancoGiro[] bancosGiro;
        public ConyugeWS conyuge;
        public bool contratoMarcoElectronico;
        public string sucursalNotaria;
        public string nombreNotaria;
    }

    public class BancoGiro
    {
        public string nombreBanco;
        public string numeroCuenta;
        public string tipoCuenta;
    }

}
