﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class TipoDocumentoDTO
    {

        public TipoDocumentoDTO()
      {
          this.Employees1 = new HashSet<TipoDocumentoDTO>();
          //this.Orders = new HashSet<TipoDocumentoDTO>();
          //this.Territories = new HashSet<Territory>();
        }


        public int id { get; set; }
        public string nombre { get; set; }
        public int? esHijoDe { get; set; }
        public bool hasChildren = true; //{ get; set; }

        public virtual ICollection<TipoDocumentoDTO> Employees1 { get; set; }
        public virtual TipoDocumentoDTO Employee1 { get; set; }
        //public virtual ICollection<Order> Orders { get; set; }
        //public virtual ICollection<Territory> Territories { get; set; }

        //public HashSet<TipoDocumentoDTO> Orders { get; set; }
    }
}
