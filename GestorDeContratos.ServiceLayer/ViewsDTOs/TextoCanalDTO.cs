﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class TextoCanalDTO
    {
        public int idTexto;
        public string NombreTexto;
    }
}
