﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class ContratoPorFirmarFirmanteDTO
    {
        public int idContratoEnviadoFirma { get; set; }
        public int idContratoEnviado { get; set; }
        public int rutFirmante { get; set; }
        public string dvFirmante { get; set; }
        public string nombreFirmante { get; set; }

        [DisplayName("Fecha Ingreso")]
        public DateTime fechaIngreso { get; set; }
        public bool firmado { get; set; }
        public DateTime? fechaFirmado { get; set; }
        public int idPersonaTipo { get; set; }

        [DisplayName("Tipo Persona")]
        public string Descripcion { get; set; }

        public int? idVersionAcuerdoLegal { get; set; }
    }
}
