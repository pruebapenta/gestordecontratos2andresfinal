﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class DireccionWSDTO
    {
        public string calle;
        public string numero;
        public string deptoCasaOtro;
        public string comuna;
        public string region;
        public string ciudad;
    }
}
