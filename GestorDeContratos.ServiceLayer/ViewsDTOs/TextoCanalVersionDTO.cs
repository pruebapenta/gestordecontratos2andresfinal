﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class TextoCanalVersionDTO
    {
        public int idTexto;
        public string NombreTexto;
        public int idVersion;
        public string Texto;
        public string fechaIngreso;
        public string fechaFin;
    }
}
