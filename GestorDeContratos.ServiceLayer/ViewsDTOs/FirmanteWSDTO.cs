﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class FirmanteWSDTO
    {
        public RepresentanteWS[] representantesCliente;
        public AvalWS[] avales;
        public RepresentanteWS[] represenantesPenta;
    }

    public class RepresentanteWS
    {
        public int idtipoPersona;
        public string tipoPersona;
        public string rut;
        public string nombre;
        public string nacionalidad;
        public string profesion;
        public DireccionWSDTO domicilio;
        public int idEstadoCivil;
        public string estadoCivil;
        public DateTime fechaPersoneria;
        public string nombreNotariaPersoneria;
        public ConyugeWS conyuge;
        public RepresentanteWS[] representantesDelRepresentanteDelCliente;
    }

    public class AvalWS
    {
        public int idtipoPersona;
        public string tipoPersona;
        public string rut;
        public string nombre;
        public string nacionalidad;
        public string profesion;
        public DireccionWSDTO domicilio;
        public int idEstadoCivil;
        public string estadoCivil;
        public ConyugeWS conyuge;
        public RepresentanteWS[] representantesDelAval;
    }

    public class ConyugeWS
    {
        public string rut;
        public string nombre;
        public string nacionalidad;
        public string profesion;
        public string domicilio;
    }
}
