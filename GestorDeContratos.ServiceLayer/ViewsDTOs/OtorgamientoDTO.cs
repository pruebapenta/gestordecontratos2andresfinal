﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class OtorgamientoDTO_DEL : IDisposable
    {
        public int idSimulaOperacion { get; set; }
        public int rutCliente { get; set; }
        public string dvCliente { get; set; }
        public string razonSocial { get; set; }
        public DateTime fechaEmision { get; set; }
        public decimal tasa { get; set; }
        public decimal montoFinanciado { get; set; }
        public decimal montoDocumentos { get; set; }
        public DateTime fechaOtorgamiento { get; set; }
        public decimal ValDoctoEnMoneda { get; set; }
        public string moneda { get; set; }
        public string mailEje { get; set; }

        private bool disposed = false;

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // TO DO: clean up managed objects
                }

                // TO DO: clean up unmanaged objects
                disposed = true;
            }
        }    
    }
}
