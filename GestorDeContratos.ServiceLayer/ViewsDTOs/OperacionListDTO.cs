﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class OperacionListDTO
    {
        public int idSimulaOperacion { get; set; }
        public int rutCliente { get; set; }
        public string dvCliente { get; set; }
        public string razonSocial { get; set; }
        public string tipoDocumento { get; set; }
        public string moneda { get; set; }
        public bool operacionAprobada { get; set; }
        public decimal AGirarEjec { get; set; }
        public decimal saldoPendiente { get; set; }
        public decimal tasa { get; set; }
        public decimal capital { get; set; }
        public DateTime fechaOtorgamiento { get; set; }
    }
}
