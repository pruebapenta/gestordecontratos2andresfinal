﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class ClienteDTO : IDisposable
    {
        //public string fechaingreso { get; set; }
        public DateTime fechaingreso { get; set; }
        public int rut { get; set; }
        public string rutStr { get; set; }
        public string dv { get; set; }
        public string rutPalabra { get; set; }
        public string razonSocial { get; set; }
        public string nombreCalle { get; set; }
        public string numeroCalle { get; set; }
        public string numeroCallePalabra { get; set; }
        public string letraCalle { get; set; }
        public string comuna { get; set; }
        public string ciudad { get; set; }
        public string region { get; set; }
        public string pais { get; set; }
        public string codigoPostal { get; set; }
        public DateTime? fechaCreacion { get; set; }
        public DateTime fechaModificacion { get; set; }
        public string autorizaCartaPoder { get; set; }
        public string email1 { get; set; }
        public string email2 { get; set; }
        public DateTime? fechaFirmaContrato { get; set; }
        public string fechaFirmaContratoStr { get; set; }
        public string repertorio { get; set; }
        public int idNotaria { get; set; }
        public string autorizaFirmarNombre { get; set; }
        public string autorizaFirmarRut { get; set; }
        public bool esPersonaNatural { get; set; }
        public string nacionalidadPersonaNatural { get; set; }
        public string profesionPersonaNatural { get; set; }
        public string telefonoPersonaNatural { get; set; }
        public int idEstadoCivilPersonaNatural { get; set; }
        public string estadoCivilPersonaNatural { get; set; }
        public int? rutConyuge  { get; set; }
        public string dvConyuge { get; set; }
        public string nombreConyuge { get; set; }
        public string nacionalidadConyuge { get; set; }
        public int idSucursal { get; set; }

        private bool disposed = false;

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // TO DO: clean up managed objects
                }

                // TO DO: clean up unmanaged objects
                disposed = true;
            }
        }    
    }
}
