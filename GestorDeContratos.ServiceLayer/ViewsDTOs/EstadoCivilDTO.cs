﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class EstadoCivilDTO
    {
        public int idEstadoCivil { get; set; }
        public string nombreEstadoCivil { get; set; }
    }
}
