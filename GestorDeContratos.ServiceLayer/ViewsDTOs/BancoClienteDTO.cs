﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class BancoClienteDTO
    {
        public string banco { get; set; }
        public string cuenta { get; set; }
        public string tipoCuenta { get; set; }
    }
}
