﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class OperacionResumenDTO : IDisposable
    {
        public int idSimulaOperacion { get; set; }
        public int rutCliente { get; set; }
        public string dvCliente { get; set; }
        public string razonSocial { get; set; }
        public DateTime fechaEmision { get; set; }
        public decimal tasa { get; set; }
        public decimal montoFinanciado { get; set; }
        public decimal montoDocumentos { get; set; }
        public decimal valorCesion { get; set; }
        public decimal AGirarEjec { get; set; }
        public decimal saldoPendiente { get; set; }
        public DateTime fechaOtorgamiento { get; set; }
        public decimal ValDoctoEnMoneda { get; set; }
        public string moneda { get; set; }
        public string mailEje { get; set; }
        public string apPatEje { get; set; }
        public string apMatEje { get; set; }
        public string nombresEje { get; set; }
        public string nombreUsuarioEje { get; set; }

        private bool disposed = false;

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // TO DO: clean up managed objects
                }

                // TO DO: clean up unmanaged objects
                disposed = true;
            }
        }   
    }
}
