﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class ContratoPorFirmarDTO
    {
        [DisplayName("IdContratoEnviado")]
        public int idContratoEnviado { get; set; }

        [DisplayName("IdSimulaOper")]
        public int idSimulaOper { get; set; }

        public int idOperacionTipo { get; set; }

        [DisplayName("T.Operación")]
        public string TipoOperacion { get; set; }
        public int idContratoTipo { get; set; }

        [DisplayName("Contrato")]
        public string nombreContratoTipo { get; set; }

        public DateTime fechaEnvio { get; set; }
        public string usuario { get; set; }
        public int rutCliente { get; set; }
        public string dvCliente { get; set; }
        public string contratoBase64 { get; set; }

        [DisplayName("Páginas")]
        public int paginasContrato { get; set; }
        public bool firmado { get; set; }
        public DateTime fechaOperacion { get; set; }
        public string nombreCliente { get; set; }

        [DisplayName("Cantidad Doc.")]
        public int cantidadDocumentos { get; set; }

        [DisplayName("Monto Doc.")]
        public decimal montoDocumentos { get; set; }

        [DisplayName("Monto Financ.")]
        public decimal montoFinanciado { get; set; }
        public string estadoContrato { get; set; }
    }
}
