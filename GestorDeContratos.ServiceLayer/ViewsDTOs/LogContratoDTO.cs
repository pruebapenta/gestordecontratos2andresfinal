﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class LogContratoDTO
    {
        public int idLog { get; set; }

        [DisplayName("Fecha")]
        public DateTime fecha { get; set; }

        [DisplayName("Usuario")]
        public string usuario { get; set; }

        [DisplayName("Rut Cliente")]
        public int rutCliente { get; set; }

        [DisplayName("Dv Cliente")]
        public string dvCliente { get; set; }

        [DisplayName("Nombre Cliente")]
        public string RSocial { get; set; }

        [DisplayName("Contrato Generado")]
        public string contrato { get; set; }
    }
}
