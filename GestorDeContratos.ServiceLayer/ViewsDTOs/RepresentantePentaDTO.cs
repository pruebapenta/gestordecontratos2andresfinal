﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class RepresentantePentaDTO : IDisposable
    {
        public int idRepresentantePenta { get; set; }
        public int rut { get; set; }
        public string dv { get; set; }
        public string rutPalabras { get; set; }
        public string nombres { get; set; }
        public string apPaterno { get; set; }
        public string apMaterno { get; set; }
        public string nombreCompleto { get; set; }
        public string estadoCivil { get; set; }
        public string profesion { get; set; }
        public string nacionalidad { get; set; }
        public bool vigente { get; set; }

        private bool disposed = false;

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // TO DO: clean up managed objects
                }

                // TO DO: clean up unmanaged objects
                disposed = true;
            }
        }
    }
}
