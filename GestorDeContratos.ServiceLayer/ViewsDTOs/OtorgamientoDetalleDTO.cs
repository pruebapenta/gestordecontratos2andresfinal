﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class OtorgamientoDetalleDTO_DEL : IDisposable
    {
        public int idSimulaOperacion { get; set; }
        public int rutCliente { get; set; }
        public string dvCliente { get; set; }
        public string cliente { get; set; }
        public int rutDeudor { get; set; }
        public string dvDeudor { get; set; }
        public string deudor { get; set; }
        public DateTime fechaEmision { get; set; }
        public DateTime fechaVcto { get; set; }
        public int numDocto { get; set; }
        public decimal valorCesion { get; set; }
        public decimal valorDocto { get; set; }
        public decimal valorDoctoEnMoneda { get; set; }
        public int numNCredito { get; set; }
        public decimal montoNCredito { get; set; }
        public string tipoDocto { get; set; }
        public string moneda { get; set; }
        public bool operacionAprobada { get; set; }
        public string opDevuelta { get; set; }
        public DateTime fechaAprobacion { get; set; }
        public int simulacionesEnviada { get; set; }
        public decimal AGirarEjec { get; set; }
        public decimal saldoPendiente { get; set; }
        public decimal tasas { get; set; }
        public decimal capital { get; set; }
        public decimal interesDocumento { get; set; }
        public DateTime fechaOtor { get; set; }


        private bool disposed = false;

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // TO DO: clean up managed objects
                }

                // TO DO: clean up unmanaged objects
                disposed = true;
            }
        }    
    }
}
