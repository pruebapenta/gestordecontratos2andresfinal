﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class DatosOperacionContratoWS
    {
        public decimal precioDeCompra;
        public decimal saldoPorPagar;
        public decimal saldoPendiente;
        public decimal cantidadDocumentos;
        public decimal montoTotalOperacion;
        public DateTime fechaOtorgamiento;
    }
}
