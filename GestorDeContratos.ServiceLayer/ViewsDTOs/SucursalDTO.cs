﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class SucursalDTO : IDisposable
    {
        public int idSucursal { get; set; }

        [DisplayName("Nombre")]
        public string nombre  { get; set; }

        [DisplayName("Direccion")]
        public string direccion  { get; set; }

        [DisplayName("Direccion en Palabras")]
        public string direccionPalabras { get; set; }

        [DisplayName("Comuna")]
        public string comuna { get; set; }

        [DisplayName("Ciudad")]
        public string ciudad { get; set; }

        [DisplayName("Vigente")]
        public bool vigente { get; set; }

        [DisplayName("IdNotaría")]
        public int idNotaria { get; set; }

        [DisplayName("rut Representante 1")]
        public int rutRepresentantePenta1 { get; set; }

        [DisplayName("rut Representante 2")]
        public int? rutRepresentantePenta2 { get; set; }


        private bool disposed = false;

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // TO DO: clean up managed objects
                }

                // TO DO: clean up unmanaged objects
                disposed = true;
            }
        }
    }
}
