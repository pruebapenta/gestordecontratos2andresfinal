﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class ContratoEnviadoFirmaDTO
    {
        public int idSimulaOper;
        public int idOperacionTipo;
        public int idContratoTipo;
        public int rutFirmante;
        public DateTime fechaIngreso;
        public bool firmado;
        public DateTime? fechaFirmado;
    }
}
