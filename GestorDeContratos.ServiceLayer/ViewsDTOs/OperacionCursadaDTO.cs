﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class OperacionCursadaDTO
    {
        
        public int NroOt { get; set; }

        [DisplayName("NroOt PDF")]
        public string URLOperLegalPDF { get; set; }
        public int NumContrato { get; set; }
        public int RutCliente { get; set; }
        public string DVCliente { get; set; }
		public string Cliente { get; set; }
		public int CantDoctos { get; set; }
		public string Tipo { get; set; }
		//public string FOtorg { get; set; }
        public DateTime FOtorg { get; set; }
		public int NumDeudores { get; set; }
		public decimal Gasto { get; set; }
		public decimal ValorTotal { get; set; }
		public decimal PrecioCompra { get; set; }
		public decimal SaldoPorPagar { get; set; }
		public decimal SaldoPendiente { get; set; }
        public int idsimulaoper { get; set; }
    }
}
