﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorDeContratos.ServiceLayer.ViewsDTOs
{
    public class ContratoEnviadoDTO
    {
        public int idContratoEnviado { get; set; }
        public int idSimulaOper { get; set; }
        public int idOperacionTipo { get; set; }
        public string tipoOperacion { get; set; }
        public int idContratoTipo { get; set; }
        public string nombreContratoTipo { get; set; }
        public DateTime fechaEnvio { get; set; }
        public string usuario { get; set; }
        public int rutCliente { get; set; }
        public string dvCliente { get; set; }
        public string contratoBase64 { get; set; }
        public bool firmado;
        public int paginasContrato { get; set; }
        public DateTime fechaOperacion;
        public string nombreCliente;
        public int cantidadDocumentos;
        public decimal montoDocumentos;
        public decimal montoFinanciado;
        public string estadoContrato;
    }
}
