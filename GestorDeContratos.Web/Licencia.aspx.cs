﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GestorDeContratos.Web
{
    public partial class Licencia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ASPOSE_WORD_Click(object sender, EventArgs e)
        {
            // For complete examples and data files, please go to https://github.com/aspose-words/Aspose.Words-for-.NET
            Aspose.Words.License license = new Aspose.Words.License();

            // This line attempts to set a license from several locations relative to the executable and Aspose.Words.dll.
            // You can also use the additional overload to load a license from a stream, this is useful for instance when the 
            // license is stored as an embedded resource
            try
            {
                //license.SetLicense(Server.MapPath(@"~/Aspose.Words.Product.Family.lic").ToString());
                license.SetLicense(Server.MapPath(@"~/Aspose.Words.lic").ToString());
                Console.WriteLine("License set successfully.");
                Response.Write("<p>License set successfully.</p>");
            }
            catch (Exception ex)
            {
                // We do not ship any license with this example, visit the Aspose site to obtain either a temporary or permanent license. 
                Console.WriteLine("\nThere was an error setting the license: " + ex.Message);
                Response.Write("<p>There was an error setting the license: " + ex.Message + "</p>");
            }
        }
    }
}