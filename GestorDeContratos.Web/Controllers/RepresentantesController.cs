﻿using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestorDeContratos.Web.Controllers
{
    public class RepresentantesController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;

        public RepresentantesController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }

        //
        // GET: /Representantes/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id = 0)
        {
            ViewData["RutCliente"] = id;
            PopulateCategories();
            return View();
        }

        public ActionResult Prueba()
        {
            return View();
        }

        public ActionResult EstadosCiviles_Read()
        {
            IEnumerable<EstadoCivilDTO> lista = new List<EstadoCivilDTO>();
            lista = gestorDeContratos.obtenerEstadosCiviles();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        private void PopulateCategories()
        {
            IEnumerable<EstadoCivilDTO> lista = new List<EstadoCivilDTO>();
            lista = gestorDeContratos.obtenerEstadosCiviles();

            ViewData["categories"] = lista;
            ViewData["defaultCategory"] = lista.First();
        }

        public ActionResult RepresentantesCliente_Read([DataSourceRequest] DataSourceRequest request, int rutCliente)
        {
            IEnumerable<RepresentanteClienteDTO> lista = new List<RepresentanteClienteDTO>();
            lista = gestorDeContratos.obtenerRepresentantesCliente(rutCliente);
            return Json(lista.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RepresentantesCliente_Create([DataSourceRequest] DataSourceRequest request, RepresentanteClienteDTO representante, int rutCliente)
        {
            if (representante != null && ModelState.IsValid)
            {
                //Conversion de números en palabras
                //FuncionesUtiles funcionesUtiles = new FuncionesUtiles();
                //if (!string.IsNullOrEmpty(representante.rutRepresentante.ToString()) && !string.IsNullOrEmpty(representante.dvRepresentante))
                //{
                //    representante.rutPalabra = funcionesUtiles.convertirRutEnPalabras(representante.rutRepresentante, representante.dvRepresentante.ToString());
                //}
                //else
                //{
                //    representante.rutPalabra = null;
                //}
                //if (!string.IsNullOrEmpty(representante.rutConyuge.ToString()) && !string.IsNullOrEmpty(representante.dvConyuge.ToString()))
                //{
                //    representante.rutConyugePalabra = funcionesUtiles.convertirRutEnPalabras((int)representante.rutConyuge, representante.dvConyuge.ToString());
                //}
                //else
                //{
                //    representante.rutConyugePalabra = null;
                //}

                gestorDeContratos.agregarRepresentanteCliente(representante.rutCliente, representante.rutRepresentante, representante.dvRepresentante 
                    , representante.nombre, representante.nacionalidad, representante.idEstadoCivil, representante.profesion, representante.nombreCalle
                    , representante.numeroCalle, representante.letraCalle, representante.comuna, representante.ciudad, representante.ciudad, representante.fiador
                    , representante.fechaCtoFact, representante.notariaPersoneria, representante.fechaPersoneria, representante.telefono, representante.rutConyuge, representante.dvConyuge,
                    representante.nombreConyuge, representante.nacionalidadConyuge, representante.profesionConyuge);
            }
            return Json(new[] { representante }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RepresentantesCliente_Update([DataSourceRequest] DataSourceRequest request, RepresentanteClienteDTO representante)
        {
            if (representante != null)
            {
                //Conversion de números en palabras
                //FuncionesUtiles funcionesUtiles = new FuncionesUtiles();
                //if (!string.IsNullOrEmpty(representante.rutRepresentante.ToString()) && !string.IsNullOrEmpty(representante.dvRepresentante))
                //{
                //    representante.rutPalabra = funcionesUtiles.convertirRutEnPalabras(representante.rutRepresentante, representante.dvRepresentante.ToString());
                //}
                //else
                //{
                //    representante.rutPalabra = null;
                //}
                //if (!string.IsNullOrEmpty(representante.rutConyuge.ToString()) && !string.IsNullOrEmpty(representante.dvConyuge))
                //{
                //    representante.rutConyugePalabra = funcionesUtiles.convertirRutEnPalabras((int)representante.rutConyuge, representante.dvConyuge.ToString());
                //}
                //else
                //{
                //    representante.rutConyugePalabra = null;
                //}

                gestorDeContratos.actualizarRepresentanteCliente(representante.rutCliente, representante.rutRepresentante, representante.dvRepresentante
                    , representante.nombre, representante.nacionalidad, representante.idEstadoCivil, representante.profesion, representante.nombreCalle
                    , representante.numeroCalle, representante.letraCalle, representante.comuna, representante.ciudad, representante.ciudad, representante.fiador
                    , representante.fechaCtoFact, representante.notariaPersoneria, representante.fechaPersoneria, representante.telefono, representante.rutConyuge, representante.dvConyuge
                    , representante.nombreConyuge, representante.nacionalidadConyuge, representante.profesionConyuge);
            }
            return Json(new[] { representante }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RepresentantesCliente_Destroy([DataSourceRequest] DataSourceRequest request, RepresentanteClienteDTO representante)
        {
            var results = new List<RepresentanteClienteDTO>();

            if (representante != null)
            {
                gestorDeContratos.eliminarRepresentanteCliente(representante.rutCliente, representante.rutRepresentante);
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

    }
}
