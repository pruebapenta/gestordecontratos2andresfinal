﻿using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestorDeContratos.Web.Controllers
{
    public class NotariaController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;

        //
        // GET: /Notaria/

        public NotariaController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Notarias_Read()
        {
            IEnumerable<NotariaDTO> lista = new List<NotariaDTO>();
            lista = gestorDeContratos.obtenerNotarias();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
    }
}
