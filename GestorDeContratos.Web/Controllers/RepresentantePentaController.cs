﻿using Factoring.Utiles;
using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestorDeContratos.ServiceLayer;

namespace GestorDeContratos.Web.Controllers
{
    public class RepresentantePentaController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;

        public RepresentantePentaController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }

        //
        // GET: /RepresentantesPenta/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult RepresentantePenta_Read2()
        {
            IEnumerable<RepresentantePentaDTO> lista = new List<RepresentantePentaDTO>();
            lista = gestorDeContratos.obtenerRepresentantesPenta();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RepresentantePenta_Read([DataSourceRequest] DataSourceRequest request)
        {
            IEnumerable<RepresentantePentaDTO> lista = new List<RepresentantePentaDTO>();
            lista = gestorDeContratos.obtenerRepresentantesPenta();
            return Json(lista.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RepresentantePenta_Create([DataSourceRequest] DataSourceRequest request, RepresentantePentaDTO representantePenta)
        {
            if (representantePenta != null && ModelState.IsValid)
            {
                gestorDeContratos.agregarRepresentantePenta(representantePenta.rut, representantePenta.dv, representantePenta.nombres, representantePenta.apPaterno, representantePenta.apMaterno, 
                    representantePenta.estadoCivil, representantePenta.profesion, representantePenta.nacionalidad, representantePenta.vigente);
            }
            return Json(new[] { representantePenta }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RepresentantePenta_Update([DataSourceRequest] DataSourceRequest request, RepresentantePentaDTO representantePenta)
        {
            if (representantePenta != null)
            {
                gestorDeContratos.actualizarRepresentantePenta(representantePenta.idRepresentantePenta, representantePenta.rut, representantePenta.dv, representantePenta.nombres, 
                    representantePenta.apPaterno, representantePenta.apMaterno, representantePenta.estadoCivil, representantePenta.profesion, representantePenta.nacionalidad, 
                    representantePenta.vigente);
            }
            return Json(new[] { representantePenta }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RepresentantePenta_Destroy([DataSourceRequest] DataSourceRequest request, RepresentantePentaDTO representantePenta)
        {
            var results = new List<RepresentantePentaDTO>();
            
            if (representantePenta != null)
            {
                gestorDeContratos.eliminarRepresentantePenta(representantePenta.idRepresentantePenta);
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        private void PopulateNotarias()
        {
            IEnumerable<NotariaDTO> lista = new List<NotariaDTO>();
            lista = gestorDeContratos.obtenerNotarias();

            ViewData["notarias"] = lista;
            ViewData["defaultNotarias"] = lista.First();
        }

        private void PopulateRepresentantesPenta()
        {
            IEnumerable<RepresentantePentaDTO> lista = new List<RepresentantePentaDTO>();
            lista = gestorDeContratos.obtenerRepresentantesPenta();

            ViewData["representantesPenta"] = lista;
            ViewData["defaultrepresentantesPenta"] = lista.First();
        }
    }
}
