﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace GestorDeContratos.Web.Controllers
{
    public class LogContratoController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;

        //
        // GET: /Buscar/
        public LogContratoController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogContrato_Read([DataSourceRequest] DataSourceRequest request)
        {
            IEnumerable<LogContratoDTO> lista = new List<LogContratoDTO>();
            lista = gestorDeContratos.obtenerLogContratos();
            return Json(lista.ToDataSourceResult(request));
        }

        public ActionResult LogContratoCliente_Read([DataSourceRequest] DataSourceRequest request, int rutCliente)
        {
            IEnumerable<LogContratoDTO> lista = new List<LogContratoDTO>();
            lista = gestorDeContratos.obtenerLogContrato(rutCliente);
            return Json(lista.ToDataSourceResult(request));
        }

        public void LogContrato_Agregar(string usuario, int rutCliente, string contrato)
        {
            string usuarioLogIn = System.Environment.UserName;
            usuarioLogIn = usuarioLogIn.ToString().Remove(0, usuarioLogIn.ToString().IndexOf("\\") + 1);
            usuario = usuarioLogIn;

            gestorDeContratos.agregarLogContrato(usuario, rutCliente, contrato);
        }

    }
}
