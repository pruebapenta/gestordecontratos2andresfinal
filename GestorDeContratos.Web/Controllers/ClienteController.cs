﻿using Factoring.Utiles;
using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestorDeContratos.ServiceLayer;

namespace GestorDeContratos.Web.Controllers
{
    public class ClienteController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;
        
        public ClienteController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }

        //
        // GET: /Cliente/Rut/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id = 0)
        {
            ViewData["RutCliente"] = id;
            PopulateCategories();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Cliente_Buscar([DataSourceRequest] DataSourceRequest request, string buscar)
        {
            IEnumerable<ClienteDTO> lista = new List<ClienteDTO>();
            lista = gestorDeContratos.obtenerClientes(buscar);

            return Json(lista.ToDataSourceResult(request));
        }

        public ActionResult Cliente_Read(int rut)
        {
            bool procesoCorrecto = false;
            string mensajeRetorno = "";

            IEnumerable<ClienteDTO> lista = new List<ClienteDTO>();
            ClienteDTO cliente = new ClienteDTO();

            try
            {
                lista = gestorDeContratos.obtenerCliente(rut);
                if (lista.Count() > 0)
                {
                    procesoCorrecto = true;
                    mensajeRetorno = "Datos cargados correctamente";
                    cliente.rut = rut;
                    cliente.dv = lista.FirstOrDefault().dv.ToString();
                    cliente.razonSocial = lista.FirstOrDefault().razonSocial.ToString();
                    cliente.nombreCalle = lista.FirstOrDefault().nombreCalle.ToString();
                    cliente.numeroCalle = lista.FirstOrDefault().numeroCalle.ToString();
                    cliente.letraCalle = lista.FirstOrDefault().letraCalle.ToString();
                    cliente.comuna = lista.FirstOrDefault().comuna.ToString();
                    cliente.ciudad = lista.FirstOrDefault().ciudad.ToString();
                    cliente.region = lista.FirstOrDefault().region.ToString();
                    cliente.pais = lista.FirstOrDefault().pais.ToString();
                    cliente.codigoPostal = lista.FirstOrDefault().codigoPostal.ToString();
                    cliente.autorizaCartaPoder = lista.FirstOrDefault().autorizaCartaPoder.ToString();
                    cliente.email1 = lista.FirstOrDefault().email1.ToString();
                    cliente.email2 = lista.FirstOrDefault().email2.ToString();
                    //cliente.fechaFirmaContrato = (DateTime)lista.FirstOrDefault().fechaFirmaContrato2;
                    cliente.fechaFirmaContratoStr = lista.FirstOrDefault().fechaFirmaContratoStr;
                    cliente.repertorio = lista.FirstOrDefault().repertorio;
                    cliente.idNotaria = lista.FirstOrDefault().idNotaria;
                    cliente.autorizaFirmarNombre = lista.FirstOrDefault().autorizaFirmarNombre;
                    cliente.autorizaFirmarRut = lista.FirstOrDefault().autorizaFirmarRut;
                    cliente.esPersonaNatural = lista.FirstOrDefault().esPersonaNatural;
                    cliente.nacionalidadPersonaNatural = lista.FirstOrDefault().nacionalidadPersonaNatural;
                    cliente.profesionPersonaNatural = lista.FirstOrDefault().profesionPersonaNatural;
                    cliente.telefonoPersonaNatural = lista.FirstOrDefault().telefonoPersonaNatural;
                    cliente.idEstadoCivilPersonaNatural = lista.FirstOrDefault().idEstadoCivilPersonaNatural;
                    cliente.rutConyuge = lista.FirstOrDefault().rutConyuge;
                    cliente.dvConyuge = lista.FirstOrDefault().dvConyuge;
                    cliente.nombreConyuge = lista.FirstOrDefault().nombreConyuge;
                    cliente.nacionalidadConyuge = lista.FirstOrDefault().nacionalidadConyuge;
                    cliente.idSucursal = lista.FirstOrDefault().idSucursal;
                }
            }
            catch (Exception ex)
            {
                mensajeRetorno = ex.Message;
            }

            return new JsonResult
            {
                ContentType = "text/html",
                Data = new
                {
                    success = procesoCorrecto,
                    mensajeRetorno,
                    cliente.rut,
                    cliente.dv,
                    cliente.razonSocial,
                    cliente.nombreCalle,
                    cliente.numeroCalle,
                    cliente.letraCalle,
                    cliente.comuna,
                    cliente.ciudad,
                    cliente.region,
                    cliente.pais,
                    cliente.codigoPostal,
                    cliente.autorizaCartaPoder,
                    cliente.email1,
                    cliente.email2,
                    //cliente.fechaFirmaContrato,
                    cliente.fechaFirmaContratoStr,
                    cliente.repertorio,
                    cliente.idNotaria,
                    cliente.autorizaFirmarNombre,
                    cliente.autorizaFirmarRut,
                    cliente.esPersonaNatural,
                    cliente.nacionalidadPersonaNatural,
                    cliente.profesionPersonaNatural,
                    cliente.telefonoPersonaNatural,
                    cliente.idEstadoCivilPersonaNatural,
                    cliente.rutConyuge,
                    cliente.dvConyuge,
                    cliente.nombreConyuge,
                    cliente.nacionalidadConyuge,
                    cliente.idSucursal,
                }
            };
        }

        public ActionResult EstadosCiviles_Read()
        {
            IEnumerable<EstadoCivilDTO> lista = new List<EstadoCivilDTO>();
            lista = gestorDeContratos.obtenerEstadosCiviles();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Cliente_Update(int rut, char dv, string rutPalabra, string razonSocial, string nombreCalle, string numeroCalle, string numeroCallePalabra, string letraCalle, 
            string comuna, string ciudad, string autorizaCartaPoder, string email1, string email2, DateTime? fechaFirmaContrato, string repertorio, int idNotaria, string autorizaFirmarNombre,
            string autorizaFirmarRut, bool esPersonaNatural, string nacionalidadPersonaNatural, string profesionPersonaNatural, string telefonoPersonaNatural, int? rutConyuge, char? dvConyuge, 
            string nombreConyuge, string nacionalidadConyuge, int idSucursal, int? idEstadoCivilPersonaNatural)
        {
            bool procesoCorrecto = false;
            string mensajeRetorno = "Datos cargados correctamente!";

            try
            {
                //Conversion de números en palabras
                //FuncionesUtiles funcionesUtiles = new FuncionesUtiles();
                //rutPalabra = funcionesUtiles.convertirRutEnPalabras(rut, dv.ToString());
                //numeroCallePalabra = funcionesUtiles.convertirNumeroEnPalabras(numeroCalle.ToString());

                gestorDeContratos.actualizarCliente(rut, dv, rutPalabra, razonSocial, nombreCalle, numeroCalle, numeroCallePalabra, letraCalle, comuna, ciudad, autorizaCartaPoder, email1,
                    email2, fechaFirmaContrato, repertorio, idNotaria, autorizaFirmarNombre, autorizaFirmarRut, esPersonaNatural, nacionalidadPersonaNatural, profesionPersonaNatural, telefonoPersonaNatural, idEstadoCivilPersonaNatural,
                    rutConyuge, dvConyuge, nombreConyuge, nacionalidadConyuge, idSucursal);

                procesoCorrecto = true;
            }
            catch (Exception ex)
            {
                mensajeRetorno = ex.Message.ToString();
            }

            return new JsonResult
            {
                ContentType = "text/html",
                Data = new { success = procesoCorrecto, mensajeRetorno }
            };
        }

        private void PopulateCategories()
        {
            IEnumerable<EstadoCivilDTO> lista = new List<EstadoCivilDTO>();
            lista = gestorDeContratos.obtenerEstadosCiviles();

            ViewData["categories"] = lista;
            ViewData["defaultCategory"] = lista.First();
        }

        public ActionResult Operaciones_Read(int tipoOperacion, int rutCliente)
        {
            IEnumerable<OperacionListDTO> lista = new List<OperacionListDTO>();
            lista = gestorDeContratos.obtenerOperaciones(tipoOperacion, rutCliente);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OperacionesCursadas_Read([DataSourceRequest] DataSourceRequest request, int rutCliente)
        {
            IEnumerable<OperacionCursadaDTO> lista = new List<OperacionCursadaDTO>();
            lista = gestorDeContratos.obtenerOperacionesCursadas(rutCliente);
            return Json(lista.ToDataSourceResult(request));
        }
    }
}
