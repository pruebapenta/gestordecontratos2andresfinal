﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace GestorDeContratos.Web.Controllers
{
    public class TextoCanalController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;

        public TextoCanalController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }

        //
        // GET: /TextoCanal/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TextoCanal_Read()
        {
            IEnumerable<TextoCanalDTO> lista = new List<TextoCanalDTO>();
            lista = gestorDeContratos.obtenerTextoCanalList();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TextoCanalVersion_Read(int idTexto)
        {
            bool procesoCorrecto = false;
            string mensajeRetorno = "";

            IEnumerable<TextoCanalVersionDTO> lista = new List<TextoCanalVersionDTO>();
            TextoCanalVersionDTO textoCanalVersion = new TextoCanalVersionDTO();

            try
            {
                lista = gestorDeContratos.obtenerTextoCanalVersionDetalle(idTexto);
                if (lista.Count() > 0)
                {
                    procesoCorrecto = true;
                    mensajeRetorno = "Datos cargados correctamente";
                    textoCanalVersion.idTexto = lista.FirstOrDefault().idTexto;
                    textoCanalVersion.NombreTexto = lista.FirstOrDefault().NombreTexto;
                    textoCanalVersion.idVersion = lista.FirstOrDefault().idVersion;
                    textoCanalVersion.Texto = lista.FirstOrDefault().Texto;
                    textoCanalVersion.fechaIngreso = lista.FirstOrDefault().fechaIngreso;
                }
            }
            catch (Exception ex)
            {
                mensajeRetorno = ex.Message;
            }

            return new JsonResult
            {
                ContentType = "text/html",
                Data = new
                {
                    success = procesoCorrecto,
                    mensajeRetorno,
                    textoCanalVersion.idTexto,
                    textoCanalVersion.NombreTexto,
                    textoCanalVersion.idVersion,
                    textoCanalVersion.Texto,
                    textoCanalVersion.fechaIngreso,
                }
            };
        }

        public ActionResult TextoCanalVersion_Update(int idTexto, string texto)
        {
            bool procesoCorrecto = false;
            string mensajeRetorno = "Datos cargados correctamente!";

            texto = System.Net.WebUtility.HtmlDecode(texto);

            try
            {
                gestorDeContratos.agregarTextoCanalVersion(idTexto, texto);
                procesoCorrecto = true;
            }
            catch (Exception ex)
            {
                mensajeRetorno = ex.Message.ToString();
            }

            return new JsonResult
            {
                ContentType = "text/html",
                Data = new { success = procesoCorrecto, mensajeRetorno }
            };
        }

        public ActionResult TextoCanalVersionHistory_Read([DataSourceRequest] DataSourceRequest request, int idTexto)
        {
            IEnumerable<TextoCanalVersionDTO> lista = new List<TextoCanalVersionDTO>();
            lista = gestorDeContratos.obtenerTextoCanalVersionList(idTexto);
            return Json(lista.ToDataSourceResult(request));
        }
    }
}
