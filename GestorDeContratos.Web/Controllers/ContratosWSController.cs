﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aspose.Words;
using Aspose.Words.Replacing;
using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;

namespace GestorDeContratos.Web.Controllers
{
    public class ContratosWSController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;

        public ContratosWSController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }
        //
        // GET: /ContratosWS/

        public bool ValidarOperacion(int idSimulaOper)
        {
            bool resultado = false;
            try
            {
                IEnumerable<DatosOperacionContratoWS> datosOp = gestorDeContratos.ObtenerDatosOperacion(idSimulaOper);
                if (datosOp.Count() == 0)
                {
                    resultado = false;
                }
                else
                {
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                resultado = false;
            }

            return resultado;
        }

        public string MarcarContratoFirmado(int idInstancia)
        {
            string resultado = "";

            try
            {
                gestorDeContratos.MarcarContratoFirmado(idInstancia);
                resultado = "CONTRATO MARCADO COMO FIRMADO";    
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string SetLegalNoPep(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";
           
            try
            {
                int idContrato = datosContrato.idContrato;
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                /*
                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);
                */
                 
                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";
                
                plantilla = "SetLegalNOPEP.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);                
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                string parrafoVariable1="";
                string parrafoVariable2 = "";
                string parrafoVariable3 = "";
                string parrafoVariable4 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato, 
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutCliente#", cliente.rut);

                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato, 
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO2#");


                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato, 
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO3#");
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NacionalidadCliente#", cliente.nacionalidad);

                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO4#");
                    parrafoVariable4 = parrafoVariable4.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable4 = parrafoVariable4.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable4 = parrafoVariable4.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }
                else if ((cliente.idTipoPersona == 2)&&(firmante.representantesCliente.Count()==1)) //persona jurídica y un representante
                {
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);

                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO2#");
                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneria1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaPersoneria1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO3#");
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);

                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO4#");
                    parrafoVariable4 = parrafoVariable4.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable4 = parrafoVariable4.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable4 = parrafoVariable4.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable4 = parrafoVariable4.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante
                {
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", firmante.representantesCliente[1].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);


                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO2#");
                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneria1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaPersoneria1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneria2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaPersoneria2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO3#");
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente2#", firmante.representantesCliente[1].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);

                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO4#");
                    parrafoVariable4 = parrafoVariable4.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable4 = parrafoVariable4.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable4 = parrafoVariable4.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable4 = parrafoVariable4.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable4 = parrafoVariable4.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }

                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);
                doc.Range.Replace("#PARRAFO4#", parrafoVariable4, options);
                doc.Range.Replace("#AutorizaCartaPoder#", cliente.autorizaCartaPoder, options);
                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#Email1#", cliente.emailAutorizaCartaPoder, options);
                string[] bancosCliente = new string[50];
                int i = 0;
                foreach (BancoGiro item in cliente.bancosGiro)
                {
                    bancosCliente[i] = item.tipoCuenta + ", del Banco " + item.nombreBanco + ", número: " + item.numeroCuenta;
                    i = i + 1;
                }

                for (int j = 0; j <= 3; j++)
                {
                    if (!string.IsNullOrEmpty(bancosCliente[j]))
                    {
                        doc.Range.Replace("#Banco" + j.ToString() + "#", bancosCliente[j], options);
                    }
                    else
                    {
                        doc.Range.Replace("#Banco" + j.ToString() + "#", "", options);
                    }
                }
                
                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".","").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                
                //generar PDF
                
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);


                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    resultado = fileBase64;


                    #region AgregarContratoEnviado

                    //Agregar/Enviar Contrato
                    string[] rutSeparado = cliente.rut.Split('-');
                    int rutParteEntera = 0;
                    if (rutSeparado.Count() > 1)
                        rutParteEntera = Convert.ToInt32(rutSeparado[0]);


                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea=1 - En curse=2 - Cursada=3
                            , 8 //TipoContrato, PEP
                            , "WebSerive"
                            , rutParteEntera
                            , fileBase64
                            , totalPaginasDocumento
                            , cliente.fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.nombre
                            , 0 //cantidad documentos
                            , 0 //monto documentos
                            , 0 //monto financiado
                        );

                    gestorDeContratos.InsertaRelacionInstaciaOperacion(datosContrato.idInstancia, 0, 8, datosContrato.idContrato, rutParteEntera);

                    #endregion

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".","")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }
           
            return resultado;
        }

        public string SetLegalSiPep(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";

            try
            {
                int idContrato = datosContrato.idContrato;
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "SetLegalSIPEP.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                string parrafoVariable1 = "";
                string parrafoVariable2 = "";
                string parrafoVariable3 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutCliente#", cliente.rut);

                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO2#");

                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO3#");
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante
                {
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);

                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO2#");
                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneria1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaPersoneria1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                    
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO3#");
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante
                {
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", firmante.representantesCliente[1].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);


                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO2#");
                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneria1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaPersoneria1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneria2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaPersoneria2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO3#");
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }

                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);
                doc.Range.Replace("#AutorizaCartaPoder#", cliente.autorizaCartaPoder, options);
                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#Email1#", cliente.emailAutorizaCartaPoder, options);
                string[] bancosCliente = new string[50];
                int i = 0;
                foreach (BancoGiro item in cliente.bancosGiro)
                {
                    bancosCliente[i] = item.tipoCuenta + ", del Banco " + item.nombreBanco + ", número: " + item.numeroCuenta;
                    i = i + 1;
                }

                for (int j = 0; j <= 3; j++)
                {
                    if (!string.IsNullOrEmpty(bancosCliente[j]))
                    {
                        doc.Range.Replace("#Banco" + j.ToString() + "#", bancosCliente[j], options);
                    }
                    else
                    {
                        doc.Range.Replace("#Banco" + j.ToString() + "#", "", options);
                    }
                }

                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);

                fileSaveAs = fileSaveAs + ".pdf";
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;

                #region AgregarContratoEnviado

                //Agregar/Enviar Contrato
                string[] rutSeparado = cliente.rut.Split('-');
                int rutParteEntera = 0;
                if (rutSeparado.Count() > 1)
                    rutParteEntera = Convert.ToInt32(rutSeparado[0]);


                IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                contratoEnviado =
                    gestorDeContratos.agregarContratoEnviado(
                        0 //idSimulaOperacion
                        , 1 //Linea=1 - En curse=2 - Cursada=3
                        , 8 //TipoContrato, PEP
                        , "WebSerive"
                        , rutParteEntera
                        , fileBase64
                        , totalPaginasDocumento
                        , cliente.fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                        , cliente.nombre
                        , 0 //cantidad documentos
                        , 0 //monto documentos
                        , 0 //monto financiado
                    );

                gestorDeContratos.InsertaRelacionInstaciaOperacion(datosContrato.idInstancia, 0, 8, datosContrato.idContrato, rutParteEntera);

                #endregion

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoMarcoInstPrivConResp(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";

            try
            {
                int idContrato = datosContrato.idContrato;
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "ContratoMarcoInstPrivConResp.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);

                #region Parrafo1 
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");
                    
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionCliente#", cliente.profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilCliente#", cliente.estadoCivil);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona==1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);                    
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona==1) && (firmante.representantesCliente[1].idtipoPersona==1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente2#", firmante.representantesCliente[1].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente2#", firmante.representantesCliente[1].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[1].rut));
                    
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2) 
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);
                 
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4, 
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO1#");


                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion

                #region Parrafo2
                string parrafoVariable2 = "";
                if (firmante.represenantesPenta.Count() == 1) //un representante de Penta
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                6, 
                                                                                "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta1#", firmante.represenantesPenta[0].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta1#", firmante.represenantesPenta[0].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta1#", firmante.represenantesPenta[0].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta1#", firmante.represenantesPenta[0].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta1#", ConvierteRutEnPalabras(firmante.represenantesPenta[0].rut));
                    
                    parrafoVariable2 = parrafoVariable2.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal));
                }
                else if (firmante.represenantesPenta.Count() == 2)
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             7,
                                                                             "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta1#", firmante.represenantesPenta[0].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta1#", firmante.represenantesPenta[0].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta1#", firmante.represenantesPenta[0].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta1#", firmante.represenantesPenta[0].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta1#", ConvierteRutEnPalabras(firmante.represenantesPenta[0].rut));
                    
                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta2#", firmante.represenantesPenta[1].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta2#", firmante.represenantesPenta[1].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta2#", firmante.represenantesPenta[1].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta2#", firmante.represenantesPenta[1].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta2#", ConvierteRutEnPalabras(firmante.represenantesPenta[1].rut));

                    parrafoVariable2 = parrafoVariable2.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal));
                }

                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                #endregion 

                #region Parrafo3
                string parrafoVariable3T = "";
                string parrafoVariable3 = "";
                if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cliente.idTipoPersona == 1))//casado y persona natural
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             8,
                                                                             "#PARRAFO3#");

                    parrafoVariable3T = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             8,
                                                                             "#PARRAFO3T#");

                    parrafoVariable3T = parrafoVariable3T.Replace("#NombreConyugeCliente#", cliente.conyuge.nombre);
                    parrafoVariable3T = parrafoVariable3T.Replace("#NacionalidadConyugeCliente#", cliente.conyuge.nacionalidad);
                    parrafoVariable3T = parrafoVariable3T.Replace("#RutConyugeCliente#", cliente.conyuge.rut);
                    parrafoVariable3T = parrafoVariable3T.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3T = parrafoVariable3T.Replace("#ProfesionConyugeCliente#", cliente.conyuge.profesion);
                }
                else
                {
                    parrafoVariable3T = "";
                    parrafoVariable3 = "";

                }

                doc.Range.Replace("#PARRAFO3T#", parrafoVariable3T, options);
                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);
                #endregion


                #region Parrafo4
                string parrafoVariable4 = "";
                int cantidadAvales;
                if (firmante.avales == null) cantidadAvales = 0; else cantidadAvales = firmante.avales.Count();

                    if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales >= 1))//casado con avales
                    {
                        parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 23,
                                                                                 "#PARRAFO4#");
                    }
                    else if ((cliente.idEstadoCivil == 1) && (cantidadAvales >= 1)) //no casado avales
                    {
                        parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                    24,
                                                                                    "#PARRAFO4#");
                    }
                    else
                    {
                        parrafoVariable4 = "";
                    }
                
                
                doc.Range.Replace("#PARRAFO4#", parrafoVariable4, options);
                #endregion

                #region Parrafo5
                string parrafoVariable5 = "";
                
                    if ((cantidadAvales == 1) && (firmante.avales[0].idtipoPersona == 1))//un aval persona natural
                    {
                        parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 10,
                                                                                 "#PARRAFO5#");

                        parrafoVariable5 = parrafoVariable5.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadAval1#", firmante.avales[0].nacionalidad);
                        parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilAval1#", firmante.avales[0].estadoCivil);
                        parrafoVariable5 = parrafoVariable5.Replace("#ProfesionAval1#", firmante.avales[0].profesion);
                        parrafoVariable5 = parrafoVariable5.Replace("#RutAval1#", ConvierteRutEnPalabras(firmante.avales[0].rut));
                        parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval1#", DireccionEnPalabras(firmante.avales[0].domicilio));
                    }
                    else if ((cantidadAvales == 2) && (firmante.avales[0].idtipoPersona == 1) && (firmante.avales[1].idtipoPersona == 1))//dos aval persona natural
                    {
                        parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                    11,
                                                                                    "#PARRAFO5#");

                        parrafoVariable5 = parrafoVariable5.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadAval1#", firmante.avales[0].nacionalidad);
                        parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilAval1#", firmante.avales[0].estadoCivil);
                        parrafoVariable5 = parrafoVariable5.Replace("#ProfesionAval1#", firmante.avales[0].profesion);
                        parrafoVariable5 = parrafoVariable5.Replace("#RutAval1#", ConvierteRutEnPalabras(firmante.avales[0].rut));
                        parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval1#", DireccionEnPalabras(firmante.avales[0].domicilio));
                        parrafoVariable5 = parrafoVariable5.Replace("#NombreAval2#", firmante.avales[1].nombre);
                        parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadAval2#", firmante.avales[1].nacionalidad);
                        parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilAval2#", firmante.avales[1].estadoCivil);
                        parrafoVariable5 = parrafoVariable5.Replace("#ProfesionAval2#", firmante.avales[1].profesion);
                        parrafoVariable5 = parrafoVariable5.Replace("#RutAval2#", ConvierteRutEnPalabras(firmante.avales[1].rut));
                        parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval2#", DireccionEnPalabras(firmante.avales[1].domicilio));
                    }

                    else if ((cantidadAvales == 1) && (firmante.avales[0].idtipoPersona == 2) &&
                        (firmante.avales[0].representantesDelAval.Count() == 1))//un aval persona juridica con un representante
                    {
                        parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                    12,
                                                                                    "#PARRAFO5#");

                        parrafoVariable5 = parrafoVariable5.Replace("#NombreRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombre);
                        parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nacionalidad);
                        parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].estadoCivil);
                        parrafoVariable5 = parrafoVariable5.Replace("#ProfesionRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].profesion);
                        parrafoVariable5 = parrafoVariable5.Replace("#RutRepresentanteAval1#", ConvierteRutEnPalabras(firmante.avales[0].representantesDelAval[0].rut));
                        parrafoVariable5 = parrafoVariable5.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable5 = parrafoVariable5.Replace("#RutAval1#", ConvierteRutEnPalabras(firmante.avales[0].rut));
                        parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval1#", DireccionEnPalabras(firmante.avales[0].domicilio));

                    }
                    else if ((cantidadAvales == 1) && (firmante.avales[0].idtipoPersona == 2) &&
                        (firmante.avales[0].representantesDelAval.Count() == 2))//un aval persona juridica con dos representante
                    {
                        parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                    12,
                                                                                    "#PARRAFO5#");

                        parrafoVariable5 = parrafoVariable5.Replace("#NombreRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombre);
                        parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nacionalidad);
                        parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].estadoCivil);
                        parrafoVariable5 = parrafoVariable5.Replace("#ProfesionRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].profesion);
                        parrafoVariable5 = parrafoVariable5.Replace("#RutRepresentanteAval1#", ConvierteRutEnPalabras(ConvierteRutEnPalabras(firmante.avales[0].representantesDelAval[0].rut)));
                        parrafoVariable5 = parrafoVariable5.Replace("#NombreRepresentanteAval2#", firmante.avales[1].representantesDelAval[0].nombre);
                        parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadRepresentanteAval2#", firmante.avales[1].representantesDelAval[0].nacionalidad);
                        parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilRepresentanteAval2#", firmante.avales[1].representantesDelAval[0].estadoCivil);
                        parrafoVariable5 = parrafoVariable5.Replace("#ProfesionRepresentanteAval2#", firmante.avales[1].representantesDelAval[0].profesion);
                        parrafoVariable5 = parrafoVariable5.Replace("#RutRepresentanteAval2#", ConvierteRutEnPalabras(ConvierteRutEnPalabras(firmante.avales[1].representantesDelAval[0].rut)));
                        parrafoVariable5 = parrafoVariable5.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable5 = parrafoVariable5.Replace("#RutAval1#", ConvierteRutEnPalabras(firmante.avales[0].rut));
                        parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval1#", DireccionEnPalabras(firmante.avales[0].domicilio));

                    }
                    else
                    {
                        parrafoVariable5 = "";
                    }
                
                

                doc.Range.Replace("#PARRAFO5#", parrafoVariable5, options);
                #endregion 

                #region Parrafo6
                string parrafoVariable6 = "";

                if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))//cliente casado con un aval casado
                    {
                        parrafoVariable6 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 15,
                                                                                 "#PARRAFO6#");

                    }
                    else if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4))) //cliente con dos avales, uno casado
                    {
                        parrafoVariable6 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 15,
                                                                                 "#PARRAFO6#");
                    }
                else if ((cliente.idEstadoCivil == 1) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))//cliente no casado con un aval casado
                    {
                        parrafoVariable6 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 18,
                                                                                 "#PARRAFO6#");

                    }
                else if ((cliente.idEstadoCivil == 1) && (cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4))) //cliente no casado con dos avales, uno casado
                    {
                        parrafoVariable6 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 18,
                                                                                 "#PARRAFO6#");
                    }
                    else
                    {
                        parrafoVariable6 = "";
                    }
                

                doc.Range.Replace("#PARRAFO6#", parrafoVariable6, options);

                #endregion

                #region Parrafo7

                string parrafoVariable7 = "";
                if ((cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))//Un aval, casado
                {
                    parrafoVariable7 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             20,
                                                                             "#PARRAFO7#");

                    parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre);
                    parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval1#", firmante.avales[0].conyuge.nacionalidad);
                    parrafoVariable7 = parrafoVariable7.Replace("#NombreAval1#", firmante.avales[0].nombre);
                    parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval1#", firmante.avales[0].conyuge.profesion);
                    parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval1#", ConvierteRutEnPalabras(firmante.avales[0].conyuge.rut));
                }
                else if ((cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4)))//Dos aval, uno casado o dos casados
                {
                    if ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) && (firmante.avales[1].idEstadoCivil == 1))
                    {
                        parrafoVariable7 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             20,
                                                                             "#PARRAFO7#");

                        parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval1#", firmante.avales[0].conyuge.nacionalidad);
                        parrafoVariable7 = parrafoVariable7.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval1#", firmante.avales[0].conyuge.profesion);
                        parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval1#", ConvierteRutEnPalabras(firmante.avales[0].conyuge.rut));
                    }
                    else if ((firmante.avales[0].idEstadoCivil == 1) && (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4))
                    {
                        parrafoVariable7 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             20,
                                                                             "#PARRAFO7#");

                        parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval1#", firmante.avales[1].conyuge.nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval1#", firmante.avales[1].conyuge.nacionalidad);
                        parrafoVariable7 = parrafoVariable7.Replace("#NombreAval1#", firmante.avales[1].nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval1#", firmante.avales[1].conyuge.profesion);
                        parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval1#", ConvierteRutEnPalabras(firmante.avales[1].conyuge.rut));
                    }
                    else if ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) && (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4))
                    {
                        parrafoVariable7 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             21,
                                                                             "#PARRAFO7#");


                        parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval1#", firmante.avales[0].conyuge.nacionalidad);
                        parrafoVariable7 = parrafoVariable7.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval1#", firmante.avales[0].conyuge.profesion);
                        parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval1#", ConvierteRutEnPalabras(firmante.avales[0].conyuge.rut));

                        parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval2#", firmante.avales[1].conyuge.nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval2#", firmante.avales[1].conyuge.nacionalidad);
                        parrafoVariable7 = parrafoVariable7.Replace("#NombreAval2#", firmante.avales[1].nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval2#", firmante.avales[1].conyuge.profesion);
                        parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval2#", ConvierteRutEnPalabras(firmante.avales[1].conyuge.rut));
                    }
                }
                else
                {
                    parrafoVariable7 = "";
                }
                doc.Range.Replace("#PARRAFO7#", parrafoVariable7, options);

                #endregion

                #region Parrafo9AlParrafo17

                string[] parrafoVariable = new string[50];
                
                if (
                    ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 1))
                    //cliente casado con un aval soltero
                    || ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 2) && (firmante.avales[0].idEstadoCivil == 1) && (firmante.avales[1].idEstadoCivil == 1))
                    //cliente casado con dos avales solteros
                    )
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             14,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if (
                    ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))
                    //cliente casado con un aval casado
                    || ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4)))
                    //cliente casado con un aval casado
                    )
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             15,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 0))//cliente casado sin avales
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             16,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if (
                    ((cliente.idEstadoCivil == 1) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 1))
                    //cliente soltero con un aval soltero
                    || ((cliente.idEstadoCivil == 1) && (cantidadAvales == 2) && (firmante.avales[0].idEstadoCivil == 1) && (firmante.avales[1].idEstadoCivil == 1))
                    //cliente soltero con dos avales solteros
                    )
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             17,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if (
                    ((cliente.idEstadoCivil == 1) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))
                    //cliente soltero con un aval casado
                    || ((cliente.idEstadoCivil == 1) && (cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4)))
                    //cliente soltero con un aval casado
                    )
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             18,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if ((cliente.idEstadoCivil == 1) && (cantidadAvales == 0))//cliente soltero sin avales
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             19,
                                                                             "#PARRAFO" + i + "#");
                    }
                }


                for (int i = 9; i <= 17; i++)
                {
                    doc.Range.Replace("#PARRAFO"+i+"#", parrafoVariable[i], options);
                }

                #endregion

                #region Parrafo18

                string parrafoVariable18 = "";

                if ((cliente.idTipoPersona == 2)&&(firmante.representantesCliente.Count()==1)) //persona juridica con un representante
                {
                    parrafoVariable18 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             2,
                                                                             "#PARRAFO18#");

                    
                    parrafoVariable18 = parrafoVariable18.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable18 = parrafoVariable18.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable18 = parrafoVariable18.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona juridica con dos representante
                {
                    parrafoVariable18 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             3,
                                                                             "#PARRAFO18#");

                    parrafoVariable18 = parrafoVariable18.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable18 = parrafoVariable18.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable18 = parrafoVariable18.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable18 = parrafoVariable18.Replace("#FechaPersoneriaRepresentante2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable18 = parrafoVariable18.Replace("#NotariaRepresentante2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                }
                else
                {
                    parrafoVariable18 = "";
                }
                doc.Range.Replace("#PARRAFO18#", parrafoVariable18, options);
                #endregion

                #region Parrafo19
                
                string parrafoVariable19 = "";
                if (cantidadAvales > 0)
                {
                    if ((firmante.avales[0].idtipoPersona == 2) && (firmante.avales[0].representantesDelAval.Count() == 1))
                    //un aval persona juridica con un representante
                    {
                        parrafoVariable19 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 12,
                                                                                 "#PARRAFO19#");


                        parrafoVariable19 = parrafoVariable19.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable19 = parrafoVariable19.Replace("#FechaPersoneriaRepresentanteAval1#", ((DateTime)firmante.avales[0].representantesDelAval[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable19 = parrafoVariable19.Replace("#NotariaRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombreNotariaPersoneria);

                    }
                    else if ((firmante.avales[0].idtipoPersona == 2) && (firmante.avales[0].representantesDelAval.Count() == 2))
                    //un aval persona juridica con dos representante
                    {
                        parrafoVariable19 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 13,
                                                                                 "#PARRAFO19#");


                        parrafoVariable19 = parrafoVariable19.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable19 = parrafoVariable19.Replace("#FechaPersoneriaRepresentanteAval1#", ((DateTime)firmante.avales[0].representantesDelAval[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable19 = parrafoVariable19.Replace("#NotariaRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombreNotariaPersoneria);
                        parrafoVariable19 = parrafoVariable19.Replace("#FechaPersoneriaRepresentanteAval2#", ((DateTime)firmante.avales[0].representantesDelAval[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable19 = parrafoVariable19.Replace("#NotariaRepresentanteAval2#", firmante.avales[0].representantesDelAval[1].nombreNotariaPersoneria);

                    }
                }
                else
                {
                    parrafoVariable19 = "";
                }

                doc.Range.Replace("#PARRAFO19#", parrafoVariable19, options);
                #endregion


                #region Parrafo20

                string parrafoVariable20="";

                if (firmante.represenantesPenta.Count()==1) 
                //un aval persona juridica con un representante
                {
                    parrafoVariable20 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             6,
                                                                             "#PARRAFO20#");
                    
                }
                else if (firmante.represenantesPenta.Count()==2)
                //dos representantes de penta
                {
                    parrafoVariable20 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             7,
                                                                             "#PARRAFO20#");
                    
                }


                doc.Range.Replace("#PARRAFO20#", parrafoVariable20, options);
                #endregion

                
                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);

                fileSaveAs = fileSaveAs + ".pdf";
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;

                #region AgregarContratoEnviado

                //Agregar/Enviar Contrato
                string[] rutSeparado = cliente.rut.Split('-');
                int rutParteEntera = 0;
                if (rutSeparado.Count() > 1)
                    rutParteEntera = Convert.ToInt32(rutSeparado[0]);


                IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                contratoEnviado =
                    gestorDeContratos.agregarContratoEnviado(
                        0 //idSimulaOperacion
                        , 1 //Linea=1 - En curse=2 - Cursada=3
                        , 2 //TipoContrato, Contrato Marco
                        , "WebSerive"
                        , rutParteEntera
                        , fileBase64
                        , totalPaginasDocumento
                        , cliente.fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                        , cliente.nombre
                        , 0 //cantidad documentos
                        , 0 //monto documentos
                        , 0 //monto financiado
                    );

                gestorDeContratos.InsertaRelacionInstaciaOperacion(datosContrato.idInstancia, 0, 2, datosContrato.idContrato, rutParteEntera);
                #endregion

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoMarcoInstPrivSinResp(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";

            try
            {
                int idContrato = datosContrato.idContrato;
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "ContratoMarcoInstPrivSinResp.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);

                #region Parrafo1
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionCliente#", cliente.profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilCliente#", cliente.estadoCivil);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente2#", firmante.representantesCliente[1].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente2#", firmante.representantesCliente[1].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[1].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO1#");


                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion

                #region Parrafo2
                string parrafoVariable2 = "";
                if (firmante.represenantesPenta.Count() == 1) //un representante de Penta
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                6,
                                                                                "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta1#", firmante.represenantesPenta[0].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta1#", ConvierteRutEnPalabras(firmante.represenantesPenta[0].rut));
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta1#", firmante.represenantesPenta[0].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta1#", firmante.represenantesPenta[0].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta1#", firmante.represenantesPenta[0].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal));
                    
                }
                else if (firmante.represenantesPenta.Count() == 2)
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             7,
                                                                             "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta1#", firmante.represenantesPenta[0].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta1#", ConvierteRutEnPalabras(firmante.represenantesPenta[0].rut));
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta1#", firmante.represenantesPenta[0].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta1#", firmante.represenantesPenta[0].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentante1Penta#", firmante.represenantesPenta[0].profesion);

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta2#", firmante.represenantesPenta[1].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta2#", ConvierteRutEnPalabras(firmante.represenantesPenta[1].rut));
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta2#", firmante.represenantesPenta[1].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta2#", firmante.represenantesPenta[1].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta2#", firmante.represenantesPenta[1].profesion);

                    parrafoVariable2 = parrafoVariable2.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal));
                }

                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                #endregion

                #region Parrafo3

                string parrafoVariable3 = "";

                if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 1)) //persona juridica con un representante
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             2,
                                                                             "#PARRAFO3#");


                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable3 = parrafoVariable3.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona juridica con dos representante
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             3,
                                                                             "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable3 = parrafoVariable3.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaPersoneriaRepresentante2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable3 = parrafoVariable3.Replace("#NotariaRepresentante2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                }
                else
                {
                    parrafoVariable3 = "";
                }
                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);
                #endregion

                #region Parrafo4

                string parrafoVariable4 = "";

                int cantidadAvales;
                if (firmante.avales == null) cantidadAvales = 0; else cantidadAvales = firmante.avales.Count();


                if (cantidadAvales > 0)
                {
                    if ((firmante.avales[0].idtipoPersona == 2) && (firmante.avales[0].representantesDelAval.Count() == 1))
                    //un aval persona juridica con un representante
                    {
                        parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 12,
                                                                                 "#PARRAFO4#");


                        parrafoVariable4 = parrafoVariable4.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable4 = parrafoVariable4.Replace("#FechaPersoneriaRepresentanteAval1#", ((DateTime)firmante.avales[0].representantesDelAval[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable4 = parrafoVariable4.Replace("#NotariaRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombreNotariaPersoneria);

                    }
                    else if ((firmante.avales[0].idtipoPersona == 2) && (firmante.avales[0].representantesDelAval.Count() == 2))
                    //un aval persona juridica con dos representante
                    {
                        parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 13,
                                                                                 "#PARRAFO4#");


                        parrafoVariable4 = parrafoVariable4.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable4 = parrafoVariable4.Replace("#FechaPersoneriaRepresentanteAval1#", ((DateTime)firmante.avales[0].representantesDelAval[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable4 = parrafoVariable4.Replace("#NotariaRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombreNotariaPersoneria);
                        parrafoVariable4 = parrafoVariable4.Replace("#FechaPersoneriaRepresentanteAval2#", ((DateTime)firmante.avales[0].representantesDelAval[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable4 = parrafoVariable4.Replace("#NotariaRepresentanteAval2#", firmante.avales[0].representantesDelAval[1].nombreNotariaPersoneria);

                    }
                }
                else
                {
                    parrafoVariable4 = "";
                }

                doc.Range.Replace("#PARRAFO4#", parrafoVariable4, options);
                #endregion


                #region Parrafo5

                string parrafoVariable5 = "";

                if (firmante.represenantesPenta.Count() == 1)
                //un representante de penta
                {
                    parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             6,
                                                                             "#PARRAFO5#");

                }
                else if (firmante.represenantesPenta.Count() == 2)
                //dos representantes de penta
                {
                    parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             7,
                                                                             "#PARRAFO5#");

                }


                doc.Range.Replace("#PARRAFO5#", parrafoVariable5, options);
                #endregion


                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);

                fileSaveAs = fileSaveAs + ".pdf";
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;

                #region AgregarContratoEnviado

                //Agregar/Enviar Contrato
                string[] rutSeparado = cliente.rut.Split('-');
                int rutParteEntera = 0;
                if (rutSeparado.Count() > 1)
                    rutParteEntera = Convert.ToInt32(rutSeparado[0]);


                IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                contratoEnviado =
                    gestorDeContratos.agregarContratoEnviado(
                        0 //idSimulaOperacion
                        , 1 //Linea=1 - En curse=2 - Cursada=3
                        , 2 //TipoContrato, Contrato Marco
                        , "WebSerive"
                        , rutParteEntera
                        , fileBase64
                        , totalPaginasDocumento
                        , cliente.fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                        , cliente.nombre
                        , 0 //cantidad documentos
                        , 0 //monto documentos
                        , 0 //monto financiado
                    );

                gestorDeContratos.InsertaRelacionInstaciaOperacion(datosContrato.idInstancia, 0, 2, datosContrato.idContrato, rutParteEntera);
                #endregion


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoMarcoSinResponsabilidad(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";

            try
            {
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "ContratoMarcoSinResponsabilidad.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);

                #region Parrafo1
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionCliente#", cliente.profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilCliente#", cliente.estadoCivil);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente2#", firmante.representantesCliente[1].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente2#", firmante.representantesCliente[1].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[1].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO1#");


                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion

                #region Parrafo2
                string parrafoVariable2 = "";
                if (firmante.represenantesPenta.Count() == 1) //un representante de Penta
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                6,
                                                                                "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta1#", firmante.represenantesPenta[0].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta1#", firmante.represenantesPenta[0].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta1#", firmante.represenantesPenta[0].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta1#", firmante.represenantesPenta[0].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta1#", ConvierteRutEnPalabras(firmante.represenantesPenta[0].rut));
                    parrafoVariable2 = parrafoVariable2.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal));
                }
                else if (firmante.represenantesPenta.Count() == 2)
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             7,
                                                                             "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta1#", firmante.represenantesPenta[0].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta1#", firmante.represenantesPenta[0].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta1#", firmante.represenantesPenta[0].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta1#", firmante.represenantesPenta[0].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta1#", ConvierteRutEnPalabras(firmante.represenantesPenta[0].rut));

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta2#", firmante.represenantesPenta[1].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta2#", firmante.represenantesPenta[1].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta2#", firmante.represenantesPenta[1].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta2#", firmante.represenantesPenta[1].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta2#", ConvierteRutEnPalabras(firmante.represenantesPenta[1].rut));

                    parrafoVariable2 = parrafoVariable2.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal));
                }

                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                #endregion

                #region Parrafo3

                string parrafoVariable3 = "";

                if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 1)) //persona juridica con un representante
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             2,
                                                                             "#PARRAFO3#");


                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable3 = parrafoVariable3.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona juridica con dos representante
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             3,
                                                                             "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable3 = parrafoVariable3.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaPersoneriaRepresentante2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable3 = parrafoVariable3.Replace("#NotariaRepresentante2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                }
                else
                {
                    parrafoVariable3 = "";
                }
                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);
                #endregion

                
                #region Parrafo4

                string parrafoVariable4 = "";

                if (firmante.represenantesPenta.Count() == 1)
                //un representante de penta
                {
                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             6,
                                                                             "#PARRAFO4#");

                }
                else if (firmante.represenantesPenta.Count() == 2)
                //dos representantes de penta
                {
                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             7,
                                                                             "#PARRAFO4#");

                }


                doc.Range.Replace("#PARRAFO4#", parrafoVariable4, options);
                #endregion

                #region Firmas

                if (cliente.idTipoPersona == 2) //persona juridica
                {
                    if (firmante.representantesCliente.Count() == 1)
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente1PJ#", firmante.representantesCliente[0].nombre, options);
                        doc.Range.Replace("#NombreRepresentanteCliente2PJ#", "", options);
                        doc.Range.Replace("#NombreClientePJ#", "p.p. " + cliente.nombre, options);
                    }
                    if (firmante.representantesCliente.Count() == 2)
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente1PJ#", firmante.representantesCliente[0].nombre, options);
                        doc.Range.Replace("#NombreRepresentanteCliente2PJ#", firmante.representantesCliente[1].nombre, options);
                        doc.Range.Replace("#NombreClientePJ#", "p.p. " + cliente.nombre, options);
                    }
                    doc.Range.Replace("#NombreClientePN#", "", options);
                }
                else if (cliente.idTipoPersona == 1)
                {
                    doc.Range.Replace("#NombreRepresentanteCliente1PJ#", "", options);
                    doc.Range.Replace("#NombreRepresentanteCliente2PJ#", "", options);
                    doc.Range.Replace("#NombreClientePJ#", "", options);
                    doc.Range.Replace("#NombreClientePN#", cliente.nombre, options);
                }

                if (firmante.represenantesPenta.Count() == 1)
                {
                    doc.Range.Replace("#NombreRepresentantePenta1PJ#", firmante.represenantesPenta[0].nombre, options);
                    doc.Range.Replace("#NombreRepresentantePenta2PJ#", "", options);
                }
                else if (firmante.represenantesPenta.Count() == 2)
                {
                    doc.Range.Replace("#NombreRepresentantePenta1PJ#", firmante.represenantesPenta[0].nombre, options);
                    doc.Range.Replace("#NombreRepresentantePenta2PJ#", firmante.represenantesPenta[1].nombre, options);
                }
                #endregion



                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                /*DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);*/

                fileSaveAs = fileSaveAs + ".docx";
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }


        public string ContratoCesionFirmaFisica(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion, string formato)
        {
            string resultado = "";

            try
            {
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "ContratoCesionFisico.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);
                doc.Range.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal), options);

                #region Parrafo1
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionCliente#", cliente.profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilCliente#", cliente.estadoCivil);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente2#", firmante.representantesCliente[1].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente2#", firmante.representantesCliente[1].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[1].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO1#");


                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion

                #region Parrafo2

                string parrafoVariable2 = "";

                if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 1)) //persona juridica con un representante
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             2,
                                                                             "#PARRAFO2#");


                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona juridica con dos representante
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             3,
                                                                             "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                }
                else
                {
                    parrafoVariable2 = "";
                }
                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                #endregion


                #region Parrafo3


                string parrafoVariable3 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente2#", firmante.representantesCliente[1].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut);
                    
                }


                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO3#");


                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut);
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut);
                }
                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);

                
                #endregion

                #region Parrafo4

                string parrafoVariable4 = "";
                if (!cliente.contratoMarcoElectronico) //cliente firmó en notaría
                {
                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                25,
                                                                                "#PARRAFO4#");

                    parrafoVariable4 = parrafoVariable4.Replace("#NotariaSucursal#", cliente.sucursalNotaria);
                    parrafoVariable4 = parrafoVariable4.Replace("#NotariaNombreNotario#", cliente.nombreNotaria);
                    parrafoVariable4 = parrafoVariable4.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }
                else
                {
                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                26,
                                                                                "#PARRAFO4#");

                    parrafoVariable4 = parrafoVariable4.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }

                doc.Range.Replace("#PARRAFO4#", parrafoVariable4, options);

                #endregion 



                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                /*
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);
                */

                fileSaveAs = fileSaveAs + "." + formato;
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoCesionFirmaElectronica(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";

            try
            {
                int idContrato = datosContrato.idContrato;
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "ContratoCesionFirmaElectronica.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);
                doc.Range.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal), options);

                #region Parrafo1
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionCliente#", cliente.profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilCliente#", cliente.estadoCivil);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente2#", firmante.representantesCliente[1].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente2#", firmante.representantesCliente[1].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[1].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO1#");


                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion

                #region Parrafo2

                string parrafoVariable2 = "";

                if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 1)) //persona juridica con un representante
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             2,
                                                                             "#PARRAFO2#");


                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona juridica con dos representante
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             3,
                                                                             "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                }
                else
                {
                    parrafoVariable2 = "";
                }
                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                #endregion

                #region Parrafo3

                string parrafoVariable3 = "";
                if (!cliente.contratoMarcoElectronico) //cliente firmó en notaría
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                25,
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NotariaSucursal#", cliente.sucursalNotaria);
                    parrafoVariable3 = parrafoVariable3.Replace("#NotariaNombreNotario#", cliente.nombreNotaria);
                    parrafoVariable3 = parrafoVariable3.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }
                else
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                26,
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }

                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);

                #endregion 

                #region DatosOperacion

                IEnumerable<DatosOperacionContratoWS> datosOp = gestorDeContratos.ObtenerDatosOperacion(datosOperacion.idSimulaOper);

                
                    doc.Range.Replace("#PrecioDeCompra#", string.Format("{0:N0}", datosOp.FirstOrDefault().precioDeCompra), options);
                    doc.Range.Replace("#SaldoPorPagar#", string.Format("{0:N0}", datosOp.FirstOrDefault().saldoPorPagar), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N0}", datosOp.FirstOrDefault().saldoPendiente), options);
                    doc.Range.Replace("#TotalDocumentos#", string.Format("{0:N0}", datosOp.FirstOrDefault().cantidadDocumentos), options);
                    doc.Range.Replace("#TotalOperacion#", string.Format("{0:N0}", datosOp.FirstOrDefault().montoTotalOperacion), options);
                    doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)datosOp.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                
                #endregion  


                    


                    string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);
                

                fileSaveAs = fileSaveAs + ".pdf";
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                #region AgregarContratoEnviado

                //Agregar/Enviar Contrato
                string[] rutSeparado = cliente.rut.Split('-');
                int rutParteEntera=0;
                if (rutSeparado.Count()>1)
                    rutParteEntera = Convert.ToInt32(rutSeparado[0]);

                    
                IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                contratoEnviado =
                    gestorDeContratos.agregarContratoEnviado(
                        datosOperacion.idSimulaOper //idSimulaOperacion
                        , 1 //Linea=1 - En curse=2 - Cursada=3
                        , 1 //TipoContrato, Contrato Cesion
                        , "WebSerive"
                        , rutParteEntera
                        , fileBase64
                        , totalPaginasDocumento
                        , cliente.fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                        , cliente.nombre
                        , Convert.ToInt32(datosOp.FirstOrDefault().cantidadDocumentos) //cantidad documentos
                        , datosOp.FirstOrDefault().montoTotalOperacion //monto documentos
                        , datosOp.FirstOrDefault().precioDeCompra //monto financiado
                    );

                gestorDeContratos.InsertaRelacionInstaciaOperacion(datosContrato.idInstancia, datosOperacion.idSimulaOper, 1, datosContrato.idContrato, rutParteEntera);

                #endregion

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoCesionCobrazaDelegada(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion, string formato)
        {
            string resultado = "";

            try
            {
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "ContratoCesionCobranzaDelegada.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);
                doc.Range.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal), options);

                #region Parrafo1
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);
                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionCliente#", cliente.profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilCliente#", cliente.estadoCivil);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente2#", firmante.representantesCliente[1].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente2#", firmante.representantesCliente[1].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[1].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO1#");


                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion

                #region Parrafo2

                string parrafoVariable2 = "";

                if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 1)) //persona juridica con un representante
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             2,
                                                                             "#PARRAFO2#");


                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona juridica con dos representante
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             3,
                                                                             "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable2 = parrafoVariable2.Replace("#FechaPersoneriaRepresentante2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable2 = parrafoVariable2.Replace("#NotariaRepresentante2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                }
                else
                {
                    parrafoVariable2 = "";
                }
                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                #endregion


                #region Parrafo3


                string parrafoVariable3 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", ConvierteRutEnPalabras(cliente.rut));

                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {

                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", ConvierteRutEnPalabras(cliente.rut));
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {

                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[1].rut));
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", ConvierteRutEnPalabras(cliente.rut));
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {

                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO3#");

                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));

                }


                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {

                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO3#");


                    parrafoVariable3 = parrafoVariable3.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutCliente#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable3 = parrafoVariable3.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable3 = parrafoVariable3.Replace("#RutRepresentanteDelRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut));
                }
                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);


                #endregion

                #region Parrafo4

                string parrafoVariable4 = "";
                /*
                if (!cliente.contratoMarcoElectronico) //cliente firmó en notaría
                {
                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                25,
                                                                                "#PARRAFO4#");

                    parrafoVariable4 = parrafoVariable4.Replace("#NotariaSucursal#", cliente.sucursalNotaria);
                    parrafoVariable4 = parrafoVariable4.Replace("#NotariaNombreNotario#", cliente.nombreNotaria);
                    parrafoVariable4 = parrafoVariable4.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                }
                 */ 
                //else
                //{
                 
                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                26,
                                                                                "#PARRAFO4#");

                    parrafoVariable4 = parrafoVariable4.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                //}

                doc.Range.Replace("#PARRAFO4#", parrafoVariable4, options);

                #endregion 



                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                
                /*
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);
                */

                fileSaveAs = fileSaveAs + "." + formato;
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string CartaDepositoTercero(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";

            try
            {
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "CartaDepositoTercero.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#FechaActual#", ((DateTime)DateTime.Now).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#FechaActualMasSeisMeses#", ((DateTime)DateTime.Now.AddMonths(6)).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);
                doc.Range.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal), options);

                #region Parrafo1
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    string rutClienteEnPalabras = funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[0].Replace(".", "").ToString())
                        + " guión " + funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[1].ToString());

                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionCliente#", cliente.profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", rutClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilCliente#", cliente.estadoCivil);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    string rutClienteEnPalabras = funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[0].Replace(".", "").ToString())
                        + " guión " + funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[1].ToString());

                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", rutClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    string rutClienteEnPalabras = funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[0].Replace(".", "").ToString()) +
                        " guión " + funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[1].ToString());

                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", firmante.representantesCliente[1].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", rutClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    string rutClienteEnPalabras = funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[0].Replace(".", "").ToString()) +
                        " guión " + funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[1].ToString());

                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", rutClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }


                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    string rutClienteEnPalabras = funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[0].Replace(".", "").ToString()) +
                        " guión " + funcionesUtiles.convertirNumeroEnPalabras(cliente.rut.Split('-')[1].ToString());

                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO1#");


                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", rutClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion


                #region Firmas

                if (cliente.idTipoPersona == 2) //persona juridica
                {
                    if (firmante.representantesCliente.Count() == 1)
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente1PJ#", firmante.representantesCliente[0].nombre, options);
                        doc.Range.Replace("#NombreRepresentanteCliente2PJ#", "", options);
                        doc.Range.Replace("#NombreClientePJ#", "p.p. " + cliente.nombre, options);
                        doc.Range.Replace("#RutClientePJ#", cliente.rut, options);
                    }
                    if (firmante.representantesCliente.Count() == 2)
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente1PJ#", firmante.representantesCliente[0].nombre, options);
                        doc.Range.Replace("#NombreRepresentanteCliente2PJ#", firmante.representantesCliente[1].nombre, options);
                        doc.Range.Replace("#NombreClientePJ#", "p.p. " + cliente.nombre, options);
                        doc.Range.Replace("#RutClientePJ#", cliente.rut, options);
                    }
                    doc.Range.Replace("#NombreClientePN#", "", options);
                    doc.Range.Replace("#RutClientePN#", "", options);
                }
                else if (cliente.idTipoPersona == 1)
                {
                    doc.Range.Replace("#NombreRepresentanteCliente1PJ#", "", options);
                    doc.Range.Replace("#NombreRepresentanteCliente2PJ#", "", options);
                    doc.Range.Replace("#NombreClientePJ#", "", options);
                    doc.Range.Replace("#NombreClientePN#", cliente.nombre, options);
                    doc.Range.Replace("#RutClientePN#", cliente.rut, options);
                }

                
                #endregion




                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                /*
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);
                */

                fileSaveAs = fileSaveAs + ".docx";
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public Tuple<bool,string> FichaAvalistaFirmaFisica(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";
            bool ejecutado = true;
            try
            {
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "FichaAvalistaFirmaFisica.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                if (firmante.avales == null || firmante.avales.Count() == 0)
                {
                    ejecutado = false;
                    resultado = "Este documento requiere Avales para ser generado";
                }
                else
                {

                    doc.Range.Replace("#NOMBREAVAL1#", firmante.avales[0].nombre, options);
                    doc.Range.Replace("#RutAval1#", firmante.avales[0].rut, options);
                    doc.Range.Replace("#DOMICILIOAVAL1#", DireccionEnPalabras(firmante.avales[0].domicilio), options);
                    doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);


                    if (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) //casado
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "CONYUGE AVAL: " + firmante.avales[0].conyuge.nombre, options);
                        doc.Range.Replace("#RutConyuge1#", "C.I. N° " + firmante.avales[0].conyuge.rut, options);
                        doc.Range.Replace("#DOMICILIOCONYUGEAVAL1#", "Domicilio: " + firmante.avales[0].conyuge.domicilio, options);
                        doc.Range.Replace("#FirmaConyuge#", "Firma conyuge de fiador y/o codeudor solidario", options);

                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#DOMICILIOCONYUGEAVAL1#", "", options);
                    }


                    string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                    string fileSaveAsPath = "";
                    int totalPaginasDocumento = 0;

                    //generar PDF

                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    /*
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);
                    */

                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);


                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    resultado = fileBase64;


                    //Se registra contrato en la tabla log
                    LogContratoController logC = new LogContratoController();
                    logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                    file.Close();
                }
            }
            catch (Exception ex)
            {
                ejecutado = false;
                resultado = ex.ToString();
            }

            return Tuple.Create(ejecutado,resultado);
        }

        public Tuple<bool, string> FichaAvalistaFirmaElectronica(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";
            bool ejecutado = true;

            try
            {
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "FichaAvalistaFirmaElectronica.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                if (firmante.avales == null || firmante.avales.Count()==0)
                {
                    ejecutado = false;
                    resultado = "Este documento requiere Avales para ser generado";
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", firmante.avales[0].nombre, options);
                    doc.Range.Replace("#RutAval1#", firmante.avales[0].rut, options);
                    doc.Range.Replace("#DOMICILIOAVAL1#", DireccionEnPalabras(firmante.avales[0].domicilio), options);
                    doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);


                    if (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) //casado
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "CONYUGE AVAL: " + firmante.avales[0].conyuge.nombre, options);
                        doc.Range.Replace("#RutConyuge1#", "C.I. N° " + firmante.avales[0].conyuge.rut, options);
                        doc.Range.Replace("#DOMICILIOCONYUGEAVAL1#", "Domicilio: " + firmante.avales[0].conyuge.domicilio, options);
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#DOMICILIOCONYUGEAVAL1#", "", options);
                    }


                    string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                    string fileSaveAsPath = "";
                    int totalPaginasDocumento = 0;

                    //generar PDF

                    //Se agrega un salto de página, para posterior inserta las firmas digitales

                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);


                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);


                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    resultado = fileBase64;


                    //Se registra contrato en la tabla log
                    LogContratoController logC = new LogContratoController();
                    logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                    file.Close();
                }
            }
            catch (Exception ex)
            {
                ejecutado = false;
                resultado = ex.ToString();
            }

            return Tuple.Create(ejecutado, resultado);
        }

        public string FichaPEPPositivo(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";

            try
            {
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "DocumentoPEPPositivo.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#DomicilioCliente#", cliente.domicilioCliente.calle + " " + cliente.domicilioCliente.numero + "  " + cliente.domicilioCliente.deptoCasaOtro + ", comuna " + cliente.domicilioCliente.comuna + ", región " + cliente.domicilioCliente.region, options);
                doc.Range.Replace("#RutCliente#", cliente.rut, options);

                #region Parrafo1
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutCliente#", cliente.rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutCliente#", cliente.rut);
                    
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                 
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", firmante.representantesCliente[0].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", firmante.representantesCliente[1].rut);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                 
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutCliente#", cliente.rut);
                    
                }

                
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion

                


                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                /*
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);
                */

                fileSaveAs = fileSaveAs + ".docx";
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string CartaGuia(int idContrato)
        {
            string resultado = "";

            try
            {
                string plantilla = "";

                plantilla = "CartaGuia.xls";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

        
                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileName);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(idContrato), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

        
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string FichaBeneficiarioFinal(int idContrato)
        {
            string resultado = "";

            try
            {
                string plantilla = "";

                plantilla = "FormularioBeneficiarioFinal.pdf";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileName);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(idContrato), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoFactoringConResponsabilidad(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string resultado = "";

            try
            {
                IEnumerable<RepresentanteWS> representantesCliente = new List<RepresentanteWS>();
                representantesCliente = firmante.representantesCliente;

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";

                plantilla = "ContratoFactoringConResponsabilidad.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = System.Web.HttpContext.Current.Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.nombre, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Sucursal#", cliente.domicilioSucursal.ciudad, options);

                #region Parrafo1
                string parrafoVariable1 = "";
                if (cliente.idTipoPersona == 1) //persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                1, //PERSONA NATURAL
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadCliente#", cliente.nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionCliente#", cliente.profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilCliente#", cliente.estadoCivil);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 1)) //persona jurídica y un representante persona natural
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                2, //PERSONA JURIDICA y UN REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 1) && (firmante.representantesCliente[1].idtipoPersona == 1) && (firmante.representantesCliente.Count() == 2)) //persona jurídica y dos representante personas naturales
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);

                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                3, //PERSONA JURIDICA y DOS REPRESENTANTE
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente1#", firmante.representantesCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente1#", firmante.representantesCliente[0].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente1#", firmante.representantesCliente[0].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente2#", firmante.representantesCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteCliente2#", firmante.representantesCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#EstadoCivilRepresentanteCliente2#", firmante.representantesCliente[1].estadoCivil);
                    parrafoVariable1 = parrafoVariable1.Replace("#ProfesionRepresentanteCliente2#", firmante.representantesCliente[1].profesion);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[1].rut));

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }

                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 1)
                    ) //persona jurídica y un representante personas jurídica y este tiene un representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);
                    
                    
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                4,
                                                                                "#PARRAFO1#");

                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente[0].idtipoPersona == 2)
                    && (firmante.representantesCliente.Count() == 1)
                    && (firmante.representantesCliente[0].representantesDelRepresentanteDelCliente.Count() == 2)
                    ) //persona jurídica y un representante personas jurídica y este tiene dos representante
                {
                    string direccionClienteEnPalabras = DireccionEnPalabras(cliente.domicilioCliente);
                 
                    parrafoVariable1 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                5,
                                                                                "#PARRAFO1#");


                    parrafoVariable1 = parrafoVariable1.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutClientePalabras#", ConvierteRutEnPalabras(cliente.rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteCliente1#", firmante.representantesCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente1#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente1#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[0].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#NombreRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nombre);
                    parrafoVariable1 = parrafoVariable1.Replace("#RutRepresentanteDelRepresentanteCliente2#", ConvierteRutEnPalabras(firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].rut));
                    parrafoVariable1 = parrafoVariable1.Replace("#NacionalidadRepresentanteDelRepresentanteCliente2#", firmante.representantesCliente[0].representantesDelRepresentanteDelCliente[1].nacionalidad);
                    parrafoVariable1 = parrafoVariable1.Replace("#DomicilioCliente#", direccionClienteEnPalabras);
                }
                doc.Range.Replace("#PARRAFO1#", parrafoVariable1, options);
                #endregion

                #region Parrafo2
                string parrafoVariable2 = "";
                if (firmante.represenantesPenta.Count() == 1) //un representante de Penta
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                6,
                                                                                "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta1#", firmante.represenantesPenta[0].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta1#", firmante.represenantesPenta[0].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta1#", firmante.represenantesPenta[0].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta1#", firmante.represenantesPenta[0].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta1#", ConvierteRutEnPalabras(firmante.represenantesPenta[0].rut));

                    parrafoVariable2 = parrafoVariable2.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal));
                }
                else if (firmante.represenantesPenta.Count() == 2)
                {
                    parrafoVariable2 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             7,
                                                                             "#PARRAFO2#");

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta1#", firmante.represenantesPenta[0].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta1#", firmante.represenantesPenta[0].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta1#", firmante.represenantesPenta[0].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta1#", firmante.represenantesPenta[0].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta1#", ConvierteRutEnPalabras(firmante.represenantesPenta[0].rut));

                    parrafoVariable2 = parrafoVariable2.Replace("#NombreRepresentantePenta2#", firmante.represenantesPenta[1].nombre);
                    parrafoVariable2 = parrafoVariable2.Replace("#NacionalidadRepresentantePenta2#", firmante.represenantesPenta[1].nacionalidad);
                    parrafoVariable2 = parrafoVariable2.Replace("#EstadoCivilRepresentantePenta2#", firmante.represenantesPenta[1].estadoCivil);
                    parrafoVariable2 = parrafoVariable2.Replace("#ProfesionRepresentantePenta2#", firmante.represenantesPenta[1].profesion);
                    parrafoVariable2 = parrafoVariable2.Replace("#RutRepresentantePenta2#", ConvierteRutEnPalabras(firmante.represenantesPenta[1].rut));

                    parrafoVariable2 = parrafoVariable2.Replace("#DomicilioSucursal#", DireccionEnPalabras(cliente.domicilioSucursal));
                }

                doc.Range.Replace("#PARRAFO2#", parrafoVariable2, options);
                #endregion

                #region Parrafo3
                string parrafoVariable3T = "";
                string parrafoVariable3 = "";
                if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cliente.idTipoPersona == 1))//casado y persona natural
                {
                    parrafoVariable3 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             8,
                                                                             "#PARRAFO3#");

                    parrafoVariable3T = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             8,
                                                                             "#PARRAFO3T#");

                    parrafoVariable3T = parrafoVariable3T.Replace("#NombreConyugeCliente#", cliente.conyuge.nombre);
                    parrafoVariable3T = parrafoVariable3T.Replace("#NacionalidadConyugeCliente#", cliente.conyuge.nacionalidad);
                    parrafoVariable3T = parrafoVariable3T.Replace("#RutConyugeCliente#", cliente.conyuge.rut);
                    parrafoVariable3T = parrafoVariable3T.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable3T = parrafoVariable3T.Replace("#ProfesionConyugeCliente#", cliente.conyuge.profesion);
                }
                else
                {
                    parrafoVariable3T = "";
                    parrafoVariable3 = "";

                }

                doc.Range.Replace("#PARRAFO3T#", parrafoVariable3T, options);
                doc.Range.Replace("#PARRAFO3#", parrafoVariable3, options);
                #endregion


                #region Parrafo4
                string parrafoVariable4 = "";
                int cantidadAvales;
                if (firmante.avales == null) cantidadAvales = 0; else cantidadAvales = firmante.avales.Count();

                if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales >= 1))//casado con avales
                {
                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             23,
                                                                             "#PARRAFO4#");
                }
                else if ((cliente.idEstadoCivil == 1) && (cantidadAvales >= 1)) //no casado avales
                {
                    parrafoVariable4 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                24,
                                                                                "#PARRAFO4#");
                }
                else
                {
                    parrafoVariable4 = "";
                }


                doc.Range.Replace("#PARRAFO4#", parrafoVariable4, options);
                #endregion

                #region Parrafo5
                string parrafoVariable5 = "";

                if ((cantidadAvales == 1) && (firmante.avales[0].idtipoPersona == 1))//un aval persona natural
                {
                    parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             10,
                                                                             "#PARRAFO5#");

                    parrafoVariable5 = parrafoVariable5.Replace("#NombreAval1#", firmante.avales[0].nombre);
                    parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadAval1#", firmante.avales[0].nacionalidad);
                    parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilAval1#", firmante.avales[0].estadoCivil);
                    parrafoVariable5 = parrafoVariable5.Replace("#ProfesionAval1#", firmante.avales[0].profesion);
                    parrafoVariable5 = parrafoVariable5.Replace("#RutAval1#", ConvierteRutEnPalabras(firmante.avales[0].rut));
                    parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval1#", DireccionEnPalabras(firmante.avales[0].domicilio));
                }
                else if ((cantidadAvales == 2) && (firmante.avales[0].idtipoPersona == 1) && (firmante.avales[1].idtipoPersona == 1))//dos aval persona natural
                {
                    parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                11,
                                                                                "#PARRAFO5#");

                    parrafoVariable5 = parrafoVariable5.Replace("#NombreAval1#", firmante.avales[0].nombre);
                    parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadAval1#", firmante.avales[0].nacionalidad);
                    parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilAval1#", firmante.avales[0].estadoCivil);
                    parrafoVariable5 = parrafoVariable5.Replace("#ProfesionAval1#", firmante.avales[0].profesion);
                    parrafoVariable5 = parrafoVariable5.Replace("#RutAval1#", ConvierteRutEnPalabras(firmante.avales[0].rut));
                    parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval1#", DireccionEnPalabras(firmante.avales[0].domicilio));
                    parrafoVariable5 = parrafoVariable5.Replace("#NombreAval2#", firmante.avales[1].nombre);
                    parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadAval2#", firmante.avales[1].nacionalidad);
                    parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilAval2#", firmante.avales[1].estadoCivil);
                    parrafoVariable5 = parrafoVariable5.Replace("#ProfesionAval2#", firmante.avales[1].profesion);
                    parrafoVariable5 = parrafoVariable5.Replace("#RutAval2#", ConvierteRutEnPalabras(firmante.avales[1].rut));
                    parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval2#", DireccionEnPalabras(firmante.avales[1].domicilio));
                }

                else if ((cantidadAvales == 1) && (firmante.avales[0].idtipoPersona == 2) &&
                    (firmante.avales[0].representantesDelAval.Count() == 1))//un aval persona juridica con un representante
                {
                    parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                12,
                                                                                "#PARRAFO5#");

                    parrafoVariable5 = parrafoVariable5.Replace("#NombreRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombre);
                    parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nacionalidad);
                    parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].estadoCivil);
                    parrafoVariable5 = parrafoVariable5.Replace("#ProfesionRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].profesion);
                    parrafoVariable5 = parrafoVariable5.Replace("#RutRepresentanteAval1#", ConvierteRutEnPalabras(firmante.avales[0].representantesDelAval[0].rut));
                    parrafoVariable5 = parrafoVariable5.Replace("#NombreAval1#", firmante.avales[0].nombre);
                    parrafoVariable5 = parrafoVariable5.Replace("#RutAval1#", ConvierteRutEnPalabras(firmante.avales[0].rut));
                    parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval1#", DireccionEnPalabras(firmante.avales[0].domicilio));

                }
                else if ((cantidadAvales == 1) && (firmante.avales[0].idtipoPersona == 2) &&
                    (firmante.avales[0].representantesDelAval.Count() == 2))//un aval persona juridica con dos representante
                {
                    parrafoVariable5 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                12,
                                                                                "#PARRAFO5#");

                    parrafoVariable5 = parrafoVariable5.Replace("#NombreRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombre);
                    parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nacionalidad);
                    parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].estadoCivil);
                    parrafoVariable5 = parrafoVariable5.Replace("#ProfesionRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].profesion);
                    parrafoVariable5 = parrafoVariable5.Replace("#RutRepresentanteAval1#", ConvierteRutEnPalabras(ConvierteRutEnPalabras(firmante.avales[0].representantesDelAval[0].rut)));
                    parrafoVariable5 = parrafoVariable5.Replace("#NombreRepresentanteAval2#", firmante.avales[1].representantesDelAval[0].nombre);
                    parrafoVariable5 = parrafoVariable5.Replace("#NacionalidadRepresentanteAval2#", firmante.avales[1].representantesDelAval[0].nacionalidad);
                    parrafoVariable5 = parrafoVariable5.Replace("#EstadoCivilRepresentanteAval2#", firmante.avales[1].representantesDelAval[0].estadoCivil);
                    parrafoVariable5 = parrafoVariable5.Replace("#ProfesionRepresentanteAval2#", firmante.avales[1].representantesDelAval[0].profesion);
                    parrafoVariable5 = parrafoVariable5.Replace("#RutRepresentanteAval2#", ConvierteRutEnPalabras(ConvierteRutEnPalabras(firmante.avales[1].representantesDelAval[0].rut)));
                    parrafoVariable5 = parrafoVariable5.Replace("#NombreAval1#", firmante.avales[0].nombre);
                    parrafoVariable5 = parrafoVariable5.Replace("#RutAval1#", ConvierteRutEnPalabras(firmante.avales[0].rut));
                    parrafoVariable5 = parrafoVariable5.Replace("#DomicilioAval1#", DireccionEnPalabras(firmante.avales[0].domicilio));

                }
                else
                {
                    parrafoVariable5 = "";
                }



                doc.Range.Replace("#PARRAFO5#", parrafoVariable5, options);
                #endregion

                #region Parrafo6
                string parrafoVariable6 = "";

                if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))//cliente casado con un aval casado
                {
                    parrafoVariable6 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             15,
                                                                             "#PARRAFO6#");

                }
                else if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4))) //cliente con dos avales, uno casado
                {
                    parrafoVariable6 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             15,
                                                                             "#PARRAFO6#");
                }
                else if ((cliente.idEstadoCivil == 1) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))//cliente no casado con un aval casado
                {
                    parrafoVariable6 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             18,
                                                                             "#PARRAFO6#");

                }
                else if ((cliente.idEstadoCivil == 1) && (cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4))) //cliente no casado con dos avales, uno casado
                {
                    parrafoVariable6 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             18,
                                                                             "#PARRAFO6#");
                }
                else
                {
                    parrafoVariable6 = "";
                }


                doc.Range.Replace("#PARRAFO6#", parrafoVariable6, options);

                #endregion

                #region Parrafo7

                string parrafoVariable7 = "";
                if ((cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))//Un aval, casado
                {
                    parrafoVariable7 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             20,
                                                                             "#PARRAFO7#");

                    parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre);
                    parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval1#", firmante.avales[0].conyuge.nacionalidad);
                    parrafoVariable7 = parrafoVariable7.Replace("#NombreAval1#", firmante.avales[0].nombre);
                    parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval1#", firmante.avales[0].conyuge.profesion);
                    parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval1#", ConvierteRutEnPalabras(firmante.avales[0].conyuge.rut));
                }
                else if ((cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4)))//Dos aval, uno casado o dos casados
                {
                    if ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) && (firmante.avales[1].idEstadoCivil == 1))
                    {
                        parrafoVariable7 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             20,
                                                                             "#PARRAFO7#");

                        parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval1#", firmante.avales[0].conyuge.nacionalidad);
                        parrafoVariable7 = parrafoVariable7.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval1#", firmante.avales[0].conyuge.profesion);
                        parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval1#", ConvierteRutEnPalabras(firmante.avales[0].conyuge.rut));
                    }
                    else if ((firmante.avales[0].idEstadoCivil == 1) && (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4))
                    {
                        parrafoVariable7 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             20,
                                                                             "#PARRAFO7#");

                        parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval1#", firmante.avales[1].conyuge.nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval1#", firmante.avales[1].conyuge.nacionalidad);
                        parrafoVariable7 = parrafoVariable7.Replace("#NombreAval1#", firmante.avales[1].nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval1#", firmante.avales[1].conyuge.profesion);
                        parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval1#", ConvierteRutEnPalabras(firmante.avales[1].conyuge.rut));
                    }
                    else if ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) && (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4))
                    {
                        parrafoVariable7 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             21,
                                                                             "#PARRAFO7#");


                        parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval1#", firmante.avales[0].conyuge.nacionalidad);
                        parrafoVariable7 = parrafoVariable7.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval1#", firmante.avales[0].conyuge.profesion);
                        parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval1#", ConvierteRutEnPalabras(firmante.avales[0].conyuge.rut));

                        parrafoVariable7 = parrafoVariable7.Replace("#NombreConyugeAval2#", firmante.avales[1].conyuge.nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#NacionalidadConyugeAval2#", firmante.avales[1].conyuge.nacionalidad);
                        parrafoVariable7 = parrafoVariable7.Replace("#NombreAval2#", firmante.avales[1].nombre);
                        parrafoVariable7 = parrafoVariable7.Replace("#ProfesionConyugeAval2#", firmante.avales[1].conyuge.profesion);
                        parrafoVariable7 = parrafoVariable7.Replace("#RutConyugeAval2#", ConvierteRutEnPalabras(firmante.avales[1].conyuge.rut));
                    }
                }
                else
                {
                    parrafoVariable7 = "";
                }
                doc.Range.Replace("#PARRAFO7#", parrafoVariable7, options);

                #endregion

                #region Parrafo9AlParrafo17

                string[] parrafoVariable = new string[50];

                if (
                    ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 1))
                    //cliente casado con un aval soltero
                    || ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 2) && (firmante.avales[0].idEstadoCivil == 1) && (firmante.avales[1].idEstadoCivil == 1))
                    //cliente casado con dos avales solteros
                    )
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             14,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if (
                    ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))
                    //cliente casado con un aval casado
                    || ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4)))
                    //cliente casado con un aval casado
                    )
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             15,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if ((cliente.idEstadoCivil == 2 || cliente.idEstadoCivil == 4) && (cantidadAvales == 0))//cliente casado sin avales
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             16,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if (
                    ((cliente.idEstadoCivil == 1) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 1))
                    //cliente soltero con un aval soltero
                    || ((cliente.idEstadoCivil == 1) && (cantidadAvales == 2) && (firmante.avales[0].idEstadoCivil == 1) && (firmante.avales[1].idEstadoCivil == 1))
                    //cliente soltero con dos avales solteros
                    )
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             17,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if (
                    ((cliente.idEstadoCivil == 1) && (cantidadAvales == 1) && (firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4))
                    //cliente soltero con un aval casado
                    || ((cliente.idEstadoCivil == 1) && (cantidadAvales == 2) && ((firmante.avales[0].idEstadoCivil == 2 || firmante.avales[0].idEstadoCivil == 4) || (firmante.avales[1].idEstadoCivil == 2 || firmante.avales[1].idEstadoCivil == 4)))
                    //cliente soltero con un aval casado
                    )
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             18,
                                                                             "#PARRAFO" + i + "#");
                    }
                }
                else if ((cliente.idEstadoCivil == 1) && (cantidadAvales == 0))//cliente soltero sin avales
                {
                    for (int i = 9; i <= 17; i++)
                    {
                        parrafoVariable[i] = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             19,
                                                                             "#PARRAFO" + i + "#");
                    }
                }


                for (int i = 9; i <= 17; i++)
                {
                    doc.Range.Replace("#PARRAFO" + i + "#", parrafoVariable[i], options);
                }

                #endregion

                #region Parrafo18

                string parrafoVariable18 = "";

                if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 1)) //persona juridica con un representante
                {
                    parrafoVariable18 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             2,
                                                                             "#PARRAFO18#");


                    parrafoVariable18 = parrafoVariable18.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable18 = parrafoVariable18.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable18 = parrafoVariable18.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);

                }
                else if ((cliente.idTipoPersona == 2) && (firmante.representantesCliente.Count() == 2)) //persona juridica con dos representante
                {
                    parrafoVariable18 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             3,
                                                                             "#PARRAFO18#");

                    parrafoVariable18 = parrafoVariable18.Replace("#NombreCliente#", cliente.nombre);
                    parrafoVariable18 = parrafoVariable18.Replace("#FechaPersoneriaRepresentante1#", ((DateTime)firmante.representantesCliente[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable18 = parrafoVariable18.Replace("#NotariaRepresentante1#", firmante.representantesCliente[0].nombreNotariaPersoneria);
                    parrafoVariable18 = parrafoVariable18.Replace("#FechaPersoneriaRepresentante2#", ((DateTime)firmante.representantesCliente[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    parrafoVariable18 = parrafoVariable18.Replace("#NotariaRepresentante2#", firmante.representantesCliente[1].nombreNotariaPersoneria);

                }
                else
                {
                    parrafoVariable18 = "";
                }
                doc.Range.Replace("#PARRAFO18#", parrafoVariable18, options);
                #endregion

                #region Parrafo19

                string parrafoVariable19 = "";
                if (cantidadAvales > 0)
                {
                    if ((firmante.avales[0].idtipoPersona == 2) && (firmante.avales[0].representantesDelAval.Count() == 1))
                    //un aval persona juridica con un representante
                    {
                        parrafoVariable19 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 12,
                                                                                 "#PARRAFO19#");


                        parrafoVariable19 = parrafoVariable19.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable19 = parrafoVariable19.Replace("#FechaPersoneriaRepresentanteAval1#", ((DateTime)firmante.avales[0].representantesDelAval[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable19 = parrafoVariable19.Replace("#NotariaRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombreNotariaPersoneria);

                    }
                    else if ((firmante.avales[0].idtipoPersona == 2) && (firmante.avales[0].representantesDelAval.Count() == 2))
                    //un aval persona juridica con dos representante
                    {
                        parrafoVariable19 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                                 13,
                                                                                 "#PARRAFO19#");


                        parrafoVariable19 = parrafoVariable19.Replace("#NombreAval1#", firmante.avales[0].nombre);
                        parrafoVariable19 = parrafoVariable19.Replace("#FechaPersoneriaRepresentanteAval1#", ((DateTime)firmante.avales[0].representantesDelAval[0].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable19 = parrafoVariable19.Replace("#NotariaRepresentanteAval1#", firmante.avales[0].representantesDelAval[0].nombreNotariaPersoneria);
                        parrafoVariable19 = parrafoVariable19.Replace("#FechaPersoneriaRepresentanteAval2#", ((DateTime)firmante.avales[0].representantesDelAval[1].fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                        parrafoVariable19 = parrafoVariable19.Replace("#NotariaRepresentanteAval2#", firmante.avales[0].representantesDelAval[1].nombreNotariaPersoneria);

                    }
                }
                else
                {
                    parrafoVariable19 = "";
                }

                doc.Range.Replace("#PARRAFO19#", parrafoVariable19, options);
                #endregion


                #region Parrafo20

                string parrafoVariable20 = "";

                if (firmante.represenantesPenta.Count() == 1)
                //un aval persona juridica con un representante
                {
                    parrafoVariable20 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             6,
                                                                             "#PARRAFO20#");

                }
                else if (firmante.represenantesPenta.Count() == 2)
                //dos representantes de penta
                {
                    parrafoVariable20 = gestorDeContratos.ObtenerParrafoVariable(idContrato,
                                                                             7,
                                                                             "#PARRAFO20#");

                }


                doc.Range.Replace("#PARRAFO20#", parrafoVariable20, options);
                #endregion

                #region Firmas

                if (cliente.idTipoPersona == 2) //persona juridica
                {
                    if (firmante.representantesCliente.Count() == 1)
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente1PJ#", firmante.representantesCliente[0].nombre, options);
                        doc.Range.Replace("#NombreRepresentanteCliente2PJ#", "", options);
                        doc.Range.Replace("#NombreClientePJ#", "p.p. " + cliente.nombre, options);
                    }
                    if (firmante.representantesCliente.Count() == 2)
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente1PJ#", firmante.representantesCliente[0].nombre, options);
                        doc.Range.Replace("#NombreRepresentanteCliente2PJ#", firmante.representantesCliente[1].nombre, options);
                        doc.Range.Replace("#NombreClientePJ#", "p.p. " + cliente.nombre, options);
                    }
                    doc.Range.Replace("#NombreClientePN#", "", options);
                }
                else if (cliente.idTipoPersona == 1)
                {
                    doc.Range.Replace("#NombreRepresentanteCliente1PJ#", "", options);
                    doc.Range.Replace("#NombreRepresentanteCliente2PJ#", "", options);
                    doc.Range.Replace("#NombreClientePJ#", "", options);
                    doc.Range.Replace("#NombreClientePN#", cliente.nombre, options);
                }

                if (firmante.avales.Count() > 0) //existen avales 
                {
                    if ((firmante.avales.Count() == 1)) //un aval
                    {
                        if(firmante.avales[0].idtipoPersona == 1) //persona natural
                        {
                            doc.Range.Replace("#NombreAval1PN#", firmante.avales[0].nombre, options);
                            doc.Range.Replace("#NombreAval2PN#", "", options);
                            doc.Range.Replace("#PieFirmaAvalPN#", "por si como fiador y codeudor solidario", options);

                            doc.Range.Replace("#NombreRepresentanteAval1PN#", "", options);
                            doc.Range.Replace("#NombreRepresentanteAval2PN#", "", options);
                            doc.Range.Replace("#NombreAval#", "", options);
                            doc.Range.Replace("#PieFirmaAvalPJ#", "", options);

                            if ((firmante.avales[0].idEstadoCivil == 2) || (firmante.avales[0].idEstadoCivil == 4)) //casado
                            {
                                doc.Range.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre, options);
                                doc.Range.Replace("#NombreConyugeAval2#", "", options);
                                doc.Range.Replace("#PieFirmaAvalCasado#", "como cónyuge autorizante", options);
                            }
                            else
                            {
                                doc.Range.Replace("#NombreConyugeAval1#", "", options);
                                doc.Range.Replace("#NombreConyugeAval2#", "", options);
                                doc.Range.Replace("#PieFirmaAvalCasado#", "", options);
                            }
                        }
                        else if (firmante.avales[0].idtipoPersona == 2) //persona juridica
                        {
                            if (firmante.avales[0].representantesDelAval.Count() == 1)
                            {
                                doc.Range.Replace("#NombreRepresentanteAval1PN#", firmante.avales[0].representantesDelAval[0].nombre, options);
                                doc.Range.Replace("#NombreRepresentanteAval2PN#", "", options);
                                doc.Range.Replace("#NombreAval#", firmante.avales[0].nombre, options);
                                doc.Range.Replace("#PieFirmaAvalPJ#", "y por si como fiador y codeudor solidario", options);

                                doc.Range.Replace("#NombreAval1PN#", "", options);
                                doc.Range.Replace("#NombreAval2PN#", "", options);
                                doc.Range.Replace("#PieFirmaAvalPN#", "", options);
                            }
                            else if (firmante.avales[0].representantesDelAval.Count() == 2)
                            {
                                doc.Range.Replace("#NombreRepresentanteAval1PN#", firmante.avales[0].representantesDelAval[0].nombre, options);
                                doc.Range.Replace("#NombreRepresentanteAval2PN#", firmante.avales[0].representantesDelAval[1].nombre, options);
                                doc.Range.Replace("#NombreAval#", firmante.avales[0].nombre, options);
                                doc.Range.Replace("#PieFirmaAvalPJ#", "y por si como fiador y codeudor solidario", options);

                                doc.Range.Replace("#NombreAval1PN#", "", options);
                                doc.Range.Replace("#NombreAval2PN#", "", options);
                                doc.Range.Replace("#PieFirmaAvalPN#", "", options);
                            }
                        }
                    }
                    else if ((firmante.avales.Count() == 2) && (firmante.avales[0].idtipoPersona == 1) && (firmante.avales[1].idtipoPersona == 1)) //2 avales persona natural
                    {
                        doc.Range.Replace("#NombreAval1PN#", firmante.avales[0].nombre, options);
                        doc.Range.Replace("#NombreAval2PN#", firmante.avales[1].nombre, options);
                        doc.Range.Replace("#PieFirmaAval#", "por si como fiador y codeudor solidario", options);

                        doc.Range.Replace("#NombreRepresentanteAval1PN#", "", options);
                        doc.Range.Replace("#NombreRepresentanteAval2PN#", "", options);
                        doc.Range.Replace("#NombreAval#", "", options);
                        doc.Range.Replace("#PieFirmaAvalPJ#", "", options);
                        if (((firmante.avales[0].idEstadoCivil == 2) || (firmante.avales[0].idEstadoCivil == 4)) //uno casado 
                            &&((firmante.avales[1].idEstadoCivil != 2) && (firmante.avales[1].idEstadoCivil != 4))) //otro soltero
                        {
                            doc.Range.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre, options);
                            doc.Range.Replace("#NombreConyugeAval2#", "", options);
                            doc.Range.Replace("#PieFirmaAvalCasado#", "como cónyuge autorizante", options);
                        }
                        else if (((firmante.avales[1].idEstadoCivil == 2) || (firmante.avales[1].idEstadoCivil == 4)) //uno casado
                            &&((firmante.avales[0].idEstadoCivil != 2) && (firmante.avales[0].idEstadoCivil != 4))) //otro soltero
                        {
                            doc.Range.Replace("#NombreConyugeAval1#", "", options);
                            doc.Range.Replace("#NombreConyugeAval2#", firmante.avales[1].conyuge.nombre, options);
                            doc.Range.Replace("#PieFirmaAvalCasado#", "como cónyuge autorizante", options);
                        }
                        else if (((firmante.avales[1].idEstadoCivil == 2) || (firmante.avales[1].idEstadoCivil == 4)) //uno casado
                            && ((firmante.avales[0].idEstadoCivil == 2) && (firmante.avales[0].idEstadoCivil == 4))) //otro casado
                        {
                            doc.Range.Replace("#NombreConyugeAval1#", firmante.avales[0].conyuge.nombre, options);
                            doc.Range.Replace("#NombreConyugeAval2#", firmante.avales[1].conyuge.nombre, options);
                            doc.Range.Replace("#PieFirmaAvalCasado#", "como cónyuge autorizante", options);
                        }
                        else
                        {
                            doc.Range.Replace("#NombreConyugeAval1#", "", options);
                            doc.Range.Replace("#NombreConyugeAval2#", "", options);
                            doc.Range.Replace("#PieFirmaAvalCasado#", "", options);
                        }
                    }
                }
                else
                {
                    doc.Range.Replace("#NombreAval1PN#", "", options);
                    doc.Range.Replace("#NombreAval2PN#", "", options);
                    doc.Range.Replace("#PieFirmaAval#", "", options);
                    doc.Range.Replace("#NombreRepresentanteAval1PN#", "", options);
                    doc.Range.Replace("#NombreRepresentanteAval2PN#", "", options);
                    doc.Range.Replace("#NombreAval#", "", options);
                    doc.Range.Replace("#PieFirmaAvalPJ#", "", options);
                    doc.Range.Replace("#NombreConyugeAval1#", "", options);
                    doc.Range.Replace("#NombreConyugeAval2#", "", options);
                    doc.Range.Replace("#PieFirmaAvalCasado#", "", options);
                }


                
                #endregion


                string fileSaveAs = cliente.rut.Split('-')[0].Replace(".", "").ToString() + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;

                //generar PDF

                //Se agrega un salto de página, para posterior inserta las firmas digitales
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToDocumentEnd();
                builder.InsertBreak(BreakType.PageBreak);

                fileSaveAs = fileSaveAs + ".docx";
                fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                doc.Save(fileSaveAsPath);


                //Convertir en BASE64
                Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                String fileBase64 = Convert.ToBase64String(bytes);

                resultado = fileBase64;


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", Convert.ToInt32(cliente.rut.Split('-')[0].Replace(".", "")), System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + "WSPDF" + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ConvierteRutEnPalabras(string rutCompleto)
        {
            FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

            string rutClienteEnPalabras = funcionesUtiles.convertirNumeroEnPalabras(rutCompleto.Split('-')[0].Replace(".", "").ToString())
                        + " guión " + funcionesUtiles.convertirNumeroEnPalabras(rutCompleto.Split('-')[1].ToString());

            return rutClienteEnPalabras;
        }

        public string CadenaAleatoria(int longitud)
        {
            /*Texto Aleatorio*/
            Guid miGuid = Guid.NewGuid();
            string token = Convert.ToBase64String(miGuid.ToByteArray());
            token = token.Replace("=", "").Replace("+", "");
            return token.Substring(0, longitud);
        }

        public string DireccionEnPalabras(DireccionWSDTO direccion)
        {
            FuncionesUtiles funcionesUtiles = new FuncionesUtiles();
            string direccionEnPalabras = "";

            if (string.IsNullOrEmpty(direccion.deptoCasaOtro))
            {
                direccionEnPalabras = direccion.calle + " " + funcionesUtiles.convertirNumeroEnPalabras(direccion.numero) +
                          ", comuna de " + direccion.comuna + ", ciudad de " +
                         direccion.ciudad;
            }
            else
            {
                direccionEnPalabras = direccion.calle + " " + funcionesUtiles.convertirNumeroEnPalabras(direccion.numero) +
                            " piso/dpto/casa/oficina " + funcionesUtiles.convertirNumeroEnPalabras(direccion.deptoCasaOtro) + ", comuna de " + direccion.comuna + ", ciudad de " +
                            direccion.ciudad;
            }
            return direccionEnPalabras;
        }

    }
}
