﻿using Factoring.Utiles;
using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestorDeContratos.ServiceLayer;

namespace GestorDeContratos.Web.Controllers
{
    public class SucursalController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;

        public SucursalController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }

        //
        // GET: /Sucursal/

        public ActionResult Index()
        {
            PopulateNotarias();
            PopulateRepresentantesPenta();
            return View();
        }

        public JsonResult Sucursales_Read2()
        {
            IEnumerable<SucursalDTO> lista = new List<SucursalDTO>();
            lista = gestorDeContratos.obtenerSucursales();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Sucursales_Read([DataSourceRequest] DataSourceRequest request)
        {
            IEnumerable<SucursalDTO> lista = new List<SucursalDTO>();
            lista = gestorDeContratos.obtenerSucursales();
            return Json(lista.ToDataSourceResult(request));
            //return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Sucursal_Create([DataSourceRequest] DataSourceRequest request, SucursalDTO sucursal)
        {
            if (sucursal != null && ModelState.IsValid)
            {
                gestorDeContratos.agregarSucursal(sucursal.nombre, sucursal.direccion, sucursal.direccionPalabras, sucursal.comuna, sucursal.ciudad, sucursal.vigente, sucursal.idNotaria,
                    sucursal.rutRepresentantePenta1, sucursal.rutRepresentantePenta2);
            }
            return Json(new[] { sucursal }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Sucursal_Update([DataSourceRequest] DataSourceRequest request, SucursalDTO sucursal)
        {
            if (sucursal != null)
            {
                gestorDeContratos.actualizarSucursal(sucursal.idSucursal, sucursal.nombre, sucursal.direccion, sucursal.direccionPalabras, sucursal.comuna, sucursal.ciudad, sucursal.vigente,
                    sucursal.idNotaria, sucursal.rutRepresentantePenta1, sucursal.rutRepresentantePenta2);
            }
            return Json(new[] { sucursal }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Sucursal_Destroy([DataSourceRequest] DataSourceRequest request, SucursalDTO sucursal)
        {
            var results = new List<RepresentanteClienteDTO>();

            if (sucursal != null)
            {
                //gestorDeContratos.eliminarRepresentanteCliente(representante.rutCliente, representante.rutRepresentante);
                gestorDeContratos.eliminarSucursal(sucursal.idSucursal);
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        private void PopulateNotarias()
        {
            IEnumerable<NotariaDTO> lista = new List<NotariaDTO>();
            lista = gestorDeContratos.obtenerNotarias();

            ViewData["notarias"] = lista;
            ViewData["defaultNotarias"] = lista.First();
        }

        private void PopulateRepresentantesPenta()
        {
            IEnumerable<RepresentantePentaDTO> lista = new List<RepresentantePentaDTO>();
            lista = gestorDeContratos.obtenerRepresentantesPenta();

            ViewData["representantesPenta"] = lista;
            ViewData["defaultrepresentantesPenta"] = lista.First();
        }
    }
}
