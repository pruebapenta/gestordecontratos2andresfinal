﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GestorDeContratos.BLL.ServiceImplementation;
using GestorDeContratos.ServiceLayer.Service;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using Kendo.Mvc.Extensions;
using Microsoft.Office.Interop.Word;
using GestorDeContratos.ServiceLayer;
using Aspose.Words;
using Aspose.Words.Tables;
using Aspose.Words.Saving;
using Aspose.Words.Replacing;
using System.Drawing;
using System.Text;
using Kendo.Mvc.UI;
using System.Configuration;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace GestorDeContratos.Web.Controllers
{
    public class ContratosController : Controller
    {
        readonly IGestorDeContratosSvcImpl gestorDeContratos;

        public ContratosController()
        {
            gestorDeContratos = new GestorDeContratosSvcImpl();
        }

        public ActionResult Details(int id = 0)
        {
            ViewData["RutCliente"] = id;
            PopulateCategories();
            return View();
        }

        public ActionResult Firmados()
        {
            return View();
        }

        public ActionResult PorFirmar()
        {
            return View();
        }

        private void PopulateCategories()
        {
            IEnumerable<EstadoCivilDTO> lista = new List<EstadoCivilDTO>();
            lista = gestorDeContratos.obtenerEstadosCiviles();

            ViewData["categories"] = lista;
            ViewData["defaultCategory"] = lista.First();
        }

        public ActionResult ContratosFEGenerados_Read([DataSourceRequest] DataSourceRequest request, int rutCliente)
        {
            IEnumerable<ContratoFEGeneradoDTO> lista = new List<ContratoFEGeneradoDTO>();
            lista = gestorDeContratos.obtenerContratosFEGenerados(rutCliente);
            return Json(lista.ToDataSourceResult(request));
        }

        public ActionResult ContratosFirmados_Read([DataSourceRequest] DataSourceRequest request)
        {
            IEnumerable<ContratoFirmadoDTO> lista = new List<ContratoFirmadoDTO>();
            lista = gestorDeContratos.obtenerContratosFirmados();
            return Json(lista.ToDataSourceResult(request));
        }

        public ActionResult ContratosPorFirmar_Read([DataSourceRequest] DataSourceRequest request)
        {
            IEnumerable<ContratoPorFirmarDTO> lista = new List<ContratoPorFirmarDTO>();
            lista = gestorDeContratos.obtenerContratosPorFirmar();
            return Json(lista.ToDataSourceResult(request));
        }

        public ActionResult ContratosPorFirmarFirmantes_Read([DataSourceRequest] DataSourceRequest request, int idContratoEnviado)
        {
            IEnumerable<ContratoPorFirmarFirmanteDTO> lista = new List<ContratoPorFirmarFirmanteDTO>();
            lista = gestorDeContratos.obtenerContratosPorFirmar_Firmantes(idContratoEnviado);
            return Json(lista.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContratosPorFirmar_Destroy([DataSourceRequest] DataSourceRequest request, ContratoPorFirmarDTO contratoPorFirmar)
        {
            var results = new List<RepresentanteClienteDTO>();

            if (contratoPorFirmar != null)
            {
                gestorDeContratos.eliminarContratoPorFirmar(contratoPorFirmar.idContratoEnviado);
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        //Visualizar Contrato Firma Electrónica
        public string Ver(int id = 0)
        {
            string resultado = "";

            IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
            contratoEnviado = gestorDeContratos.obtenerContratoEnviadoDetalle(id);

            if (contratoEnviado.Count() > 0)
            {
                Response.Clear();
                Response.ContentType = "Application/pdf";
                Response.AppendHeader("Content-Disposition", "inline; filename=" + contratoEnviado.FirstOrDefault().rutCliente + "_" + id + ".pdf");
                Response.BinaryWrite(System.Convert.FromBase64String(contratoEnviado.FirstOrDefault().contratoBase64));
                Response.Flush();
                Response.End();
            }
            else
            {
                resultado = "<p>CONTRATO NO ENCONTRADO!!</p>";
            }

            return resultado;
        }

        //LINEAS
        public string CartaBancoLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<BancoClienteDTO> bancosCliente = new List<BancoClienteDTO>();
                bancosCliente = gestorDeContratos.obtenerBancoCliente(rutCliente);

                string plantilla = "CartaBanco.docx";

                //string dirPlantillas = Server.MapPath(@"~/Plantillas/").ToString();
                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#Nombrecliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representante 1
                if (representante1.Count() > 0)
                {
                    using (var repP = representante1.FirstOrDefault())
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NombreRepresentanteCliente1#", "", options);
                }

                //Datos Representante 2
                if (representante2.Count() > 0)
                {
                    using (var repP = representante2.FirstOrDefault())
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente2#", representante2.FirstOrDefault().nombre, options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NombreRepresentanteCliente2#", "", options);
                }


                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToBookmark("CuentasBanco");
                builder.ParagraphFormat.ClearFormatting();
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;
                builder.Bold = true;

                builder.InsertCell();
                builder.Write("BANCO");
                builder.InsertCell();
                builder.Write("CUENTA");
                builder.InsertCell();
                builder.Write("TIPO CUENTA");
                builder.EndRow();

                builder.Bold = false;
                foreach (BancoClienteDTO bancoC in bancosCliente)
                {
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                    //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.5);
                    builder.Write(bancoC.banco);
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                    //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(4);
                    builder.Write(bancoC.cuenta);
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                    //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(30.5);
                    builder.Write(bancoC.tipoCuenta);
                    builder.EndRow();
                }
                builder.EndTable();


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea=1 - En curse=2 - Cursada=3
                            , 7 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count() //operacionDetalle.Count()
                            , 0 // operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string CartaPoderLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();


                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (!(cliente.FirstOrDefault().autorizaCartaPoder.Length > 0))
                {
                    resultado += WebUtility.HtmlDecode("<p>Autoriza carta poder <strong>NO INGRESADO</strong>!</p>");
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "";
                //Si el cliente es persona natural
                if (formato.ToUpper() == "SEND-PDF")
                {
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        plantilla = "CartaPoder.FE.Nat.docx";
                    }
                    else
                    {
                        plantilla = "CartaPoder.FE.docx";
                    }
                }
                else
                {
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        plantilla = "CartaPoder.Nat.docx";
                    }
                    else
                    {
                        plantilla = "CartaPoder.docx";
                    }
                }

                //object fileName = (Server.MapPath(@"~/Plantillas/CartaPoder.docx"));
                //object fileName = (Server.MapPath(@"~/Plantillas/" + plantilla));
                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                //Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application { Visible = false };
                //Microsoft.Office.Interop.Word.Document aDoc = wordApp.Documents.Open(fileName, ReadOnly: true, Visible: true);
                //aDoc.Activate();

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                //Aspose.Words.Document doc = new Aspose.Words.Document(fileName);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#Nombrecliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#Rutcliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#AutorizaCartaPoder#", cliente.FirstOrDefault().autorizaCartaPoder, options);
                doc.Range.Replace("#Email1#", cliente.FirstOrDefault().email1, options);
                //FindAndReplace(wordApp, "#Email2#", cliente.FirstOrDefault().email2);
                doc.Range.Replace("#Email2#", (
                    string.IsNullOrEmpty(cliente.FirstOrDefault().email2) ? cliente.FirstOrDefault().email1 : cliente.FirstOrDefault().email2 
                ), options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Cliente Persona Natural
                if (!(rutR1 > 0 && rutR2 > 0))
                {
                    doc.Range.Replace("#NacionalidadCliente#", ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural, options);
                    doc.Range.Replace("#EstadoCivilCliente#", ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural, options);
                    doc.Range.Replace("#ProfesionCliente#", ", " + cliente.FirstOrDefault().profesionPersonaNatural, options);

                    if (cliente.FirstOrDefault().idEstadoCivilPersonaNatural == 1)
                    {
                        doc.Range.Replace("#NOMBRECONYUGE#", "don(ña) " + cliente.FirstOrDefault().nombreConyuge, options);
                        doc.Range.Replace("#PIENOMBRECONYUGE#", cliente.FirstOrDefault().nombreConyuge.ToUpper(), options);
                        doc.Range.Replace("#NacionalidadConyuge#", ", " + cliente.FirstOrDefault().nacionalidadConyuge, options);
                        doc.Range.Replace("#CasadoConyuge#", ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural + " con el constituyente ", options);
                        doc.Range.Replace("#NombreClienteConyu#", "don(ña) " + cliente.FirstOrDefault().razonSocial, options);
                        doc.Range.Replace("#RutConyuge#", ", cédula nacional de identidad número " + funcionesUtiles.convertirRutEnPalabras((int)cliente.FirstOrDefault().rutConyuge, cliente.FirstOrDefault().dvConyuge.ToString()) + ", y de su mismo domicilio", options);
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE#", "", options);
                        doc.Range.Replace("#PIENOMBRECONYUGE#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge#", "", options);
                        doc.Range.Replace("#CasadoConyuge#", "", options);
                        doc.Range.Replace("#NombreClienteConyu#", "", options);
                        doc.Range.Replace("#RutConyuge#", "", options);
                    }
                }

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentantesMinimos = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre + ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    datosRepresentantesMinimos += "don(ña) " + representante1.FirstOrDefault().nombre;

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        datosRepresentantesMinimos += ", y don(ña) " + representante2.FirstOrDefault().nombre;
                        doc.Range.Replace("#ambos#", "ambos", options);
                        doc.Range.Replace("#de/delosrepesentantes#", "de los representantes", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria + " y " + representante2.FirstOrDefault().notariaPersoneria, options);
                    }
                    else
                    {
                        doc.Range.Replace("#ambos#", "", options);
                        doc.Range.Replace("#de/delosrepesentantes#", "del representante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    }
                    doc.Range.Replace("#DatosMinimosRepresentantes#", datosRepresentantesMinimos, options);
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);
                }

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato 
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea - En curse - Cursada
                            , 4 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count() //operacionDetalle.Count()
                            , 0 // operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoCesionLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();


                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "";
                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "ContratoCesion.Nat.docx";
                }
                else
                {
                    plantilla = "ContratoCesion.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);

                doc.Range.Replace("#DatosCliente#",
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural +
                    ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural, options
                );

                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + 
                    " " + cliente.FirstOrDefault().numeroCalle + 
                    " " + cliente.FirstOrDefault().letraCalle + 
                    ", comuna de " + cliente.FirstOrDefault().comuna + 
                    ", ciudad de " + cliente.FirstOrDefault().ciudad, options
                );
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#NotariaNombreNotario#", notario.FirstOrDefault().nombreNotario, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representantes
                var datosRepresentantes = "";
                //var datosRepresentantesMinimos = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante1#",
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper(), options
                    );
                    //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante;
                        doc.Range.Replace("#YPor#", " y por ", options);
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante2#",
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper(), options
                        );
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                        //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                        doc.Range.Replace("#ambos#", "ambos ", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "de los representantes");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente",
                            options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPor#", "", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#DatosRepresentante2#", "", options);
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                        doc.Range.Replace("#ambos#", "", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    }
                    //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    if (rutR2 > 0)
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                        doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                    }
                    else
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                        doc.Range.Replace("#AmbosPP#", "", options);
                    }
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoFactoringConResponsabilidadLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();


                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                    if (representante1.FirstOrDefault().idEstadoCivil == 1 && //representante1.FirstOrDefault().fiador &&
                        (string.IsNullOrEmpty(representante1.FirstOrDefault().rutConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().dvConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().nombreConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidadConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().profesionConyuge.ToString())
                        )
                    )
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> es casado y faltan datos de su conyuge!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante " + representante2.FirstOrDefault().nombre.ToUpper() + " no tiene fecha personería!</p>");
                    }
                    if (representante2.FirstOrDefault().idEstadoCivil == 1 && //representante2.FirstOrDefault().fiador &&
                        (string.IsNullOrEmpty(representante2.FirstOrDefault().rutConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().dvConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().nombreConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidadConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().profesionConyuge.ToString())
                        )
                    )
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> es casado y faltan datos de su conyuge!</p>");
                    }
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }


                string plantilla = "";
                bool fiadores = false;
                bool casados = false;

                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    if (cliente.FirstOrDefault().idEstadoCivilPersonaNatural == 1)//Si es casado
                    {
                        plantilla = "ContratoFactoringConResponsabilidad.Nat.Casado.docx";
                    }
                    else
                    {
                        plantilla = "ContratoFactoringConResponsabilidad.Nat.docx";
                    }
                }
                else
                {
                    //Si los representantes son fiadores y/o casados (Estado civil=1)
                    if (representante1.FirstOrDefault().fiador) fiadores = true;
                    if (representante1.FirstOrDefault().idEstadoCivil == 1) casados = true;
                    if (representante2.Count() > 0)
                    {
                        if (representante2.FirstOrDefault().fiador) fiadores = true;
                        if (representante2.FirstOrDefault().idEstadoCivil == 1) casados = true;
                    }

                    if (fiadores && casados) plantilla = "ContratoFactoringConResponsabilidad.Rep.Fiador.Casado.docx";
                    //if (!fiadores && casados) plantilla = "ContratoFactoringConResponsabilidad.Rep.Casado.docx";
                    if (!fiadores && casados) plantilla = "ContratoFactoringConResponsabilidad.Rep.docx"; //Solicitado por RIraIra, si no es fiador y es casado.
                    if (fiadores && !casados) plantilla = "ContratoFactoringConResponsabilidad.Rep.Fiador.docx";
                    if (!fiadores && !casados) plantilla = "ContratoFactoringConResponsabilidad.Rep.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                //FindAndReplace(wordApp, "#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);
                //FindAndReplace(wordApp, "#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccionPalabras, options); // + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);


                //Datos Cliente Persona Natural
                if (!(rutR1 > 0 && rutR2 > 0))
                {
                    doc.Range.Replace("#NacionalidadCliente#", ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural, options);
                    doc.Range.Replace("#EstadoCivilCliente#", ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural, options);
                    doc.Range.Replace("#ProfesionCliente#", ", " + cliente.FirstOrDefault().profesionPersonaNatural, options);

                    if (cliente.FirstOrDefault().idEstadoCivilPersonaNatural == 1)
                    {
                        doc.Range.Replace("#NOMBRECONYUGE#", "don(ña) " + cliente.FirstOrDefault().nombreConyuge, options);
                        doc.Range.Replace("#PIENOMBRECONYUGE#", cliente.FirstOrDefault().nombreConyuge.ToUpper(), options);
                        doc.Range.Replace("#NacionalidadConyuge#", ", " + cliente.FirstOrDefault().nacionalidadConyuge, options);
                        doc.Range.Replace("#CasadoConyuge#", ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural + " con el constituyente ", options);
                        doc.Range.Replace("#NombreClienteConyu#", "don(ña) " + cliente.FirstOrDefault().razonSocial, options);
                        doc.Range.Replace("#RutConyuge#", ", cédula nacional de identidad número " + funcionesUtiles.convertirRutEnPalabras((int)cliente.FirstOrDefault().rutConyuge, cliente.FirstOrDefault().dvConyuge.ToString()) + ", y de su mismo domicilio", options);
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE#", "", options);
                        doc.Range.Replace("#PIENOMBRECONYUGE#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge#", "", options);
                        doc.Range.Replace("#CasadoConyuge#", "", options);
                        doc.Range.Replace("#NombreClienteConyu#", "", options);
                        doc.Range.Replace("#RutConyuge#", "", options);
                    }
                }

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                int cantidadAvales = 0;

                if (rutR1 > 0)
                {
                    //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#PieNombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().rutPalabra))
                        datosRepresentante1 += ", cédula de identidad número " + representante1.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DomicilioAval1#",
                            representante1.FirstOrDefault().nombreCalle +
                            //" " + representante1.FirstOrDefault().numeroCalle + 
                            " " + funcionesUtiles.convertirNumeroEnPalabras(representante1.FirstOrDefault().numeroCalle) +
                            " " + representante1.FirstOrDefault().letraCalle +
                            ", comuna " + representante1.FirstOrDefault().comuna +
                            ", " + representante1.FirstOrDefault().ciudad
                            , options
                        );
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "", options);
                        doc.Range.Replace("#DomicilioAval1#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }


                //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#NombreRepresentante2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#PieNombreRepresentante2#", "\t" + representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().rutPalabra))
                        datosRepresentante2 += ", cédula de identidad número " + representante2.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);


                    datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#YPor#", " y por ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante2#",
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    doc.Range.Replace("#de/delosrepesentantes#", "de los representantes", options);
                    //FindAndReplace(wordApp, "#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    doc.Range.Replace("#FechaPersoneria#",
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                        ", y de " +
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                        , options
                    );
                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria +
                        " y " + 
                        representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );

                    //Datos como AVAl2
                    if (representante2.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#", 
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle + 
                                //" " + representante2.FirstOrDefault().numeroCalle + 
                                " " + funcionesUtiles.convertirNumeroEnPalabras(representante2.FirstOrDefault().numeroCalle) + 
                                " " + representante2.FirstOrDefault().letraCalle + 
                                ", comuna " + representante2.FirstOrDefault().comuna + 
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                            doc.Range.Replace("#Individualizado/sYDomiciliado/s#", "individualizados y domiciliados", options);
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#", 
                                representante2.FirstOrDefault().nombreCalle + 
                                //" " + representante2.FirstOrDefault().numeroCalle + 
                                " " + funcionesUtiles.convertirNumeroEnPalabras(representante2.FirstOrDefault().numeroCalle) + 
                                " " + representante2.FirstOrDefault().letraCalle + 
                                ", comuna " + representante2.FirstOrDefault().comuna + 
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                            doc.Range.Replace("#Individualizado/sYDomiciliado/s#", "individualizado y domiciliado", options);
                        }
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                        doc.Range.Replace("#DomicilioAval2#", "", options);
                        doc.Range.Replace("#Individualizado/sYDomiciliado/s#", "individualizado y domiciliado", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#PieNombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    doc.Range.Replace("#de/delosrepesentantes#", "del representante", options);
                    //FindAndReplace(wordApp, "#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    if (rutR1 > 0)
                    {
                        doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    }
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    doc.Range.Replace("#DomicilioAval2#", "", options);
                    doc.Range.Replace("#Individualizado/sYDomiciliado/s#", "individualizado y domiciliado", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#PIENOMBREREPRESENTANTEPENTA1#", repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#PIENOMBREREPRESENTANTEPENTA2#", "\t" + repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }


                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0 && repC.idEstadoCivil == 1)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#PIENOMBRECONYUGE1#", repC.nombreConyuge.ToUpper(), options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes.", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#PIENOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0 && repC.idEstadoCivil == 1)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#PIENOMBRECONYUGE2#", "\t" + repC.nombreConyuge.ToUpper(), options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes.", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#PIENOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }

                if (rutR1 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                }
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                //Modifica la personeria de PFSA dependiendo de la cantidad de representantes
                if (sucursal.FirstOrDefault().rutRepresentantePenta1 > 0 && sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    doc.Range.Replace("#Del/DelosRepresentantePF/s#", "de los representantes", options);
                }
                else
                {
                    doc.Range.Replace("#Del/DelosRepresentantePF/s#", "del representante", options);
                }


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoMarcoInstPrivadoLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();


                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                    if (representante1.FirstOrDefault().idEstadoCivil == 1 && //representante1.FirstOrDefault().fiador &&
                        (string.IsNullOrEmpty(representante1.FirstOrDefault().rutConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().dvConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().nombreConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidadConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().profesionConyuge.ToString())
                        )
                    )
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> es casado y faltan datos de su conyuge!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante " + representante2.FirstOrDefault().nombre.ToUpper() + " no tiene fecha personería!</p>");
                    }
                    if (representante2.FirstOrDefault().idEstadoCivil == 1 && //representante2.FirstOrDefault().fiador &&
                        (string.IsNullOrEmpty(representante2.FirstOrDefault().rutConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().dvConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().nombreConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidadConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().profesionConyuge.ToString())
                        )
                    )
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> es casado y faltan datos de su conyuge!</p>");
                    }
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }


                string plantilla = "";
                bool fiadores = false;
                bool casados = false;

                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    if (cliente.FirstOrDefault().idEstadoCivilPersonaNatural == 1)//Si es casado
                    {
                        plantilla = "ContratoMarcoInstPrivado.Nat.Casado.docx";
                    }
                    else
                    {
                        plantilla = "ContratoMarcoInstPrivado.Nat.docx";
                    }
                }
                else
                {
                    //Si los representantes son fiadores y/o casados (Estado civil=1)
                    if (representante1.FirstOrDefault().fiador) fiadores = true;
                    if (representante1.FirstOrDefault().idEstadoCivil == 1) casados = true;
                    if (representante2.Count() > 0)
                    {
                        if (representante2.FirstOrDefault().fiador) fiadores = true;
                        if (representante2.FirstOrDefault().idEstadoCivil == 1) casados = true;
                    }

                    if (fiadores && casados) plantilla = "ContratoMarcoInstPrivado.Jur.Fiador.Casado.docx";
                    //if (!fiadores && casados) plantilla = "ContratoMarcoInstPrivado.Jur.Casado.docx";
                    if (!fiadores && casados) plantilla = "ContratoMarcoInstPrivado.Jur.docx"; //Solicitado por RIraIra, cuando el rep. no es fiador y es casado.
                    if (fiadores && !casados) plantilla = "ContratoMarcoInstPrivado.Jur.Fiador.docx";
                    if (!fiadores && !casados) plantilla = "ContratoMarcoInstPrivado.Jur.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                //Aspose.Words.Document doc = new Aspose.Words.Document(fileName);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                //FindAndReplace(wordApp, "#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle, options);
                doc.Range.Replace("#ComunaCliente#", cliente.FirstOrDefault().comuna.ToUpper(), options);
                doc.Range.Replace("#CiudadCliente#", cliente.FirstOrDefault().ciudad.ToUpper(), options);
                //FindAndReplace(wordApp, "#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                //doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccionPalabras, options); // + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", comuna de " + sucursal.FirstOrDefault().comuna + ", ciudad de " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);


                //Datos Cliente Persona Natural
                if (!(rutR1 > 0 && rutR2 > 0))
                {
                    doc.Range.Replace("#NacionalidadCliente#", cliente.FirstOrDefault().nacionalidadPersonaNatural, options);
                    doc.Range.Replace("#EstadoCivilCliente#", cliente.FirstOrDefault().estadoCivilPersonaNatural, options);
                    doc.Range.Replace("#ProfesionCliente#", cliente.FirstOrDefault().profesionPersonaNatural, options);

                    if (cliente.FirstOrDefault().idEstadoCivilPersonaNatural == 1)
                    {
                        doc.Range.Replace("#NOMBRECONYUGE#", "doña " + cliente.FirstOrDefault().nombreConyuge, options);
                        doc.Range.Replace("#PIENOMBRECONYUGE#", cliente.FirstOrDefault().nombreConyuge.ToUpper(), options);
                        doc.Range.Replace("#NacionalidadConyuge#", ", " + cliente.FirstOrDefault().nacionalidadConyuge, options);
                        doc.Range.Replace("#CasadoConyuge#", ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural + " con el constituyente ", options);
                        doc.Range.Replace("#NombreClienteConyu#", "don " + cliente.FirstOrDefault().razonSocial, options);
                        doc.Range.Replace("#RutConyuge#", ", cédula nacional de identidad número " + string.Format("{0:N0}", cliente.FirstOrDefault().rutConyuge) + "-" + cliente.FirstOrDefault().dvConyuge.ToString() + ", y de su mismo domicilio", options);
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE#", "", options);
                        doc.Range.Replace("#PIENOMBRECONYUGE#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge#", "", options);
                        doc.Range.Replace("#CasadoConyuge#", "", options);
                        doc.Range.Replace("#NombreClienteConyu#", "", options);
                        doc.Range.Replace("#RutConyuge#", "", options);
                    }
                }

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                int cantidadAvales = 0;

                if (rutR1 > 0)
                {
                    //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#PieNombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                    datosRepresentante1 += ", C.N.I " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante;
                    doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DomicilioAval1#",
                            representante1.FirstOrDefault().nombreCalle +
                            " " + representante1.FirstOrDefault().numeroCalle + 
                            //" " + funcionesUtiles.convertirNumeroEnPalabras(representante1.FirstOrDefault().numeroCalle) +
                            " " + representante1.FirstOrDefault().letraCalle +
                            ", comuna de " + representante1.FirstOrDefault().comuna +
                            ", ciudad de " + representante1.FirstOrDefault().ciudad
                            , options
                        );
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "", options);
                        doc.Range.Replace("#DomicilioAval1#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }


                if (rutR2 > 0)
                {
                    doc.Range.Replace("#YPor#", " y por ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#PieNombreRepresentante2#", "\t" + representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    datosRepresentante2 += ", C.N.I " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);

                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    doc.Range.Replace("#de/delosrepesentantes#", "de los representantes", options);
                    //FindAndReplace(wordApp, "#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    doc.Range.Replace("#FechaPersoneria#",
                        ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                        ", y de " +
                        ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                        , options
                    );
                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria +
                        " y " +
                        representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );

                    //Datos como AVAl2
                    if (representante2.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle + 
                                //" " + funcionesUtiles.convertirNumeroEnPalabras(representante2.FirstOrDefault().numeroCalle) +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", comuna de " + representante2.FirstOrDefault().comuna +
                                ", ciudad de " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                            doc.Range.Replace("#Individualizado/sYDomiciliado/s#", "individualizados y domiciliados", options);
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                representante2.FirstOrDefault().nombreCalle +
                                //" " + representante2.FirstOrDefault().numeroCalle + 
                                " " + funcionesUtiles.convertirNumeroEnPalabras(representante2.FirstOrDefault().numeroCalle) +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", comuna de " + representante2.FirstOrDefault().comuna +
                                ", ciudad de " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                            doc.Range.Replace("#Individualizado/sYDomiciliado/s#", "individualizado y domiciliado", options);
                        }
                        cantidadAvales += 1;
                        if (cantidadAvales == 2)
                        {
                            doc.Range.Replace("#cadaunodeellos#", "cada uno de ellos, ", options);
                            doc.Range.Replace("#respectivamenteAval#", " respectivamente", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                        doc.Range.Replace("#DomicilioAval2#", "", options);
                        doc.Range.Replace("#Individualizado/sYDomiciliado/s#", "individualizado y domiciliado", options);
                        doc.Range.Replace("#cadaunodeellos#", "", options);
                        doc.Range.Replace("#respectivamenteAval#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#PieNombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    doc.Range.Replace("#de/delosrepesentantes#", "del representante", options);
                    //FindAndReplace(wordApp, "#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    if (rutR1 > 0)
                    {
                        //doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    }
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    doc.Range.Replace("#DomicilioAval2#", "", options);
                    doc.Range.Replace("#Individualizado/sYDomiciliado/s#", "individualizado y domiciliado", options);
                    doc.Range.Replace("#cadaunodeellos#", "", options);
                    doc.Range.Replace("#respectivamenteAval#", "", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#PIENOMBREREPRESENTANTEPENTA1#", repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                    //if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    //if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    //if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    datosRepresentantePenta += ", C.N.I " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#PIENOMBREREPRESENTANTEPENTA2#", "\t" + repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                        //if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        //if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        //if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        datosRepresentantePenta += ", C.N.I " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }


                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0 && repC.idEstadoCivil == 1)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "doña " + repC.nombreConyuge, options);
                            doc.Range.Replace("#PIENOMBRECONYUGE1#", repC.nombreConyuge.ToUpper(), options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            //doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil, options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", ", casada con don " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + string.Format("{0:N0}", repC.rutConyuge) + "-" + repC.dvConyuge, options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", y de su mismo domicilio, quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes.", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#PIENOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        //doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0 && repC.idEstadoCivil == 1)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "doña " + repC.nombreConyuge, options);
                            doc.Range.Replace("#PIENOMBRECONYUGE2#", "\t" + repC.nombreConyuge.ToUpper(), options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            //doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil, options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", ", casada con don " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + string.Format("{0:N0}", repC.rutConyuge) + "-" + repC.dvConyuge, options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", y de su mismo domicilio, quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes.", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#PIENOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }

                if (rutR1 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                }
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                //Modifica la personeria de PFSA dependiendo de la cantidad de representantes
                if (sucursal.FirstOrDefault().rutRepresentantePenta1 > 0 && sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    doc.Range.Replace("#Del/DelosRepresentantePF/s#", "de los representantes", options);
                }
                else
                {
                    doc.Range.Replace("#Del/DelosRepresentantePF/s#", "del representante", options);
                }


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea=1 - En curse=2 - Cursada=3
                            , 2 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count() //operacionDetalle.Count()
                            , 0 // operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        //Console.WriteLine(cliente.FirstOrDefault().idEstadoCivilPersonaNatural.ToString());
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                        if (cliente.FirstOrDefault().idEstadoCivilPersonaNatural == 1 && (int)cliente.FirstOrDefault().rutConyuge > 0)
                        {
                            gestorDeContratos.agregarContratoEnviadoFirma(
                                contratoEnviado.FirstOrDefault().idContratoEnviado
                                , (int)cliente.FirstOrDefault().rutConyuge //rutCliente
                                , 1
                            );
                        }
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                        if (representante1.FirstOrDefault().idEstadoCivil == 1)
                        {
                            gestorDeContratos.agregarContratoEnviadoFirma(
                                contratoEnviado.FirstOrDefault().idContratoEnviado
                                , (int)representante1.FirstOrDefault().rutConyuge
                                , 1
                            );
                        }
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                        if (representante2.FirstOrDefault().idEstadoCivil == 1)
                        {
                            gestorDeContratos.agregarContratoEnviadoFirma(
                                contratoEnviado.FirstOrDefault().idContratoEnviado
                                , (int)representante2.FirstOrDefault().rutConyuge
                                , 1
                            );
                        }
                    }

                    //Representantes PFSA
                    if (representantePenta1.Count() > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representantePenta1.FirstOrDefault().rut, 2);
                    }
                    if (representantePenta2.Count() > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representantePenta2.FirstOrDefault().rut, 2);
                    }

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 15398791, 3); //KARINA MARLENE Peña Muñoz

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string AnexoCobranzaExternaLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);


                string plantilla = "AnexoCobranzaExterna.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                //string dirPlantillas = Server.MapPath(@"~/planti/").ToString();
                //string dirPlantillas = Url.Content("http://srvwebtest/GestorDeContratos2/Template/");
                //Response.Write(dirPlantillas);
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                //Aspose.Words.Document doc = new Aspose.Words.Document(fileName);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                //FindAndReplace(wordApp, "#Sucursal#", sucursal.FirstOrDefault().nombre);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                //FindAndReplace(wordApp, "#AutorizaCartaPoder#", cliente.FirstOrDefault().autorizaCartaPoder);
                //FindAndReplace(wordApp, "#Email1#", cliente.FirstOrDefault().email1);
                //FindAndReplace(wordApp, "#Email2#", cliente.FirstOrDefault().email2);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representante 1
                if (representante1.Count() > 0)
                {
                    using (var repP = representante1.FirstOrDefault())
                    {
                        doc.Range.Replace("#Representante1#", "Representante 1", options);
                        doc.Range.Replace("#NombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    }
                }
                else
                {
                    doc.Range.Replace("#Representante1#", "", options);
                    doc.Range.Replace("#NombreRepresentanteCliente1#", "", options);
                }

                //Datos Representante 2
                if (representante2.Count() > 0)
                {
                    using (var repP = representante2.FirstOrDefault())
                    {
                        doc.Range.Replace("#Representante2#", "Representante 2", options);
                        doc.Range.Replace("#NombreRepresentanteCliente2#", representante2.FirstOrDefault().nombre, options);
                    }
                }
                else
                {
                    doc.Range.Replace("#Representante2#", "", options);
                    doc.Range.Replace("#NombreRepresentanteCliente2#", "", options);
                }


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea=1 - En curse=2 - Cursada=3
                            , 6 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count() //operacionDetalle.Count()
                            , 0 // operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string FormularioRegistroDeFirmaLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {

                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);


                string plantilla = "FormularioRegistroDeFirma.xls";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);


                //FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                //var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var templateWorkbook = new HSSFWorkbook(fs, true);
                Sheet hoja = templateWorkbook.GetSheet("Sheet1");

                NPOI.SS.UserModel.Row row;
                NPOI.SS.UserModel.Cell cell;

                //Nombre Cliente
                row = hoja.GetRow(4);
                cell = row.GetCell(1);
                cell.SetCellValue(cliente.FirstOrDefault().razonSocial.ToString());

                //Rut Cliente
                row = hoja.GetRow(4);
                cell = row.GetCell(8);
                cell.SetCellValue(string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper());


                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    //Datos Cliente Persona Natural
                    using (var repC = cliente.FirstOrDefault())
                    {
                        if (repC != null)
                        {
                            //Nombre Representante
                            row = hoja.GetRow(14);
                            cell = row.GetCell(1);
                            cell.SetCellValue(repC.razonSocial.ToString());

                            //Rut Representante
                            row = hoja.GetRow(17);
                            cell = row.GetCell(1);
                            cell.SetCellValue(string.Format("{0:N0}", repC.rut) + "-" + repC.dv.ToUpper());

                            //Estado Civil Representante
                            row = hoja.GetRow(17);
                            cell = row.GetCell(3);
                            cell.SetCellValue(repC.estadoCivilPersonaNatural.ToString());
                        }
                    }
                }
                else
                {
                    //Representante 1
                    using (var repC = representante1.FirstOrDefault())
                    {
                        if (repC != null)
                        {
                            //Nombre Representante
                            row = hoja.GetRow(14);
                            cell = row.GetCell(1);
                            cell.SetCellValue(repC.nombre.ToString());

                            //Rut Representante
                            row = hoja.GetRow(17);
                            cell = row.GetCell(1);
                            cell.SetCellValue(string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper());

                            //Estado Civil Representante
                            row = hoja.GetRow(17);
                            cell = row.GetCell(3);
                            cell.SetCellValue(repC.estadoCivil);
                        }
                    }

                    //Representante 2
                    using (var repC = representante2.FirstOrDefault())
                    {
                        if (repC != null)
                        {
                            //Nombre Representante
                            row = hoja.GetRow(14);
                            cell = row.GetCell(6);
                            cell.SetCellValue(repC.nombre.ToString());

                            //Rut Representante
                            row = hoja.GetRow(17);
                            cell = row.GetCell(6);
                            cell.SetCellValue(string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper());

                            //Estado Civil Representante
                            row = hoja.GetRow(17);
                            cell = row.GetCell(7);
                            cell.SetCellValue(repC.estadoCivil);
                        }
                    }
                }

                MemoryStream ms = new MemoryStream();

                //nuevo archivo
                String strNombreArchivo = dirDocumentosTemp + rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name + ".xls";
                System.IO.FileStream archivoSalida = System.IO.File.Create(strNombreArchivo);
                fs.Close();
                templateWorkbook.Write(archivoSalida);
                archivoSalida.Close();

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                //string fileSaveAsPath = "";
                if (formato.ToUpper() == "EXCEL")
                {
                    fileSaveAs = fileSaveAs + ".xls";

                    Response.Clear();
                    //Response.ContentType = "Application/ms-excel";
                    Response.ContentType = "Application/vnd.ms-excel";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(strNombreArchivo);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string nominaConfirmingENLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                //Validar notaria
                if (notario.FirstOrDefault().nombreNotario == null)
                {
                    resultado += WebUtility.HtmlDecode("<p>El cliente no tiene <strong>notaria asignada</strong>!</p>");
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "";
                plantilla = "NominaConfirming.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DatosCliente#",
                    ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle +
                    " " + cliente.FirstOrDefault().numeroCalle +
                    " " + cliente.FirstOrDefault().letraCalle +
                    ", comuna de " + cliente.FirstOrDefault().comuna +
                    ", ciudad de " + cliente.FirstOrDefault().ciudad
                    , options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#NotariaNombreNotario#", notario.FirstOrDefault().nombreNotario, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //doc.Range.Replace("#RepresentantePenta#", representantePenta1.FirstOrDefault().nombreCompleto, options);
                //doc.Range.Replace("#RepresentantePentaRut#", string.Format("{0:N0}", representantePenta1.FirstOrDefault().rut) + "-" + representantePenta1.FirstOrDefault().dv.ToUpper(), options);

                //Datos Representantes
                var datosRepresentantes = "";
                //var datosRepresentantesMinimos = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante1#",
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante;
                        doc.Range.Replace("#YPor#", " y por ", options);
                        doc.Range.Replace("#NombreRepresentante2#", " ; " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante2#",
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                        //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                        doc.Range.Replace("#ambos#", "ambos ", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "de los representantes");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPor#", "", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#DatosRepresentante2#", "", options);
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                        doc.Range.Replace("#ambos#", "", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    }
                    //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    if (rutR2 > 0)
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                        doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                    }
                    else
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                        doc.Range.Replace("#AmbosPP#", "", options);
                    }
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string CartaGuiaLINEA(string formato, int rutCliente)
        {
            string resultado = "";

            try
            {

                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);


                string plantilla = "CartaGuia.xls";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);


                //FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                //var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var templateWorkbook = new HSSFWorkbook(fs, true);
                Sheet hoja = templateWorkbook.GetSheet("Hoja1");

                NPOI.SS.UserModel.Row row;
                NPOI.SS.UserModel.Cell cell;

                //Nombre Cliente
                row = hoja.GetRow(5);
                cell = row.GetCell(4);
                cell.SetCellValue(cliente.FirstOrDefault().razonSocial.ToString());

                //Domicilio Cliente
                row = hoja.GetRow(6);
                cell = row.GetCell(4);
                cell.SetCellValue(cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle);

                //Comuna
                row = hoja.GetRow(7);
                cell = row.GetCell(4);
                cell.SetCellValue(cliente.FirstOrDefault().comuna);

                //Rut Cliente
                row = hoja.GetRow(8);
                cell = row.GetCell(4);
                cell.SetCellValue(string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper());

                ////Domicilio Cliente
                //row = hoja.GetRow(9);
                //cell = row.GetCell(8);
                //cell.SetCellValue(cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna);
                //row = hoja.GetRow(79);
                //cell = row.GetCell(3);
                //cell.SetCellValue(cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle);

                ////Comuna Cliente
                //row = hoja.GetRow(80);
                //cell = row.GetCell(3);
                //cell.SetCellValue(cliente.FirstOrDefault().comuna);

                ////Ciudad Cliente
                //row = hoja.GetRow(11);
                //cell = row.GetCell(8);
                //cell.SetCellValue(cliente.FirstOrDefault().ciudad);
                //row = hoja.GetRow(80);
                //cell = row.GetCell(8);
                //cell.SetCellValue(cliente.FirstOrDefault().ciudad);


                MemoryStream ms = new MemoryStream();

                //nuevo archivo
                String strNombreArchivo = dirDocumentosTemp + rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name + ".xls";
                System.IO.FileStream archivoSalida = System.IO.File.Create(strNombreArchivo);
                fs.Close();
                templateWorkbook.Write(archivoSalida);
                archivoSalida.Close();

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                //string fileSaveAsPath = "";
                if (formato.ToUpper() == "EXCEL")
                {
                    fileSaveAs = fileSaveAs + ".xls";

                    Response.Clear();
                    //Response.ContentType = "Application/ms-excel";
                    Response.ContentType = "Application/vnd.ms-excel";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(strNombreArchivo);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoFactoringSinResponsabilidadLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "ContratoFactoringSinResponsabilidad.NAT.docx";
                }
                else
                {
                    plantilla = "ContratoFactoringSinResponsabilidad.docx";
                }


                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DatosCliente#",
                    cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                //doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioClientePalabras#", cliente.FirstOrDefault().nombreCalle + " número " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                //doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioSucursalPalabras#", sucursal.FirstOrDefault().direccionPalabras, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                int cantidadAvales = 0;
                if (rutR1 > 0)
                {
                    //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().rutPalabra))
                        datosRepresentante1 += ", cédula de identidad número " + representante1.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DomicilioAval1#",
                            representante1.FirstOrDefault().nombreCalle +
                            " " + representante1.FirstOrDefault().numeroCalle +
                            " " + representante1.FirstOrDefault().letraCalle +
                            ", " + representante1.FirstOrDefault().comuna +
                            ", " + representante1.FirstOrDefault().ciudad
                            , options
                        );
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "", options);
                        doc.Range.Replace("#DomicilioAval1#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }

                //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#YPor#", ", y don(ña) ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().rutPalabra))
                        datosRepresentante2 += ", cédula de identidad número " + representante2.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);


                    datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante2#",
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "de los representantes", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria + " y " + representante2.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    doc.Range.Replace("#FechaPersoneria#",
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                        ", y de " +
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                        , options
                    );

                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria +
                        ", y " + representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );
                    doc.Range.Replace("#otorgada/s#", "otorgadas", options);


                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "del representante", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    if (rutR1 > 0)
                    {
                        doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    }

                    doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }

                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }

                if (rutR1 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                }
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string AnexoServicioAlClienteLINEA(string formato, int rutCliente, int? rutR1)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);


                string plantilla = "AnexoServicioAlCliente.docx";

                //string dirPlantillas = Server.MapPath(@"~/Plantillas/").ToString();
                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                //FindAndReplace(wordApp, "#NacionalidadCliente#", cliente.FirstOrDefault().nacionalidadPersonaNatural);
                //FindAndReplace(wordApp, "#FechaHoy#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"));
                doc.Range.Replace("#FechaHoy#", DateTime.Now.ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                using (var sucur = sucursal.FirstOrDefault())
                {
                    doc.Range.Replace("#SUCURSAL#", sucur.nombre.ToUpper(), options);
                }


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato 
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea - En curse - Cursada
                            , 9 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count() //operacionDetalle.Count()
                            , 0 // operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string FichaAvalistaLINEA(string formato, int rutCliente, int rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                //Validar Fechas Personerias
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    resultado += WebUtility.HtmlDecode("<p>El cliente <STRONG>NO DEBE SER PERSONA NATURAL!</STRONG></p>");
                }
                if (rutR1 > 0)
                {
                    if (!representante1.FirstOrDefault().fiador)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <STRONG>DEBE SER FIADOR!</STRONG></p>");
                    }
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }


                string plantilla = "";

                if (formato.ToUpper() == "SEND-PDF")
                {
                    if (representante1.FirstOrDefault().idEstadoCivil == 1)
                    {
                        plantilla = "FichaAvalista.FE.Casado.docx";
                    }
                    else
                    {
                        plantilla = "FichaAvalista.FE.docx";
                    }
                }
                else
                {
                    if (representante1.FirstOrDefault().idEstadoCivil == 1)
                    {
                        plantilla = "FichaAvalista.Casado.docx";
                    }
                    else
                    {
                        plantilla = "FichaAvalista.docx";
                    }
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                //Aspose.Words.Document doc = new Aspose.Words.Document(fileName);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos AVAl1
                using (var rep = representante1.FirstOrDefault())
                {
                    if (rep.fiador)
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", rep.nombre.ToUpper(), options);
                        doc.Range.Replace("#RutAval1#", string.Format("{0:N0}", rep.rutRepresentante) + "-" + rep.dvRepresentante.ToUpper(), options);
                        doc.Range.Replace("#DOMICILIOAVAL1#",
                            rep.nombreCalle +
                            " " + rep.numeroCalle +
                            " " + rep.letraCalle +
                            ", " + rep.comuna +
                            ", " + rep.ciudad
                            , options
                        );
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "", options);
                        doc.Range.Replace("#RutAval1#", "", options);
                        doc.Range.Replace("#DOMICILIOAVAL1#", "", options);
                    }
                    if (rep.idEstadoCivil == 1)
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", rep.nombreConyuge.ToUpper(), options);
                        doc.Range.Replace("#RutConyuge1#", string.Format("{0:N0}", rep.rutConyuge) + "-" + rep.dvConyuge, options);
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                    }
                }


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato 
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea=1 - En curse=2 - Cursada=3
                            , 3 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count() //operacionDetalle.Count()
                            , 0 // operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                        if (representante1.FirstOrDefault().idEstadoCivil == 1)
                        {
                            gestorDeContratos.agregarContratoEnviadoFirma(
                                contratoEnviado.FirstOrDefault().idContratoEnviado
                                , (int)representante1.FirstOrDefault().rutConyuge
                                , 1
                            );
                        }
                    }

                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoConfirmingNacionalLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();


                //Validar Fechas Personerias
                if ((representante1.FirstOrDefault().fechaPersoneria) == Convert.ToDateTime("1900-01-01").Date)
                {
                    resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == Convert.ToDateTime("1900-01-01").Date)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante " + representante2.FirstOrDefault().nombre.ToUpper() + " no tiene fecha personería!</p>");
                    }
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "ContratoConfirmingNacional.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#SucursalCiudad#", sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccionPalabras, options); // + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().rutPalabra))
                    datosRepresentante1 += ", cédula de identidad número " + representante1.FirstOrDefault().rutPalabra;
                doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                //Datos como AVAl1
                int cantidadAvales = 0;
                if (representante1.FirstOrDefault().fiador)
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DomicilioAval1#",
                        representante1.FirstOrDefault().nombreCalle +
                        " " + representante1.FirstOrDefault().numeroCalle +
                        " " + representante1.FirstOrDefault().letraCalle +
                        ", " + representante1.FirstOrDefault().comuna +
                        ", " + representante1.FirstOrDefault().ciudad
                        , options
                    );
                    cantidadAvales += 1;
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }

                //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#YDon#", ", y don(ña) ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().rutPalabra))
                        datosRepresentante2 += ", cédula de identidad número " + representante2.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);


                    datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#YPor#", " y por ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante2#",
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    //FindAndReplace(wordApp, "#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    doc.Range.Replace("#FechaPersoneria#",
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) + 
                        ", y de " + 
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                        , options
                    );
                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria + 
                        ", y " + representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );
                    doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YDon#", "", options);
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                    doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorDon#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                        doc.Range.Replace("#De/Los/Representantes#", "de los representantes", options);
                        doc.Range.Replace("#Todos/Ddomiciliados/RepP#", "todos domiciliados", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorDon#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                        doc.Range.Replace("#De/Los/Representantes#", "del representante", options);
                        doc.Range.Replace("#Todos/Ddomiciliados/RepP#", "domiciliado", options);
                    }
                }

                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }


                doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoConfirmingInternacionalLINEA(string formato, int rutCliente, int rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                //Validar Fechas Personerias
                if ((representante1.FirstOrDefault().fechaPersoneria) == Convert.ToDateTime("1900-01-01").Date)
                {
                    resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    resultado += WebUtility.HtmlDecode("<div class='alert alert-danger'>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</div>");
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == Convert.ToDateTime("1900-01-01").Date)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante " + representante2.FirstOrDefault().nombre.ToUpper() + " no tiene fecha personería!</p>");
                    }
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "ContratoConfirmingInternacional.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                //FindAndReplace(wordApp, "#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);
                //FindAndReplace(wordApp, "#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccionPalabras, options); // + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().rutPalabra))
                    datosRepresentante1 += ", cédula de identidad número " + representante1.FirstOrDefault().rutPalabra;
                doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                //Datos como AVAl1
                int cantidadAvales = 0;
                if (representante1.FirstOrDefault().fiador)
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DomicilioAval1#",
                        representante1.FirstOrDefault().nombreCalle +
                        " " + representante1.FirstOrDefault().numeroCalle +
                        " " + representante1.FirstOrDefault().letraCalle +
                        ", " + representante1.FirstOrDefault().comuna +
                        ", " + representante1.FirstOrDefault().ciudad
                        , options
                    );
                    cantidadAvales += 1;
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }

                //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#YDon#", ", y don(ña) ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().rutPalabra))
                        datosRepresentante2 += ", cédula de identidad número " + representante2.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);


                    datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#YPor#", " y por ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante2#",
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    //FindAndReplace(wordApp, "#de/delosrepesentantes#", "de los representantes");
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    //FindAndReplace(wordApp, "#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    doc.Range.Replace("#FechaPersoneria#",
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                        ", y de " +
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                        , options
                    );
                    //FindAndReplace(wordApp, "#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria + " y " + representante2.FirstOrDefault().notariaPersoneria);
                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria +
                        ", y " + representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );
                    doc.Range.Replace("#otorgada/s#", "otorgadas", options);

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YDon#", "", options);
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    //FindAndReplace(wordApp, "#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"));
                    doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                    doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YDonRP#", ", y don(ña) ", options);
                        doc.Range.Replace("#TodosDomiciliados#", "todos domiciliados", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YDonRP#", "", options);
                        doc.Range.Replace("#TodosDomiciliados#", "domiciliado", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }

                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }


                doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoMandatoCobranzaDelegadaLINEA(string formato, int rutCliente, int rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "ContratoMandatoCobranzaDelegada.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " número " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                //doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccionPalabras, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                //doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#FechaFirmaContrato#", funcionesUtiles.convertirFechaEnPalabras((DateTime)cliente.FirstOrDefault().fechaFirmaContrato), options);

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                    datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                if (!string.IsNullOrEmpty(representante1.FirstOrDefault().rutPalabra))
                    datosRepresentante1 += ", cédula de identidad número " + representante1.FirstOrDefault().rutPalabra;
                doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                //Datos como AVAl1
                int cantidadAvales = 0;
                if (representante1.FirstOrDefault().fiador)
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DomicilioAval1#",
                        representante1.FirstOrDefault().nombreCalle +
                        " " + representante1.FirstOrDefault().numeroCalle +
                        " " + representante1.FirstOrDefault().letraCalle +
                        ", " + representante1.FirstOrDefault().comuna +
                        ", " + representante1.FirstOrDefault().ciudad
                        , options
                    );
                    cantidadAvales += 1;
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }

                //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#YPor#", ", y don(ña) ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().rutPalabra))
                        datosRepresentante2 += ", cédula de identidad número " + representante2.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);


                    datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#YPor#", " y por ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante2#",
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "de los representantes", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria + " y " + representante2.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    doc.Range.Replace("#FechaPersoneria#",
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                        ", y de " +
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                        , options
                    );

                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria +
                        ", y " + representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );
                    doc.Range.Replace("#otorgada/s#", "otorgadas", options);

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "del representante", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                    doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }

                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }


                doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string pagaresDePublicacionLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }


                //string plantilla = "AnexoCobranzaExterna.docx";
                string plantilla = "";
                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().fechaFirmaContrato < new DateTime(2017, 2, 1))
                {
                    plantilla = "PagareDePublicacion.AnteFebrero2017.docx";
                }
                else
                {
                    plantilla = "PagareDePublicacion.PostFebrero2017.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                //FindAndReplace(wordApp, "#Sucursal#", sucursal.FirstOrDefault().nombre);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                //FindAndReplace(wordApp, "#AutorizaCartaPoder#", cliente.FirstOrDefault().autorizaCartaPoder);
                //FindAndReplace(wordApp, "#Email1#", cliente.FirstOrDefault().email1);
                //FindAndReplace(wordApp, "#Email2#", cliente.FirstOrDefault().email2);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#5DiasAntes#", (DateTime.Now.AddDays(-5)).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#4DiasAntes#", (DateTime.Now.AddDays(-4)).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle
                    + " " + cliente.FirstOrDefault().numeroCalle
                    + " " + cliente.FirstOrDefault().letraCalle
                    //+ ", comuna de " + cliente.FirstOrDefault().comuna
                    //+ ", ciudad de " + cliente.FirstOrDefault().ciudad
                    , options);
                doc.Range.Replace("#ComunaCliente#", cliente.FirstOrDefault().comuna, options);

                int cantidadAvales = 0;
                string avalesAutorizo = "";
                //Datos Representante 1
                if (representante1.Count() > 0)
                {
                    using (var repP = representante1.FirstOrDefault())
                    {
                        doc.Range.Replace("#Representante1#", "REP. LEGAL 1\t: ", options);
                        doc.Range.Replace("#NombreRepresentanteCliente1#", repP.nombre + "\r", options);
                        doc.Range.Replace("#RutRepresentante1#", "C.I. REP. LEGAL 1\t: ", options);
                        doc.Range.Replace("#RutRepresentanteCliente1#", string.Format("{0:N0}", repP.rutRepresentante) + "-" + repP.dvRepresentante.ToUpper() + "\r", options);
                    }

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        cantidadAvales += 1;

                        doc.Range.Replace("#AVAL1#", "AVAL " + cantidadAvales + "\t\t: ", options);
                        doc.Range.Replace("#NOMBREAVAL1#", representante1.FirstOrDefault().nombre.ToUpper() + "\r", options);
                        doc.Range.Replace("#CNIAVAL1#", "C.N.I. AVAL " + cantidadAvales + "\t\t: ", options);
                        doc.Range.Replace("#RUTAVAL1#", string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper() + "\r", options);

                        avalesAutorizo += representante1.FirstOrDefault().nombre.ToUpper();
                        avalesAutorizo += ", C.N.I. N° ";
                        avalesAutorizo += string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    }
                    else
                    {
                        doc.Range.Replace("#AVAL1#", "", options);
                        doc.Range.Replace("#NOMBREAVAL1#", "", options);
                        doc.Range.Replace("#CNIAVAL1#", "", options);
                        doc.Range.Replace("#RUTAVAL1#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#Representante1#", "", options);
                    doc.Range.Replace("#NombreRepresentanteCliente1#", "", options);
                    doc.Range.Replace("#RutRepresentante1#", "", options);
                    doc.Range.Replace("#RutRepresentanteCliente1#", "", options);
                }

                //Datos Representante 2
                if (representante2.Count() > 0)
                {
                    using (var repP = representante2.FirstOrDefault())
                    {
                        doc.Range.Replace("#Representante2#", "REP. LEGAL 2\t: ", options);
                        doc.Range.Replace("#NombreRepresentanteCliente2#", repP.nombre + "\r", options);
                        doc.Range.Replace("#RutRepresentante2#", "C.I. REP. LEGAL 2\t: ", options);
                        doc.Range.Replace("#RutRepresentanteCliente2#", string.Format("{0:N0}", repP.rutRepresentante) + "-" + repP.dvRepresentante.ToUpper() + "\r", options);
                    }

                    //Datos como AVAl2
                    if (representante2.FirstOrDefault().fiador)
                    {
                        cantidadAvales += 1;

                        if (cantidadAvales == 1)
                        {
                            doc.Range.Replace("#AVAL1#", "AVAL " + cantidadAvales + "\t\t: ", options);
                            doc.Range.Replace("#NOMBREAVAL1#", representante2.FirstOrDefault().nombre.ToUpper() + "\r", options);
                            doc.Range.Replace("#CNIAVAL1#", "C.N.I. AVAL " + cantidadAvales + "\t\t: ", options);
                            doc.Range.Replace("#RUTAVAL1#", string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper() + "\r", options);

                            doc.Range.Replace("#AVAL2#", "", options);
                            doc.Range.Replace("#NOMBREAVAL2#", "", options);
                            doc.Range.Replace("#CNIAVAL2#", "", options);
                            doc.Range.Replace("#RUTAVAL2#", "", options);

                            avalesAutorizo += representante2.FirstOrDefault().nombre.ToUpper();
                            avalesAutorizo += ", C.N.I. N° ";
                            avalesAutorizo += string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        }
                        else if (cantidadAvales == 2)
                        {
                            doc.Range.Replace("#AVAL2#", "AVAL " + cantidadAvales + "\t\t: ", options);
                            doc.Range.Replace("#NOMBREAVAL2#", representante2.FirstOrDefault().nombre.ToUpper() + "\r", options);
                            doc.Range.Replace("#CNIAVAL2#", "C.N.I. AVAL " + cantidadAvales + "\t\t: ", options);
                            doc.Range.Replace("#RUTAVAL2#", string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper() + "\r", options);

                            avalesAutorizo += ", y por don(ña) ";
                            avalesAutorizo += representante2.FirstOrDefault().nombre.ToUpper();
                            avalesAutorizo += ", C.N.I. N° ";
                            avalesAutorizo += string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#AVAL2#", "", options);
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                        doc.Range.Replace("#CNIAVAL2#", "", options);
                        doc.Range.Replace("#RUTAVAL2#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#Representante2#", "", options);
                    doc.Range.Replace("#NombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#RutRepresentante2#", "", options);
                    doc.Range.Replace("#RutRepresentanteCliente2#", "", options);

                    doc.Range.Replace("#AVAL2#", "", options);
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    doc.Range.Replace("#CNIAVAL2#", "", options);
                    doc.Range.Replace("#RUTAVAL2#", "", options);
                }
                if (string.IsNullOrEmpty(avalesAutorizo))
                {
                    doc.Range.Replace("#AvalesAutorizo#", "", options);
                }
                else
                {
                    doc.Range.Replace("#AvalesAutorizo#", avalesAutorizo, options);
                }
                if (cantidadAvales == 1)
                {
                    doc.Range.Replace("#ambosAva#", "", options);
                }
                else if (cantidadAvales == 2)
                {
                    doc.Range.Replace("#ambosAva#", "ambos ", options);
                }
                else
                {
                    doc.Range.Replace("#ambosAva#", "", options);
                }


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    datosRepresentantePenta = ",  C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        datosRepresentantePenta = ",  C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                        doc.Range.Replace("#AmbosRep#", "ambos ", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                        doc.Range.Replace("#AmbosRep#", "", options);
                    }
                }

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea=1 - En curse=2 - Cursada=3
                            , 6 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count() //operacionDetalle.Count()
                            , 0 // operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string poderFirmaContratosLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }


                string plantilla = "PoderFirmarContratosDeCesionYAnexoCartaGuiaPorUnTercero.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                //doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#FechaHoy#", DateTime.Now.ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion 
                    + ", comuna de " + sucursal.FirstOrDefault().comuna 
                    + ", ciudad de " + sucursal.FirstOrDefault().ciudad
                , options);
                doc.Range.Replace("#5DiasAntes#", (DateTime.Now.AddDays(-5)).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#4DiasAntes#", (DateTime.Now.AddDays(-4)).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle
                    + " " + cliente.FirstOrDefault().numeroCalle
                    + " " + cliente.FirstOrDefault().letraCalle
                    + ", comuna de " + cliente.FirstOrDefault().comuna
                    + ", ciudad de " + cliente.FirstOrDefault().ciudad
                    , options);
                //doc.Range.Replace("#ComunaCliente#", cliente.FirstOrDefault().comuna, options);
                doc.Range.Replace("#NombrePoderFirmaContratos#", cliente.FirstOrDefault().autorizaFirmarNombre, options);
                doc.Range.Replace("#RutPoderFirmaContratos#", cliente.FirstOrDefault().autorizaFirmarRut, options);

                //Datos Representante 1
                if (representante1.Count() > 0)
                {
                    using (var repP = representante1.FirstOrDefault())
                    {
                        doc.Range.Replace("#NombreRepresentante1#", repP.nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante1#",
                            ", " + repP.nacionalidad +
                            ", " + repP.estadoCivil +
                            ", " + repP.profesion +
                            ", C.I. N° " + string.Format("{0:N0}", repP.rutRepresentante) + "-" + repP.dvRepresentante.ToUpper()
                            , options
                        );
                    }

                    if (rutR2 > 0)
                    {
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente",
                            options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NombreRepresentante#", "", options);
                    doc.Range.Replace("#DatosRepresentante1#", "", options);
                }

                //Datos Representante 2
                if (representante2.Count() > 0)
                {
                    doc.Range.Replace("#YPor#", ", y por ", options);
                    using (var repP = representante2.FirstOrDefault())
                    {
                        doc.Range.Replace("#NombreRepresentante2#", repP.nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante2#",
                            ", " + repP.nacionalidad +
                            ", " + repP.estadoCivil +
                            ", " + repP.profesion +
                            ", C.I. N° " + string.Format("{0:N0}", repP.rutRepresentante) + "-" + repP.dvRepresentante.ToUpper()
                            , options
                        );
                    }
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                }
                else
                {
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                }



                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea=1 - En curse=2 - Cursada=3
                            , 13 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count() //operacionDetalle.Count()
                            , 0 // operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string PEPLINEA(string formato, int rutCliente, string pep, int? rutR1)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);


                string plantilla = "";
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "PEP.Natural.docx";
                }
                else
                {
                    plantilla = "PEP.Juridica.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                //Aspose.Words.Document doc = new Aspose.Words.Document(fileName);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#NacionalidadCliente#", cliente.FirstOrDefault().nacionalidadPersonaNatural, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    if (pep.ToLower() == "si")
                    {
                        doc.Range.Replace("#Ser/NoSerPEP#", "Ser", options);
                    }
                    else
                    {
                        doc.Range.Replace("#Ser/NoSerPEP#", "No Ser", options);
                    }
                }
                else
                {
                    if (pep.ToLower() == "si")
                    {
                        doc.Range.Replace("#Ninguno/AlgunoPEP#", "Alguno", options);
                    }
                    else
                    {
                        doc.Range.Replace("#Ninguno/AlgunoPEP#", "Ninguno", options);
                    }
                }


                if (representante1.Count() > 0)
                {
                    using (var repC = representante1.FirstOrDefault())
                    {
                        doc.Range.Replace("#NombreRepresentanteCliente#", repC.nombre.ToUpper(), options);
                        doc.Range.Replace(
                            "#RutRepresentanteCliente#"
                            , string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#NacionalidadRepresentanteCliente#", repC.nacionalidad.ToUpper(), options);
                    }
                }

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato 
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            0 //idSimulaOperacion
                            , 1 //Linea - En curse - Cursada
                            , 8 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , 0 //(int)operacionDetalle.Count()
                            , 0 //operacionResumen.FirstOrDefault().montoDocumentos
                            , 0 //operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }

                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }


        //EN CURSE
        public string cesionCreditoReconoDeudaENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                //Validar notaria
                if (notario.FirstOrDefault().nombreNotario == null)
                {
                    resultado += WebUtility.HtmlDecode("<p>El cliente no tiene <strong>notaria asignada</strong>!</p>");
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "";
                //Si el cliente es persona natural
                plantilla = "CesionDeCredito.ReconocimientoDeDeudaConComparecenciaDelPagador.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DatosCliente#",
                    ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                //doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle +
                //    " " + cliente.FirstOrDefault().numeroCalle +
                //    " " + cliente.FirstOrDefault().letraCalle +
                //    ", comuna de " + cliente.FirstOrDefault().comuna +
                //    ", ciudad de " + cliente.FirstOrDefault().ciudad
                //    , options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                //doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccionPalabras, options); // + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#NotariaNombreNotario#", notario.FirstOrDefault().nombreNotario, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                doc.Range.Replace("#RepresentantePenta#", representantePenta1.FirstOrDefault().nombreCompleto, options);
                doc.Range.Replace("#RepresentantePentaRut#", string.Format("{0:N0}", representantePenta1.FirstOrDefault().rut) + "-" + representantePenta1.FirstOrDefault().dv.ToUpper(), options);

                //Datos Representantes
                var datosRepresentantes = "";
                //var datosRepresentantesMinimos = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante1#",
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        //", cédula nacional de identidad número " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        ", cédula nacional de identidad número " + representante1.FirstOrDefault().rutPalabra
                        , options
                    );
                    //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante;
                        doc.Range.Replace("#YPor#", ", y [don/doña] ", options);
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante2#",
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            //", cédula nacional de identidad número " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            ", cédula nacional de identidad número " + representante2.FirstOrDefault().rutPalabra
                            , options
                        );
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                        //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                        doc.Range.Replace("#ambos#", "ambos ", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "de los representantes");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                            ", y de " +
                            funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                            , options
                        );

                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPor#", "", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#DatosRepresentante2#", "", options);
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                        doc.Range.Replace("#ambos#", "", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    }
                    //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    if (rutR2 > 0)
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                        doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                    }
                    else
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                        doc.Range.Replace("#AmbosPP#", "", options);
                    }
                }

                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#PIENOMBREREPRESENTANTEPENTA1#", repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por [don/doña] ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#PIENOMBREREPRESENTANTEPENTA2#", "\t" + repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                /*
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.ParagraphFormat.ClearFormatting();
                builder.MoveToBookmark("CartaGuia");
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.Write("DATOS DE LOS DEUDORES");
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.Write("NOTA CREDITO");
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.EndRow();

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                builder.InsertCell();
                builder.Write("RUT");
                builder.InsertCell();
                builder.Write("DV");
                builder.InsertCell();
                builder.Write("NOMBRE O RAZÓN SOCIAL");
                builder.InsertCell();
                builder.Write("VCMTO");
                builder.InsertCell();
                builder.Write("NUMERO DOCUMENTO");
                builder.InsertCell();
                builder.Write("VALOR TOTAL");
                builder.InsertCell();
                builder.Write("N°");
                builder.InsertCell();
                builder.Write("MONTO");
                builder.EndRow();

                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.5);
                    builder.Write(string.Format("{0:N0}", operacionD.rutDeudor));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(4);
                    builder.Write(operacionD.dvDeudor.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(30.5);
                    builder.Write(operacionD.deudor.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.7);
                    builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.8);
                    builder.Write(operacionD.numDocto.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(13.5);
                    //builder.Write(operacionD.valorDoctoEnMoneda.ToString());
                    builder.Write(string.Format("{0:C0}", operacionD.valorDoctoEnMoneda));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.6);
                    builder.Write(operacionD.numNCredito.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11);
                    builder.Write(string.Format("{0:C0}", operacionD.montoNCredito));
                    builder.EndRow();
                }
                builder.EndTable();
                */

                //doc.Range.Replace("#PrecioDeCompra#", "$" + string.Format("{0:N0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#NombreDeudor#", operacionDetalle.FirstOrDefault().deudor, options);
                doc.Range.Replace("#RutDeudorPalabras#", operacionDetalle.FirstOrDefault().rutDeudorPalabra, options);
                doc.Range.Replace("#FechaEmisionDocumento#", ((DateTime)operacionDetalle.FirstOrDefault().fechaEmision).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#ValorDocumento#", string.Format("{0:C0}", operacionDetalle.FirstOrDefault().valorDocto), options);
                doc.Range.Replace("#ValorCesionDocumento#", string.Format("{0:C0}", operacionDetalle.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#FechaVencimientoDocumento#", ((DateTime)operacionDetalle.FirstOrDefault().fechaVcto).ToString("dd' de 'MMMM' de 'yyyy"), options);

                doc.Range.Replace("#PrecioDeCompra#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#SaldoPorPagar#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().AGirarEjec), options);
                doc.Range.Replace("#SaldoPendiente#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().saldoPendiente), options);

                doc.Range.Replace("#TotalDocumentos#", operacionDetalle.Count().ToString(), options);
                doc.Range.Replace("#TotalOperacion#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().montoDocumentos), options);

                


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            idSimulaOperacion
                            , 2 //Linea - En curse - Cursada
                            , 1 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial
                            , (int)operacionDetalle.Count() //operacionDetalle.Count()
                            , operacionResumen.FirstOrDefault().montoDocumentos
                            , operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 16788841, 2); // Representante Penta FELIPE

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 15398791, 3); //KARINA MARLENE Peña Muñoz

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string contratoCesionDocPrivENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2, int rutRP)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(rutRP);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                //Validar notaria
                if (notario.FirstOrDefault().nombreNotario == null)
                {
                    resultado += WebUtility.HtmlDecode("<p>El cliente no tiene <strong>notaria asignada</strong>!</p>");
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "";
                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "ContratoCesionDocumentoPrivadoFirmaElectronica.Nat.docx";
                }
                else
                {
                    plantilla = "ContratoCesionDocumentoPrivadoFirmaElectronica.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DatosCliente#",
                    ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle +
                    " " + cliente.FirstOrDefault().numeroCalle +
                    " " + cliente.FirstOrDefault().letraCalle +
                    ", comuna de " + cliente.FirstOrDefault().comuna +
                    ", ciudad de " + cliente.FirstOrDefault().ciudad
                    , options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#NotariaNombreNotario#", notario.FirstOrDefault().nombreNotario, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                doc.Range.Replace("#RepresentantePenta#", representantePenta1.FirstOrDefault().nombreCompleto, options);
                doc.Range.Replace("#RepresentantePentaRut#", string.Format("{0:N0}", representantePenta1.FirstOrDefault().rut) + "-" + representantePenta1.FirstOrDefault().dv.ToUpper(), options);

                //Datos Representantes
                var datosRepresentantes = "";
                //var datosRepresentantesMinimos = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante1#",
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante;
                        doc.Range.Replace("#YPor#", " y por ", options);
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante2#",
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                        //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                        doc.Range.Replace("#ambos#", "ambos ", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "de los representantes");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPor#", "", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#DatosRepresentante2#", "", options);
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                        doc.Range.Replace("#ambos#", "", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    }
                    //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    if (rutR2 > 0)
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                        doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                    }
                    else
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                        doc.Range.Replace("#AmbosPP#", "", options);
                    }
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToBookmark("CartaGuia");
                builder.ParagraphFormat.ClearFormatting();
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.Write("DATOS DE LOS DEUDORES");
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.Write("NOTA CREDITO");
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.EndRow();

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                builder.InsertCell();
                    builder.Write("RUT");
                builder.InsertCell();
                    builder.Write("DV");
                builder.InsertCell();
                    builder.Write("NOMBRE O RAZÓN SOCIAL");
                builder.InsertCell();
                    builder.Write("VCMTO");
                builder.InsertCell();
                    builder.Write("NUMERO DOCUMENTO");
                builder.InsertCell();
                    builder.Write("VALOR TOTAL");
                builder.InsertCell();
                    builder.Write("N°");
                builder.InsertCell();
                    builder.Write("MONTO");
                builder.EndRow();

                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    builder.InsertCell();
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                        builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.5);
                    builder.Write(string.Format("{0:N0}", operacionD.rutDeudor));
                    builder.InsertCell();
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                        builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(4);
                        builder.Write(operacionD.dvDeudor.ToString());
                    builder.InsertCell();
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                        builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(30.5);
                        builder.Write(operacionD.deudor.ToString());
                    builder.InsertCell();
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                        builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.7);
                        builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                        builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.8);
                        builder.Write(operacionD.numDocto.ToString());
                    builder.InsertCell();
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                        builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(13.5);
                        //builder.Write(operacionD.valorDoctoEnMoneda.ToString());
                        builder.Write(string.Format("{0:C0}", operacionD.valorDoctoEnMoneda));
                    builder.InsertCell();
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                        builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.6);
                        builder.Write(operacionD.numNCredito.ToString());
                    builder.InsertCell();
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                        builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11);
                        builder.Write(string.Format("{0:C0}", operacionD.montoNCredito));
                    builder.EndRow();
                }
                builder.EndTable();
                

                //doc.Range.Replace("#PrecioDeCompra#", "$" + string.Format("{0:N0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#PrecioDeCompra#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#SaldoPorPagar#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().AGirarEjec), options);
                doc.Range.Replace("#SaldoPendiente#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().saldoPendiente), options);

                doc.Range.Replace("#TotalDocumentos#", operacionDetalle.Count().ToString(), options);
                doc.Range.Replace("#TotalOperacion#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().montoDocumentos), options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    //DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);
                    
                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado = 
                        gestorDeContratos.agregarContratoEnviado(
                            idSimulaOperacion
                            , 2 //Linea - En curse - Cursada
                            , 1 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial
                            , (int)operacionDetalle.Count() //operacionDetalle.Count()
                            , operacionResumen.FirstOrDefault().montoDocumentos
                            , operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }
                    
                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutRP, 2);

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                 //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string contratoCesionENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2, int rutRP)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(rutRP);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                //Validar notaria
                if (notario.FirstOrDefault().nombreNotario == null)
                {
                    resultado += WebUtility.HtmlDecode("<p>El cliente no tiene <strong>notaria asignada</strong>!</p>");
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "";
                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "ContratoCesion.CURSE.Nat.docx";
                }
                else
                {
                    plantilla = "ContratoCesion.CURSE.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DatosCliente#",
                    ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle +
                    " " + cliente.FirstOrDefault().numeroCalle +
                    " " + cliente.FirstOrDefault().letraCalle +
                    ", comuna de " + cliente.FirstOrDefault().comuna +
                    ", ciudad de " + cliente.FirstOrDefault().ciudad
                    , options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#NotariaNombreNotario#", notario.FirstOrDefault().nombreNotario, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                doc.Range.Replace("#RepresentantePenta#", representantePenta1.FirstOrDefault().nombreCompleto, options);
                doc.Range.Replace("#RepresentantePentaRut#", string.Format("{0:N0}", representantePenta1.FirstOrDefault().rut) + "-" + representantePenta1.FirstOrDefault().dv.ToUpper(), options);

                //Datos Representantes
                var datosRepresentantes = "";
                //var datosRepresentantesMinimos = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante1#",
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante;
                        doc.Range.Replace("#YPor#", " y por ", options);
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante2#",
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                        //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                        doc.Range.Replace("#ambos#", "ambos ", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "de los representantes");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPor#", "", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#DatosRepresentante2#", "", options);
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                        doc.Range.Replace("#ambos#", "", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    }
                    //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    if (rutR2 > 0)
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                        doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                    }
                    else
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                        doc.Range.Replace("#AmbosPP#", "", options);
                    }
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToBookmark("CartaGuia");
                builder.ParagraphFormat.ClearFormatting();
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.Write("DATOS DE LOS DEUDORES");
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.Write("NOTA CREDITO");
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.EndRow();

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                builder.InsertCell();
                builder.Write("RUT");
                builder.InsertCell();
                builder.Write("DV");
                builder.InsertCell();
                builder.Write("NOMBRE O RAZÓN SOCIAL");
                builder.InsertCell();
                builder.Write("VCMTO");
                builder.InsertCell();
                builder.Write("NUMERO DOCUMENTO");
                builder.InsertCell();
                builder.Write("VALOR TOTAL");
                builder.InsertCell();
                builder.Write("N°");
                builder.InsertCell();
                builder.Write("MONTO");
                builder.EndRow();

                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.5);
                    builder.Write(string.Format("{0:N0}", operacionD.rutDeudor));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(4);
                    builder.Write(operacionD.dvDeudor.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(30.5);
                    builder.Write(operacionD.deudor.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.7);
                    builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.8);
                    builder.Write(operacionD.numDocto.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(13.5);
                    //builder.Write(operacionD.valorDoctoEnMoneda.ToString());
                    builder.Write(string.Format("{0:C0}", operacionD.valorDoctoEnMoneda));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.6);
                    builder.Write(operacionD.numNCredito.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11);
                    builder.Write(string.Format("{0:C0}", operacionD.montoNCredito));
                    builder.EndRow();
                }
                builder.EndTable();

                //doc.Range.Replace("#PrecioDeCompra#", "$" + string.Format("{0:N0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#PrecioDeCompra#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#SaldoPorPagar#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().AGirarEjec), options);
                doc.Range.Replace("#SaldoPendiente#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().saldoPendiente), options);

                doc.Range.Replace("#TotalDocumentos#", operacionDetalle.Count().ToString(), options);
                doc.Range.Replace("#TotalOperacion#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().montoDocumentos), options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    //DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            idSimulaOperacion
                            , 2 //Linea - En curse - Cursada
                            , 5 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial
                            , (int)operacionDetalle.Count() //operacionDetalle.Count()
                            , operacionResumen.FirstOrDefault().montoDocumentos
                            , operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutRP, 2);

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 15398791, 3); //KARINA MARLENE Peña Muñoz
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 16924442, 3); //Milena
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 16788841, 3); //Felipe Lorca

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string modificacionCesionCreditoCambioFechaPagoENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                //Validar notaria
                if (notario.FirstOrDefault().nombreNotario == null)
                {
                    resultado += WebUtility.HtmlDecode("<p>El cliente no tiene <strong>notaria asignada</strong>!</p>");
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "";
                plantilla = "ModificacionDeCesionDeCreditoPorCambioDeFechaDePago.docx";


                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DatosCliente#",
                    ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                //doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle +
                //    " " + cliente.FirstOrDefault().numeroCalle +
                //    " " + cliente.FirstOrDefault().letraCalle +
                //    ", comuna de " + cliente.FirstOrDefault().comuna +
                //    ", ciudad de " + cliente.FirstOrDefault().ciudad
                //    , options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                //doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccionPalabras, options); // + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#NotariaNombreNotario#", notario.FirstOrDefault().nombreNotario, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                doc.Range.Replace("#RepresentantePenta#", representantePenta1.FirstOrDefault().nombreCompleto, options);
                doc.Range.Replace("#RepresentantePentaRut#", string.Format("{0:N0}", representantePenta1.FirstOrDefault().rut) + "-" + representantePenta1.FirstOrDefault().dv.ToUpper(), options);

                //Datos Representantes
                var datosRepresentantes = "";
                //var datosRepresentantesMinimos = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante1#",
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        //", cédula nacional de identidad número " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        ", cédula nacional de identidad número " + representante1.FirstOrDefault().rutPalabra
                        , options
                    );
                    //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante;
                        doc.Range.Replace("#YPor#", ", y [don/doña] ", options);
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante2#",
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            //", cédula nacional de identidad número " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            ", cédula nacional de identidad número " + representante2.FirstOrDefault().rutPalabra
                            , options
                        );
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                        //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                        doc.Range.Replace("#ambos#", "ambos ", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "de los representantes");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                            ", y de " +
                            funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                            , options
                        );

                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPor#", "", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#DatosRepresentante2#", "", options);
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                        doc.Range.Replace("#ambos#", "", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    }
                    //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    if (rutR2 > 0)
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                        doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                    }
                    else
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                        doc.Range.Replace("#AmbosPP#", "", options);
                    }
                }

                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#PIENOMBREREPRESENTANTEPENTA1#", repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por [don/doña] ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#PIENOMBREREPRESENTANTEPENTA2#", "\t" + repP.nombres.ToUpper() + " " + repP.apPaterno.ToUpper() + " " + repP.apMaterno.ToUpper(), options);
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                /*
                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.ParagraphFormat.ClearFormatting();
                builder.MoveToBookmark("CartaGuia");
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.Write("DATOS DE LOS DEUDORES");
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.First;
                builder.Write("NOTA CREDITO");
                builder.InsertCell();
                builder.CellFormat.HorizontalMerge = CellMerge.Previous;
                builder.EndRow();

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                builder.InsertCell();
                builder.Write("RUT");
                builder.InsertCell();
                builder.Write("DV");
                builder.InsertCell();
                builder.Write("NOMBRE O RAZÓN SOCIAL");
                builder.InsertCell();
                builder.Write("VCMTO");
                builder.InsertCell();
                builder.Write("NUMERO DOCUMENTO");
                builder.InsertCell();
                builder.Write("VALOR TOTAL");
                builder.InsertCell();
                builder.Write("N°");
                builder.InsertCell();
                builder.Write("MONTO");
                builder.EndRow();

                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.5);
                    builder.Write(string.Format("{0:N0}", operacionD.rutDeudor));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(4);
                    builder.Write(operacionD.dvDeudor.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(30.5);
                    builder.Write(operacionD.deudor.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.7);
                    builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.8);
                    builder.Write(operacionD.numDocto.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(13.5);
                    //builder.Write(operacionD.valorDoctoEnMoneda.ToString());
                    builder.Write(string.Format("{0:C0}", operacionD.valorDoctoEnMoneda));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(8.6);
                    builder.Write(operacionD.numNCredito.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11);
                    builder.Write(string.Format("{0:C0}", operacionD.montoNCredito));
                    builder.EndRow();
                }
                builder.EndTable();
                */

                //doc.Range.Replace("#PrecioDeCompra#", "$" + string.Format("{0:N0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#NombreDeudor#", operacionDetalle.FirstOrDefault().deudor, options);
                doc.Range.Replace("#RutDeudorPalabras#", operacionDetalle.FirstOrDefault().rutDeudorPalabra, options);
                doc.Range.Replace("#FechaEmisionDocumento#", ((DateTime)operacionDetalle.FirstOrDefault().fechaEmision).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#ValorDocumento#", string.Format("{0:C0}", operacionDetalle.FirstOrDefault().valorDocto), options);
                doc.Range.Replace("#ValorCesionDocumento#", string.Format("{0:C0}", operacionDetalle.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#FechaVencimientoDocumento#", ((DateTime)operacionDetalle.FirstOrDefault().fechaVcto).ToString("dd' de 'MMMM' de 'yyyy"), options);

                doc.Range.Replace("#PrecioDeCompra#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#SaldoPorPagar#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().AGirarEjec), options);
                doc.Range.Replace("#SaldoPendiente#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().saldoPendiente), options);

                doc.Range.Replace("#TotalDocumentos#", operacionDetalle.Count().ToString(), options);
                doc.Range.Replace("#TotalOperacion#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().montoDocumentos), options);




                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            idSimulaOperacion
                            , 2 //Linea - En curse - Cursada
                            , 1 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial
                            , (int)operacionDetalle.Count() //operacionDetalle.Count()
                            , operacionResumen.FirstOrDefault().montoDocumentos
                            , operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 16788841, 2); // Representante Penta FELIPE

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 15398791, 3); //KARINA MARLENE Peña Muñoz

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string PagareDolarENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";
                bool fiadores = false;

                //Si los representantes son fiadores
                if (rutR1 > 0)
                {
                    if (representante1.FirstOrDefault().fiador) fiadores = true;
                }
                if (rutR2 > 0)
                {
                    if (representante2.FirstOrDefault().fiador) fiadores = true;
                }
                if (fiadores)
                {
                    plantilla = "PagareDolar.Aval.docx";
                }
                else
                {
                    plantilla = "PagareDolar.docx";
                }


                //string dirPlantillas = Server.MapPath(@"~/Plantillas/").ToString();
                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Nombrecliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle
                    //+ ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad
                    , options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#ComunaCliente#", cliente.FirstOrDefault().comuna, options);

                string representantesLegales = "";
                string avalesCliente = "";
                //REPRESENTANTE1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (repC != null)
                    {
                        representantesLegales += "REPRESENTANTE LEGAL\t\t: " + repC.nombre.ToUpper() + "\r";
                        representantesLegales += "C.I.\t\t\t\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";

                        //AVAL1
                        if (repC.fiador)
                        {
                            avalesCliente += "AVAL\t\t: " + repC.nombre.ToUpper() + "\r";
                            avalesCliente += "C.I.\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";
                        }
                    }
                }

                //REPRESENTANTE2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (repC != null)
                    {
                        representantesLegales += "REPRESENTANTE LEGAL 2\t: " + repC.nombre.ToUpper() + "\r";
                        representantesLegales += "C.I. 2\t\t\t\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";

                        //AVAL2
                        if (repC.fiador)
                        {
                            avalesCliente += "AVAL 2\t\t: " + repC.nombre.ToUpper() + "\r";
                            avalesCliente += "C.I. 2\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";
                        }
                    }
                }
                doc.Range.Replace("#RepresentantesLegales#", representantesLegales, options);
                doc.Range.Replace("#AvalesCliente#", avalesCliente, options);

                //OPERACION
                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    if (operacionR.idSimulaOperacion == 0)
                    {
                        doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#FechaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    }
                    else
                    {
                        doc.Range.Replace("#FechaOtorgamiento#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#FechaContrato#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    }
                }

                doc.Range.Replace("#MontoCapital#", string.Format("{0:N2}", operacionResumen.FirstOrDefault().montoFinanciado), options);
                //doc.Range.Replace("#MontoCapitalPalabras#", string.Format("{0:C0}", funcionesUtiles.convertirNumeroEnPalabras((int)operacionResumen.FirstOrDefault().montoFinanciado)), options);
                doc.Range.Replace("#MontoCapitalPalabras#", funcionesUtiles.convertirNumeroEnPalabras((decimal)operacionResumen.FirstOrDefault().montoFinanciado), options);
                doc.Range.Replace("#TasaInteres#", string.Format("{0:0.00}", operacionResumen.FirstOrDefault().tasa), options);


                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToBookmark("ListadoDocumentos");
                builder.ParagraphFormat.ClearFormatting();
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                builder.InsertCell();
                builder.Write("CUOTA");
                builder.InsertCell();
                builder.Write("FECHA PAGO");
                builder.InsertCell();
                builder.Write("CAPITAL USD");
                builder.InsertCell();
                builder.Write("INTERÉS USD");
                builder.InsertCell();
                builder.Write("VALOR CUOTA USD");
                builder.EndRow();

                int x = 0;
                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    x += 1;
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.9);
                    builder.Write(x.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(22.1);
                    builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(24.4);
                    builder.Write(string.Format("{0:N2}", operacionD.valorCesion));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(14.1);
                    builder.Write(string.Format("{0:N2}", operacionD.interesDocumento));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(27.3);
                    builder.Write(string.Format("{0:N2}", operacionD.valorDoctoEnMoneda));
                    builder.EndRow();
                }
                builder.EndTable();


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            idSimulaOperacion
                            , 2 //Linea=1 / En curse=2 / Cursada=3
                            , 12 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , (int)operacionDetalle.Count() //operacionDetalle.Count()
                            , operacionResumen.FirstOrDefault().montoDocumentos
                            , operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 15398791, 3); //KARINA MARLENE Peña Muñoz

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string PagareUFENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";
                bool fiadores = false;

                //Si los representantes son fiadores
                if (rutR1 > 0)
                {
                    if (representante1.FirstOrDefault().fiador) fiadores = true;
                }
                if (rutR2 > 0)
                {
                    if (representante2.FirstOrDefault().fiador) fiadores = true;
                }
                if (fiadores)
                {
                    plantilla = "PagareUF.Aval.docx";
                }
                else
                {
                    plantilla = "PagareUF.docx";
                }


                //string dirPlantillas = Server.MapPath(@"~/Plantillas/").ToString();
                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Nombrecliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle
                    //+ ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad
                    , options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#ComunaCliente#", cliente.FirstOrDefault().comuna, options);

                string representantesLegales = "";
                string avalesCliente = "";
                //REPRESENTANTE1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (repC != null)
                    {
                        representantesLegales += "REPRESENTANTE LEGAL\t\t: " + repC.nombre.ToUpper() + "\r";
                        representantesLegales += "C.I.\t\t\t\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";

                        //AVAL1
                        if (repC.fiador)
                        {
                            avalesCliente += "AVAL\t\t\t: " + repC.nombre.ToUpper() + "\r";
                            avalesCliente += "C.I.\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";
                        }
                    }
                }

                //REPRESENTANTE2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (repC != null)
                    {
                        representantesLegales += "REPRESENTANTE LEGAL 2\t: " + repC.nombre.ToUpper() + "\r";
                        representantesLegales += "C.I. 2\t\t\t\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";

                        //AVAL2
                        if (repC.fiador)
                        {
                            avalesCliente += "AVAL 2\t\t: " + repC.nombre.ToUpper() + "\r";
                            avalesCliente += "C.I. 2\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";
                        }
                    }
                }
                doc.Range.Replace("#RepresentantesLegales#", representantesLegales, options);
                doc.Range.Replace("#AvalesCliente#", avalesCliente, options);

                //OPERACION
                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    if (operacionR.idSimulaOperacion == 0)
                    {
                        doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#FechaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    }
                    else
                    {
                        doc.Range.Replace("#FechaOtorgamiento#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#FechaContrato#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    }
                }

                doc.Range.Replace("#MontoCapital#", string.Format("{0:N2}", operacionResumen.FirstOrDefault().montoFinanciado), options);
                //doc.Range.Replace("#MontoCapitalPalabras#", string.Format("{0:C0}", funcionesUtiles.convertirNumeroEnPalabras((int)operacionResumen.FirstOrDefault().montoFinanciado)), options);
                doc.Range.Replace("#MontoCapitalPalabras#", funcionesUtiles.convertirNumeroEnPalabras((decimal)operacionResumen.FirstOrDefault().montoFinanciado), options);
                doc.Range.Replace("#TasaInteres#", string.Format("{0:0.00}", operacionResumen.FirstOrDefault().tasa), options);


                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToBookmark("ListadoDocumentos");
                builder.ParagraphFormat.ClearFormatting();
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                builder.InsertCell();
                builder.Write("CUOTA");
                builder.InsertCell();
                builder.Write("FECHA PAGO");
                builder.InsertCell();
                builder.Write("CAPITAL UF");
                builder.InsertCell();
                builder.Write("INTERÉS UF");
                builder.InsertCell();
                builder.Write("VALOR CUOTA UF");
                builder.EndRow();

                int x = 0;
                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    x += 1;
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(11.9);
                    builder.Write(x.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(22.1);
                    builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(24.4);
                    builder.Write(string.Format("{0:N2}", operacionD.valorCesion));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(14.1);
                    builder.Write(string.Format("{0:N2}", operacionD.interesDocumento));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(27.3);
                    builder.Write(string.Format("{0:N2}", operacionD.valorDoctoEnMoneda));                    
                    builder.EndRow();
                }
                builder.EndTable();


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            idSimulaOperacion
                            , 2 //Linea=1 / En curse=2 / Cursada=3
                            , 11 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , (int)operacionDetalle.Count() //operacionDetalle.Count()
                            , operacionResumen.FirstOrDefault().montoDocumentos
                            , operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 15398791, 3); //KARINA MARLENE Peña Muñoz

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string PagarePesosENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";
                bool fiadores = false;

                //Si los representantes son fiadores
                if (rutR1 > 0)
                {
                    if (representante1.FirstOrDefault().fiador) fiadores = true;
                }
                if (rutR2 > 0)
                {
                    if (representante2.FirstOrDefault().fiador) fiadores = true;
                }
                if (fiadores)
                {
                    plantilla = "PagarePesos.Aval.docx";
                }
                else
                {
                    plantilla = "PagarePesos.docx";
                }
               

                //string dirPlantillas = Server.MapPath(@"~/Plantillas/").ToString();
                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#Nombrecliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle
                    //+ ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad
                    , options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#ComunaCliente#", cliente.FirstOrDefault().comuna, options);

                string representantesLegales = "";
                string avalesCliente = "";
                //REPRESENTANTE1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (repC != null)
                    {
                        representantesLegales += "REPRESENTANTE LEGAL\t\t: " + repC.nombre.ToUpper() + "\r";
                        representantesLegales += "C.I.\t\t\t\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";

                        //AVAL1
                        if (repC.fiador)
                        {
                            avalesCliente += "AVAL\t\t\t: " + repC.nombre.ToUpper() + "\r";
                            avalesCliente += "C.I.\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";
                        }
                    }
                }

                //REPRESENTANTE2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (repC != null)
                    {
                        representantesLegales += "REPRESENTANTE LEGAL 2\t\t: " + repC.nombre.ToUpper() + "\r";
                        representantesLegales += "C.I. 2\t\t\t\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";

                        //AVAL2
                        if (repC.fiador)
                        {
                            avalesCliente += "AVAL 2\t\t: " + repC.nombre.ToUpper() + "\r";
                            avalesCliente += "C.I. 2\t\t\t: " + string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper() + "\r";
                        }
                    }
                }
                doc.Range.Replace("#RepresentantesLegales#", representantesLegales, options);
                doc.Range.Replace("#AvalesCliente#", avalesCliente, options);

                //OPERACION
                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    if (operacionR.idSimulaOperacion == 0)
                    {
                        doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#FechaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    }
                    else
                    {
                        doc.Range.Replace("#FechaOtorgamiento#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#FechaContrato#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    }
                }

                doc.Range.Replace("#MontoCapital#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().montoFinanciado), options);
                doc.Range.Replace("#MontoCapitalPalabras#", string.Format("{0:C0}", funcionesUtiles.convertirNumeroEnPalabras((int)operacionResumen.FirstOrDefault().montoFinanciado)), options);
                doc.Range.Replace("#TasaInteres#", string.Format("{0:0.00}", operacionResumen.FirstOrDefault().tasa), options);


                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToBookmark("ListadoDocumentos");
                builder.ParagraphFormat.ClearFormatting();
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                builder.InsertCell();
                builder.Write("CUOTA");
                builder.InsertCell();
                builder.Write("FECHA PAGO");
                builder.InsertCell();
                builder.Write("CAPITAL");
                builder.InsertCell();
                builder.Write("INTERÉS");
                builder.InsertCell();
                builder.Write("VALOR CUOTA");
                builder.EndRow();

                int x = 0;
                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    x += 1;
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(12.4);
                    builder.Write(x.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(18.5);
                    builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(25);
                    builder.Write(string.Format("{0:C0}", operacionD.valorCesion));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(18.7);
                    builder.Write(string.Format("{0:C0}", operacionD.interesDocumento));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(25.2);
                    builder.Write(string.Format("{0:C0}", operacionD.valorDocto));
                    builder.EndRow();
                }
                builder.EndTable();


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            idSimulaOperacion
                            , 2 //Linea=1 - En curse=2 - Cursada=3
                            , 10 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , (DateTime)cliente.FirstOrDefault().fechaFirmaContrato //operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial.ToString()
                            , (int)operacionDetalle.Count() //operacionDetalle.Count()
                            , operacionResumen.FirstOrDefault().montoDocumentos
                            , operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 15398791, 3); //KARINA MARLENE Peña Muñoz

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string SolicitudDeCreditoENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {

                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);


                string plantilla = "SolicitudDeCredito.xls";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);


                //FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                //var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var templateWorkbook = new HSSFWorkbook(fs, true);
                Sheet hoja = templateWorkbook.GetSheet("Solicitud de Crédito PJ");

                NPOI.SS.UserModel.Row row;
                NPOI.SS.UserModel.Cell cell;

                //Fecha Operacion
                row = hoja.GetRow(3);
                cell = row.GetCell(9);
                cell.SetCellValue(((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"));

                //Nombre Cliente
                row = hoja.GetRow(7);
                cell = row.GetCell(3);
                cell.SetCellValue(cliente.FirstOrDefault().razonSocial.ToString());
                row = hoja.GetRow(77);
                cell = row.GetCell(3);
                cell.SetCellValue(cliente.FirstOrDefault().razonSocial.ToString());

                //Rut Cliente
                row = hoja.GetRow(9);
                cell = row.GetCell(3);
                cell.SetCellValue(string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper());
                row = hoja.GetRow(78);
                cell = row.GetCell(3);
                cell.SetCellValue(string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper());

                //Domicilio Cliente
                row = hoja.GetRow(9);
                cell = row.GetCell(8);
                cell.SetCellValue(cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna);
                row = hoja.GetRow(79);
                cell = row.GetCell(3);
                cell.SetCellValue(cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle);

                //Comuna Cliente
                row = hoja.GetRow(80);
                cell = row.GetCell(3);
                cell.SetCellValue(cliente.FirstOrDefault().comuna);

                //Ciudad Cliente
                row = hoja.GetRow(11);
                cell = row.GetCell(8);
                cell.SetCellValue(cliente.FirstOrDefault().ciudad);
                row = hoja.GetRow(80);
                cell = row.GetCell(8);
                cell.SetCellValue(cliente.FirstOrDefault().ciudad);

                //Moneda
                row = hoja.GetRow(26);
                cell = row.GetCell(9);
                cell.SetCellValue(operacionResumen.FirstOrDefault().moneda);

                //Monto Capital
                row = hoja.GetRow(31);
                cell = row.GetCell(2);
                cell.SetCellValue(string.Format("{0:N2}", operacionResumen.FirstOrDefault().montoFinanciado));

                //Primer Vencimiento
                row = hoja.GetRow(35);
                cell = row.GetCell(3);
                cell.SetCellValue(((DateTime)operacionDetalle.FirstOrDefault().fechaVcto).ToString("dd' de 'MMMM' de 'yyyy"));

                //Numero de Cuotas
                row = hoja.GetRow(37);
                cell = row.GetCell(3);
                cell.SetCellValue(operacionDetalle.Count());

                //Tasa Operacion
                row = hoja.GetRow(39);
                cell = row.GetCell(3);
                cell.SetCellValue(string.Format("{0:0.00}", operacionResumen.FirstOrDefault().tasa));

                //Ejecutivo
                row = hoja.GetRow(39);
                cell = row.GetCell(9);
                cell.SetCellValue(operacionResumen.FirstOrDefault().nombresEje + " " + operacionResumen.FirstOrDefault().apPatEje + " " + operacionResumen.FirstOrDefault().apMatEje);

                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                }
                else
                {
                    //Representante 1
                    using (var repC = representante1.FirstOrDefault())
                    {
                        if (repC != null)
                        {
                            //Nombre Representante
                            row = hoja.GetRow(82);
                            cell = row.GetCell(4);
                            cell.SetCellValue(repC.nombre.ToUpper());

                            //Rut Representante
                            row = hoja.GetRow(83);
                            cell = row.GetCell(2);
                            cell.SetCellValue(string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper());

                            //Direccion Representante
                            row = hoja.GetRow(84);
                            cell = row.GetCell(2);
                            cell.SetCellValue(repC.nombreCalle +
                                " " + repC.numeroCalle +
                                " " + repC.letraCalle
                            );

                            //Comuna Representante
                            row = hoja.GetRow(85);
                            cell = row.GetCell(2);
                            cell.SetCellValue(repC.comuna);

                            //Ciudad Representante
                            row = hoja.GetRow(85);
                            cell = row.GetCell(8);
                            cell.SetCellValue(repC.ciudad);

                            if (repC.fiador)
                            {
                                //Nombre Aval
                                row = hoja.GetRow(48);
                                cell = row.GetCell(3);
                                cell.SetCellValue(repC.nombre.ToUpper());
                                row = hoja.GetRow(100);
                                cell = row.GetCell(4);
                                cell.SetCellValue(repC.nombre.ToUpper());

                                //Rut Aval
                                row = hoja.GetRow(50);
                                cell = row.GetCell(3);
                                cell.SetCellValue(string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper());
                                row = hoja.GetRow(101);
                                cell = row.GetCell(2);
                                cell.SetCellValue(string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper());

                                //Direccion Aval
                                row = hoja.GetRow(50);
                                cell = row.GetCell(8);
                                cell.SetCellValue(repC.nombreCalle +
                                    " " + repC.numeroCalle +
                                    " " + repC.letraCalle +
                                    ", " + repC.comuna
                                );
                                row = hoja.GetRow(102);
                                cell = row.GetCell(2);
                                cell.SetCellValue(repC.nombreCalle +
                                    " " + repC.numeroCalle +
                                    " " + repC.letraCalle
                                );

                                //Comuna Aval
                                row = hoja.GetRow(103);
                                cell = row.GetCell(2);
                                cell.SetCellValue(repC.comuna);

                                //Ciudad Aval
                                row = hoja.GetRow(54);
                                cell = row.GetCell(8);
                                cell.SetCellValue(repC.ciudad);
                                row = hoja.GetRow(103);
                                cell = row.GetCell(8);
                                cell.SetCellValue(repC.ciudad);
                            }
                        }
                    }

                    //Representante 2
                    using (var repC = representante2.FirstOrDefault())
                    {
                        if (repC != null)
                        {
                            //Nombre Representante
                            row = hoja.GetRow(91);
                            cell = row.GetCell(4);
                            cell.SetCellValue(repC.nombre.ToUpper());

                            //Rut Representante
                            row = hoja.GetRow(92);
                            cell = row.GetCell(2);
                            cell.SetCellValue(string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper());

                            //Direccion Representante
                            row = hoja.GetRow(93);
                            cell = row.GetCell(2);
                            cell.SetCellValue(repC.nombreCalle +
                                " " + repC.numeroCalle +
                                " " + repC.letraCalle
                            );

                            //Comuna Representante
                            row = hoja.GetRow(94);
                            cell = row.GetCell(2);
                            cell.SetCellValue(repC.comuna);

                            //Ciudad Representante
                            row = hoja.GetRow(94);
                            cell = row.GetCell(8);
                            cell.SetCellValue(repC.ciudad);

                            if (repC.fiador)
                            {
                                //Nombre Aval
                                row = hoja.GetRow(62);
                                cell = row.GetCell(3);
                                cell.SetCellValue(repC.nombre.ToUpper());
                                row = hoja.GetRow(109);
                                cell = row.GetCell(4);
                                cell.SetCellValue(repC.nombre.ToUpper());

                                //Rut Aval
                                row = hoja.GetRow(64);
                                cell = row.GetCell(3);
                                cell.SetCellValue(string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper());
                                row = hoja.GetRow(110);
                                cell = row.GetCell(2);
                                cell.SetCellValue(string.Format("{0:N0}", repC.rutRepresentante) + "-" + repC.dvRepresentante.ToUpper());

                                //Direccion Aval
                                row = hoja.GetRow(64);
                                cell = row.GetCell(8);
                                cell.SetCellValue(repC.nombreCalle +
                                    " " + repC.numeroCalle +
                                    " " + repC.letraCalle +
                                    ", " + repC.comuna
                                );
                                row = hoja.GetRow(111);
                                cell = row.GetCell(2);
                                cell.SetCellValue(repC.nombreCalle +
                                    " " + repC.numeroCalle +
                                    " " + repC.letraCalle
                                );

                                //Comuna Aval
                                row = hoja.GetRow(112);
                                cell = row.GetCell(2);
                                cell.SetCellValue(repC.comuna);

                                //Ciudad Aval
                                row = hoja.GetRow(68);
                                cell = row.GetCell(8);
                                cell.SetCellValue(repC.ciudad);
                                row = hoja.GetRow(112);
                                cell = row.GetCell(8);
                                cell.SetCellValue(repC.ciudad);
                            }
                        }
                    }

                }

                MemoryStream ms = new MemoryStream();

                //nuevo archivo
                String strNombreArchivo = dirDocumentosTemp + rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name + ".xls";
                System.IO.FileStream archivoSalida = System.IO.File.Create(strNombreArchivo);
                fs.Close();
                templateWorkbook.Write(archivoSalida);
                archivoSalida.Close();

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                //string fileSaveAsPath = "";
                if (formato.ToUpper() == "EXCEL")
                {
                    fileSaveAs = fileSaveAs + ".xls";

                    Response.Clear();
                    //Response.ContentType = "Application/ms-excel";
                    Response.ContentType = "Application/vnd.ms-excel";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(strNombreArchivo);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ConstanciaRenegociacionENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                //Validar Fechas Personerias
                if (rutR1 > 0)
                {
                    if ((representante1.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if ((representante2.FirstOrDefault().fechaPersoneria) == null)
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> no tiene fecha personería!</p>");
                    }
                }
                //Validar notaria
                if (notario.FirstOrDefault().nombreNotario == null)
                {
                    resultado += WebUtility.HtmlDecode("<p>El cliente no tiene <strong>notaria asignada</strong>!</p>");
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }

                string plantilla = "";
                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "ConstanciaRenegociacion.NAT.docx";
                }
                else
                {
                    plantilla = "ConstanciaRenegociacion.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)operacionResumen.FirstOrDefault().fechaOtorgamiento).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DatosCliente#",
                    ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle +
                    " " + cliente.FirstOrDefault().numeroCalle +
                    " " + cliente.FirstOrDefault().letraCalle +
                    ", comuna de " + cliente.FirstOrDefault().comuna +
                    ", ciudad de " + cliente.FirstOrDefault().ciudad
                    , options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#NotariaNombreNotario#", notario.FirstOrDefault().nombreNotario, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //doc.Range.Replace("#RepresentantePenta#", representantePenta1.FirstOrDefault().nombreCompleto, options);
                //doc.Range.Replace("#RepresentantePentaRut#", string.Format("{0:N0}", representantePenta1.FirstOrDefault().rut) + "-" + representantePenta1.FirstOrDefault().dv.ToUpper(), options);

                //Datos Representantes
                var datosRepresentantes = "";
                //var datosRepresentantesMinimos = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante1#",
                        ", cédula nacional de identidad número " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante;
                        doc.Range.Replace("#YPor#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DatosRepresentante2#",
                            ", cédula nacional de identidad número " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                        //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                        doc.Range.Replace("#ambos#", "ambos ", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "de los representantes");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPor#", "", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#DatosRepresentante2#", "", options);
                        doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                        doc.Range.Replace("#ambos#", "", options);
                        //FindAndReplace(wordApp, "#de/delosrepesentantes#", "del representante");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    }
                    //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                    if (rutR2 > 0)
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                        doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                    }
                    else
                    {
                        doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                        doc.Range.Replace("#AmbosPP#", "", options);
                    }
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                doc.Range.Replace("#MontoCuotas#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().montoDocumentos), options);
                doc.Range.Replace("#MontoCapital#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().montoFinanciado), options);
                doc.Range.Replace("#MontoCapitalPalabras#", string.Format("{0:C0}", funcionesUtiles.convertirNumeroEnPalabras((int)operacionResumen.FirstOrDefault().montoFinanciado)), options);
                doc.Range.Replace("#TasaInteres#", string.Format("{0:0.00}", operacionResumen.FirstOrDefault().tasa), options);


                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToBookmark("TablaDetalleCuotas");
                builder.ParagraphFormat.ClearFormatting();
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                //builder.InsertCell();
                //builder.Write("CUOTA");
                builder.InsertCell();
                builder.Write("VENCIMIENTO");
                builder.InsertCell();
                builder.Write("MONTO CUOTA");
                builder.InsertCell();
                builder.Write("INTERÉS");
                builder.InsertCell();
                builder.Write("CAPITAL");
                builder.EndRow();

                int x = 0;
                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    x += 1;
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(20.8);
                    builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(28.9);
                    builder.Write(string.Format("{0:C0}", operacionD.valorDocto));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(24);
                    builder.Write(string.Format("{0:C0}", operacionD.interesDocumento));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(26.1);
                    builder.Write(string.Format("{0:C0}", operacionD.valorCesion));
                    builder.EndRow();
                }
                builder.EndTable();

                //doc.Range.Replace("#PrecioDeCompra#", "$" + string.Format("{0:N0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#PrecioDeCompra#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().valorCesion), options);
                doc.Range.Replace("#SaldoPorPagar#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().AGirarEjec), options);
                doc.Range.Replace("#SaldoPendiente#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().saldoPendiente), options);

                doc.Range.Replace("#TotalDocumentos#", operacionDetalle.Count().ToString(), options);
                doc.Range.Replace("#TotalOperacion#", string.Format("{0:C0}", operacionResumen.FirstOrDefault().montoDocumentos), options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "SEND-PDF")
                {
                    //Se agrega un salto de página, para posterior inserta las firmas digitales
                    //DocumentBuilder builder = new DocumentBuilder(doc);
                    builder.MoveToDocumentEnd();
                    builder.InsertBreak(BreakType.PageBreak);

                    totalPaginasDocumento = doc.PageCount;

                    fileSaveAs = fileSaveAs + ".FE.pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    //Convertir en BASE64
                    Byte[] bytes = System.IO.File.ReadAllBytes(fileSaveAsPath);
                    String fileBase64 = Convert.ToBase64String(bytes);

                    //Agregar/Enviar Contrato
                    IEnumerable<ContratoEnviadoDTO> contratoEnviado = new List<ContratoEnviadoDTO>();
                    contratoEnviado =
                        gestorDeContratos.agregarContratoEnviado(
                            idSimulaOperacion
                            , 2 //Linea - En curse - Cursada
                            , 5 //TipoContrato
                            , System.Environment.UserName.ToString()
                            , rutCliente
                            , fileBase64
                            , totalPaginasDocumento
                            , operacionResumen.FirstOrDefault().fechaEmision
                            , cliente.FirstOrDefault().razonSocial
                            , (int)operacionDetalle.Count() //operacionDetalle.Count()
                            , operacionResumen.FirstOrDefault().montoDocumentos
                            , operacionResumen.FirstOrDefault().montoFinanciado
                        );

                    //Agregar Firmantes
                    if (cliente.FirstOrDefault().esPersonaNatural)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, rutCliente, 1);
                    }
                    if (rutR1 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante1.FirstOrDefault().rutRepresentante, 1);
                    }
                    if (rutR2 > 0)
                    {
                        gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, representante2.FirstOrDefault().rutRepresentante, 1);
                    }
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 16788841, 2); // Representante Penta FELIPE

                    gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 10755410, 3); //Notario 36° NOTARÍA	SANTIAGO	10755410	6	ANDRES FELIPE RIEUTORD ALVARADO	arieutord@notariarieutord.cl	36° NOTARÍA - ANDRES FELIPE RIEUTORD ALVARADO
                    //gestorDeContratos.agregarContratoEnviadoFirma(contratoEnviado.FirstOrDefault().idContratoEnviado, 15398791, 3); //KARINA MARLENE Peña Muñoz

                    resultado = "<p>CONTRATO ENVIADO!!</p>";
                    resultado += "<p><a href='../Plantillas/TempFiles/" + fileSaveAs + "'>DESCARGAR</a></p>";
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }


                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string CartaDeposito3roLINEA(string formato, int rutCliente, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);


                string plantilla = "";
                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "CartaDeposito3ro.Nat.docx";
                }
                else
                {
                    plantilla = "CartaDeposito3ro.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#Nombrecliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#Rutcliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);

                //Datos Representantes
                var datosRepresentantes = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += "don(ña) " + representante1.FirstOrDefault().nombre + ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                }
                if (rutR2 > 0)
                {
                    datosRepresentantes += ", y don(ña) " + representante2.FirstOrDefault().nombre + ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#ambos#", "ambos", options);
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#ambos#", "", options);
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);

                }
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);

                doc.Range.Replace("#FechaActual#", DateTime.Now.ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#FechaActual+6Meses#", DateTime.Now.AddMonths(6).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#FechaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string CartaDeposito3roPuntualENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                string plantilla = "";
                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "CartaDeposito3roPuntual.Nat.docx";
                }
                else
                {
                    plantilla = "CartaDeposito3roPuntual.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#Nombrecliente#", cliente.FirstOrDefault().razonSocial, options);
                doc.Range.Replace("#Rutcliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);

                //Datos Representantes
                var datosRepresentantes = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += "don(ña) " + representante1.FirstOrDefault().nombre + ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                }
                if (rutR2 > 0)
                {
                    datosRepresentantes += ", y don(ña) " + representante2.FirstOrDefault().nombre + ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#ambos#", "ambos", options);
                }
                else
                {
                    doc.Range.Replace("#ambos#", "", options);
                }
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);

                
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOtorgamiento#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#FechaContrato#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);

                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string CartaRedireccionamientoENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();


                string plantilla = "CartaRedireccionamiento.docx";

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#DireccionSucursal#", sucursal.FirstOrDefault().direccion + "," + sucursal.FirstOrDefault().comuna, options);
                doc.Range.Replace("#CiudadSucursal#", sucursal.FirstOrDefault().ciudad, options);

                doc.Range.Replace("#NOMBRECLIENTE#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#MAILAUTORIZACARTAPODER#", cliente.FirstOrDefault().email1.ToUpper(), options);

                if (rutR1 > 0)
                {
                    using (var rep = representante1.FirstOrDefault())
                    {
                        doc.Range.Replace("#REPRESENTANTELEGAL1#", "REPRESENTANTE LEGAL:\t\t", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTELEGAL#", rep.nombre.ToUpper() + "\r", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#REPRESENTANTELEGAL1#", "", options);
                    doc.Range.Replace("#NOMBREREPRESENTANTELEGAL#", "", options);
                }
                
                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#MAILEJECUTIVOCLIENTE#", operacionR.mailEje.ToUpper(), options);
                    doc.Range.Replace("#MONTOOPERACIÓN#", string.Format("{0:N0}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#Moneda#", operacionR.moneda, options);

                    if (operacionR.idSimulaOperacion == 0)
                    {
                        doc.Range.Replace("#FechaOtorgamiento#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#FechaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    }
                    else
                    {
                        doc.Range.Replace("#FechaOtorgamiento#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#FechaContrato#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    }
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                DocumentBuilder builder = new DocumentBuilder(doc);
                builder.MoveToBookmark("ListadoDocumentos");
                builder.ParagraphFormat.ClearFormatting();
                builder.StartTable();
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                //builder.CellFormat.Width = 100;

                builder.CellFormat.HorizontalMerge = CellMerge.None;
                builder.InsertCell();
                builder.CellFormat.Shading.BackgroundPatternColor = Color.FromArgb(255, 192, 0);//(255,12,1);
                builder.Write("BENEFICIARIO");
                builder.InsertCell();
                builder.Write("RUT");
                builder.InsertCell();
                builder.Write("CERTIFICADO");
                builder.InsertCell();
                builder.Write("MONTO");
                builder.EndRow();

                int x = 0;
                foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                {
                    x += 1;
                    builder.InsertCell();
                    builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(41.7);
                    //builder.Write(x.ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(17.6);
                    //builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(19.2);
                    //builder.Write(string.Format("{0:N2}", operacionD.valorCesion));
                    builder.Write(string.Format("{0:N0}", operacionD.numDocto));
                    builder.InsertCell();
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(21.3);
                    builder.Write(string.Format("{0:N2}", operacionD.valorDoctoEnMoneda));
                    builder.EndRow();
                }
                builder.EndTable();


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoCesionDS255ENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }


                string plantilla = "ContratoCesionDS255.docx";

                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "ContratoCesionDS255.NAT.docx";
                }
                else
                {
                    plantilla = "ContratoCesionDS255.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", comuna de " + sucursal.FirstOrDefault().comuna + ", ciudad de " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NOMBRECLIENTE#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#FechaContratoMarco#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().nombreNotario + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                var datosRepresentantes = "";
                if (rutR1 > 0)
                {
                datosRepresentantes += representante1.FirstOrDefault().nombre +
                    ", " + representante1.FirstOrDefault().nacionalidad +
                    ", " + representante1.FirstOrDefault().estadoCivil +
                    ", " + representante1.FirstOrDefault().profesion +
                    ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#RutRepresentante1#",
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );

                if (rutR2 > 0)
                {
                    datosRepresentantes += ", y por " + representante2.FirstOrDefault().nombre +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#RutRepresentante2#",
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                    //FindAndReplace(wordApp, "#TodosDomiciliados#", "todos domiciliados");
                        doc.Range.Replace("#ambos#", " ambos ", options);
                        doc.Range.Replace("#y#", " y ", options);
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                }
                else
                {
                    //FindAndReplace(wordApp, "#TodosDomiciliados#", "domiciliado");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                        doc.Range.Replace("#y#", "", options);
                        doc.Range.Replace("#ambos#", "", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#RutRepresentante2#", "", options);
                }
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);

                }


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper(), options);
                    //if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    //if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    //if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    //if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            ", y por " +
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper(), options);

                        datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", "", options);
                    }
                }

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


                /* DocumentBuilder builder = new DocumentBuilder(doc);
                 builder.MoveToBookmark("ListadoDocumentos");
                 builder.ParagraphFormat.ClearFormatting();
                 builder.StartTable();
                 builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                 builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                 //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                 //builder.CellFormat.Width = 100;

                 builder.CellFormat.HorizontalMerge = CellMerge.None;
                 builder.InsertCell();
                 builder.CellFormat.Shading.BackgroundPatternColor = Color.FromArgb(255, 192, 0);//(255,12,1);
                 builder.Write("BENEFICIARIO");
                 builder.InsertCell();
                 builder.Write("RUT");
                 builder.InsertCell();
                 builder.Write("CERTIFICADO");
                 builder.InsertCell();
                 builder.Write("MONTO");
                 builder.EndRow();

                 int x = 0;
                 foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                 {
                     x += 1;
                     builder.InsertCell();
                     builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(41.7);
                     //builder.Write(x.ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(17.6);
                     //builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(19.2);
                     //builder.Write(string.Format("{0:N2}", operacionD.valorCesion));
                     builder.Write(string.Format("{0:N0}", operacionD.numDocto));
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(21.3);
                     builder.Write(string.Format("{0:N2}", operacionD.valorDoctoEnMoneda));
                     builder.EndRow();
                 }
                 builder.EndTable();
                 */

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string ContratoCesionDS1ENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }


                string plantilla = "ContratoCesionDS1.docx";

                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "ContratoCesionDS1.NAT.docx";
                }
                else
                {
                    plantilla = "ContratoCesionDS1.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", comuna de " + sucursal.FirstOrDefault().comuna + ", ciudad de " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NOMBRECLIENTE#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                //doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                //doc.Range.Replace("#RutRepresentante1#", representante1.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#FechaContratoMarco#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().nombreNotario + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                var datosRepresentantes = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre +
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#RutRepresentante1#",
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y por " + representante2.FirstOrDefault().nombre +
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#RutRepresentante2#",
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        //FindAndReplace(wordApp, "#TodosDomiciliados#", "todos domiciliados");
                        doc.Range.Replace("#ambos#", " ambos ", options);
                        doc.Range.Replace("#la/sFirma/s#", " las Firmas ", options);
                        doc.Range.Replace("#y#", " y ", options);
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        //FindAndReplace(wordApp, "#TodosDomiciliados#", "domiciliado");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                        doc.Range.Replace("#y#", "", options);
                        doc.Range.Replace("#ambos#", "", options);
                        doc.Range.Replace("#la/sFirma/s#", " la Firma ", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#RutRepresentante2#", "", options);
                    }
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);

                }


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper(), options);
                    //if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    //if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    //if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    //if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            ", y por " +
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper(), options);

                        datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                       
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", "", options);

                    }
                }

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


                /* DocumentBuilder builder = new DocumentBuilder(doc);
                 builder.MoveToBookmark("ListadoDocumentos");
                 builder.ParagraphFormat.ClearFormatting();
                 builder.StartTable();
                 builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                 builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                 //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                 //builder.CellFormat.Width = 100;

                 builder.CellFormat.HorizontalMerge = CellMerge.None;
                 builder.InsertCell();
                 builder.CellFormat.Shading.BackgroundPatternColor = Color.FromArgb(255, 192, 0);//(255,12,1);
                 builder.Write("BENEFICIARIO");
                 builder.InsertCell();
                 builder.Write("RUT");
                 builder.InsertCell();
                 builder.Write("CERTIFICADO");
                 builder.InsertCell();
                 builder.Write("MONTO");
                 builder.EndRow();

                 int x = 0;
                 foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                 {
                     x += 1;
                     builder.InsertCell();
                     builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(41.7);
                     //builder.Write(x.ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(17.6);
                     //builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(19.2);
                     //builder.Write(string.Format("{0:N2}", operacionD.valorCesion));
                     builder.Write(string.Format("{0:N0}", operacionD.numDocto));
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(21.3);
                     builder.Write(string.Format("{0:N2}", operacionD.valorDoctoEnMoneda));
                     builder.EndRow();
                 }
                 builder.EndTable();
                 */

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string dsN1DocPrivadoENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }


                string plantilla = "";

                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "DS.N1.MandatoEspecialIrrevocable.DocPrivado.Natural.docx";
                }
                else
                {
                    plantilla = "DS.N1.MandatoEspecialIrrevocable.DocPrivado.Juridico.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", comuna de " + sucursal.FirstOrDefault().comuna + ", ciudad de " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NOMBRECLIENTE#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                //doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                //doc.Range.Replace("#RutRepresentante1#", representante1.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#FechaContratoMarco#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().nombreNotario + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                var datosRepresentantes = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre +
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#RutRepresentante1#",
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y por " + representante2.FirstOrDefault().nombre +
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#RutRepresentante2#",
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        //FindAndReplace(wordApp, "#TodosDomiciliados#", "todos domiciliados");
                        doc.Range.Replace("#ambos#", " ambos ", options);
                        doc.Range.Replace("#la/sFirma/s#", " las Firmas ", options);
                        doc.Range.Replace("#y#", " y ", options);
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        //FindAndReplace(wordApp, "#TodosDomiciliados#", "domiciliado");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                        doc.Range.Replace("#y#", "", options);
                        doc.Range.Replace("#ambos#", "", options);
                        doc.Range.Replace("#la/sFirma/s#", " la Firma ", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#RutRepresentante2#", "", options);
                    }
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);

                }


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper(), options);
                    //if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    //if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    //if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    //if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            ", y por " +
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper(), options);

                        datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                       
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", "", options);

                    }
                }

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


                /* DocumentBuilder builder = new DocumentBuilder(doc);
                 builder.MoveToBookmark("ListadoDocumentos");
                 builder.ParagraphFormat.ClearFormatting();
                 builder.StartTable();
                 builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                 builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                 //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                 //builder.CellFormat.Width = 100;

                 builder.CellFormat.HorizontalMerge = CellMerge.None;
                 builder.InsertCell();
                 builder.CellFormat.Shading.BackgroundPatternColor = Color.FromArgb(255, 192, 0);//(255,12,1);
                 builder.Write("BENEFICIARIO");
                 builder.InsertCell();
                 builder.Write("RUT");
                 builder.InsertCell();
                 builder.Write("CERTIFICADO");
                 builder.InsertCell();
                 builder.Write("MONTO");
                 builder.EndRow();

                 int x = 0;
                 foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                 {
                     x += 1;
                     builder.InsertCell();
                     builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(41.7);
                     //builder.Write(x.ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(17.6);
                     //builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(19.2);
                     //builder.Write(string.Format("{0:N2}", operacionD.valorCesion));
                     builder.Write(string.Format("{0:N0}", operacionD.numDocto));
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(21.3);
                     builder.Write(string.Format("{0:N2}", operacionD.valorDoctoEnMoneda));
                     builder.EndRow();
                 }
                 builder.EndTable();
                 */

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }
     
            return resultado;
        }

        public string dsN1EscriPublicaENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "DS.N1.MandatoEspecialIrrevocable.EscrituraPublica.Natural.docx";
                }
                else
                {
                    plantilla = "DS.N1.MandatoEspecialIrrevocable.EscrituraPublica.Juridica.docx";
                }


                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DatosCliente#",
                    cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                //doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioClientePalabras#", cliente.FirstOrDefault().nombreCalle + " número " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                //doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioSucursalPalabras#", sucursal.FirstOrDefault().direccionPalabras, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                int cantidadAvales = 0;
                if (rutR1 > 0)
                {
                    //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().rutPalabra))
                        datosRepresentante1 += ", cédula de identidad número " + representante1.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DomicilioAval1#",
                            representante1.FirstOrDefault().nombreCalle +
                            " " + representante1.FirstOrDefault().numeroCalle +
                            " " + representante1.FirstOrDefault().letraCalle +
                            ", " + representante1.FirstOrDefault().comuna +
                            ", " + representante1.FirstOrDefault().ciudad
                            , options
                        );
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "", options);
                        doc.Range.Replace("#DomicilioAval1#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }

                //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#YPor#", ", y don(ña) ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().rutPalabra))
                        datosRepresentante2 += ", cédula de identidad número " + representante2.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);


                    datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante2#",
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "de los representantes", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria + " y " + representante2.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    doc.Range.Replace("#FechaPersoneria#",
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                        ", y de " +
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                        , options
                    );

                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria +
                        ", y " + representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );
                    doc.Range.Replace("#otorgada/s#", "otorgadas", options);


                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "del representante", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    if (rutR1 > 0)
                    {
                        doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    }

                    doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }

                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }

                if (rutR1 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                }
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string dsN49DocPrivadoENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";
            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }


                string plantilla = "";

                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "DS.N49.MandatoEspecialIrrevocable.DocPrivado.Natural.docx";
                }
                else
                {
                    plantilla = "DS.N49.MandatoEspecialIrrevocable.DocPrivado.Juridica.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", comuna de " + sucursal.FirstOrDefault().comuna + ", ciudad de " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NOMBRECLIENTE#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                //doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                //doc.Range.Replace("#RutRepresentante1#", representante1.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#FechaContratoMarco#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().nombreNotario + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                var datosRepresentantes = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre +
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#RutRepresentante1#",
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y por " + representante2.FirstOrDefault().nombre +
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#RutRepresentante2#",
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        //FindAndReplace(wordApp, "#TodosDomiciliados#", "todos domiciliados");
                        doc.Range.Replace("#ambos#", " ambos ", options);
                        doc.Range.Replace("#la/sFirma/s#", " las Firmas ", options);
                        doc.Range.Replace("#y#", " y ", options);
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        //FindAndReplace(wordApp, "#TodosDomiciliados#", "domiciliado");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                        doc.Range.Replace("#y#", "", options);
                        doc.Range.Replace("#ambos#", "", options);
                        doc.Range.Replace("#la/sFirma/s#", " la Firma ", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#RutRepresentante2#", "", options);
                    }
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);

                }


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper(), options);
                    //if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    //if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    //if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    //if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            ", y por " +
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper(), options);

                        datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);

                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", "", options);

                    }
                }

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


                /* DocumentBuilder builder = new DocumentBuilder(doc);
                 builder.MoveToBookmark("ListadoDocumentos");
                 builder.ParagraphFormat.ClearFormatting();
                 builder.StartTable();
                 builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                 builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                 //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                 //builder.CellFormat.Width = 100;

                 builder.CellFormat.HorizontalMerge = CellMerge.None;
                 builder.InsertCell();
                 builder.CellFormat.Shading.BackgroundPatternColor = Color.FromArgb(255, 192, 0);//(255,12,1);
                 builder.Write("BENEFICIARIO");
                 builder.InsertCell();
                 builder.Write("RUT");
                 builder.InsertCell();
                 builder.Write("CERTIFICADO");
                 builder.InsertCell();
                 builder.Write("MONTO");
                 builder.EndRow();

                 int x = 0;
                 foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                 {
                     x += 1;
                     builder.InsertCell();
                     builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(41.7);
                     //builder.Write(x.ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(17.6);
                     //builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(19.2);
                     //builder.Write(string.Format("{0:N2}", operacionD.valorCesion));
                     builder.Write(string.Format("{0:N0}", operacionD.numDocto));
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(21.3);
                     builder.Write(string.Format("{0:N2}", operacionD.valorDoctoEnMoneda));
                     builder.EndRow();
                 }
                 builder.EndTable();
                 */

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string dsN49EscriPublicaENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";
            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "DS.N49.MandatoEspecialIrrevocable.EscrituraPublica.Natural.docx";
                }
                else
                {
                    plantilla = "DS.N49.MandatoEspecialIrrevocable.EscrituraPublica.Juridica.docx";
                }


                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DatosCliente#",
                    cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                //doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioClientePalabras#", cliente.FirstOrDefault().nombreCalle + " número " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                //doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioSucursalPalabras#", sucursal.FirstOrDefault().direccionPalabras, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                int cantidadAvales = 0;
                if (rutR1 > 0)
                {
                    //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().rutPalabra))
                        datosRepresentante1 += ", cédula de identidad número " + representante1.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DomicilioAval1#",
                            representante1.FirstOrDefault().nombreCalle +
                            " " + representante1.FirstOrDefault().numeroCalle +
                            " " + representante1.FirstOrDefault().letraCalle +
                            ", " + representante1.FirstOrDefault().comuna +
                            ", " + representante1.FirstOrDefault().ciudad
                            , options
                        );
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "", options);
                        doc.Range.Replace("#DomicilioAval1#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }

                //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#YPor#", ", y don(ña) ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().rutPalabra))
                        datosRepresentante2 += ", cédula de identidad número " + representante2.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);


                    datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante2#",
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "de los representantes", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria + " y " + representante2.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    doc.Range.Replace("#FechaPersoneria#",
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                        ", y de " +
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                        , options
                    );

                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria +
                        ", y " + representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );
                    doc.Range.Replace("#otorgada/s#", "otorgadas", options);


                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "del representante", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    if (rutR1 > 0)
                    {
                        doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    }

                    doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }

                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }

                if (rutR1 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                }
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string dsN255EscriPublicaENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";
            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(sucursal.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }

                FuncionesUtiles funcionesUtiles = new FuncionesUtiles();

                string plantilla = "";
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "DS.N255.MandatoEspecialIrrevocable.EscrituraPublica.Natural.docx";
                }
                else
                {
                    plantilla = "DS.N255.MandatoEspecialIrrevocable.EscrituraPublica.Juridica.docx";
                }


                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);


                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#NombreCliente#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv, options);
                doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DatosCliente#",
                    cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                //doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", " + cliente.FirstOrDefault().comuna + ", " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioClientePalabras#", cliente.FirstOrDefault().nombreCalle + " número " + cliente.FirstOrDefault().numeroCallePalabra + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                //doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", " + sucursal.FirstOrDefault().comuna + ", " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#DomicilioSucursalPalabras#", sucursal.FirstOrDefault().direccionPalabras, options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria, options);
                doc.Range.Replace("#FechaFirmaContrato#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);

                //Datos Representantes
                var datosRepresentantes = "";
                var datosRepresentante1 = "";
                var datosRepresentante2 = "";
                int cantidadAvales = 0;
                if (rutR1 > 0)
                {
                    //datosRepresentantes += cliente.FirstOrDefault().nombreRepresentante1.ToUpper() + ", C.N.I. " + string.Format("{0:N0}", cliente.FirstOrDefault().rutRepresentante1) + "-" + cliente.FirstOrDefault().dvRepresentante1;
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidad))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().estadoCivil))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().profesion))
                        datosRepresentante1 += ", " + representante1.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante1.FirstOrDefault().rutPalabra))
                        datosRepresentante1 += ", cédula de identidad número " + representante1.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante1#", datosRepresentante1, options);

                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "don(ña) " + representante1.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#DomicilioAval1#",
                            representante1.FirstOrDefault().nombreCalle +
                            " " + representante1.FirstOrDefault().numeroCalle +
                            " " + representante1.FirstOrDefault().letraCalle +
                            ", " + representante1.FirstOrDefault().comuna +
                            ", " + representante1.FirstOrDefault().ciudad
                            , options
                        );
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL1#", "", options);
                        doc.Range.Replace("#DomicilioAval1#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#NOMBREAVAL1#", "", options);
                    doc.Range.Replace("#DomicilioAval1#", "", options);
                }

                //datosRepresentantesMinimos += "don(ña) " + cliente.nombreRepresentante1;
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#YPor#", ", y don(ña) ", options);
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidad))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().nacionalidad;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().estadoCivil))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().estadoCivil;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().profesion))
                        datosRepresentante2 += ", " + representante2.FirstOrDefault().profesion;
                    if (!string.IsNullOrEmpty(representante2.FirstOrDefault().rutPalabra))
                        datosRepresentante2 += ", cédula de identidad número " + representante2.FirstOrDefault().rutPalabra;
                    doc.Range.Replace("#DatosRepresentante2#", datosRepresentante2, options);


                    datosRepresentantes += ", y de " + representante2.FirstOrDefault().nombre + ", C.N.I. " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#DatosRepresentante2#",
                        ", " + representante2.FirstOrDefault().estadoCivil +
                        ", " + representante2.FirstOrDefault().profesion +
                        ", " + representante2.FirstOrDefault().nacionalidad +
                        ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "todos domiciliados en", options);
                    //datosRepresentantesMinimos += ", y don(ña) " + cliente.nombreRepresentante2;
                    doc.Range.Replace("#ambos#", "ambos ", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "de los representantes", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") + " y " + ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria + " y " + representante2.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                    doc.Range.Replace("#FechaPersoneria#",
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria) +
                        ", y de " +
                        funcionesUtiles.convertirFechaEnPalabras((DateTime)representante2.FirstOrDefault().fechaPersoneria)
                        , options
                    );

                    doc.Range.Replace("#NotariaPersoneria#",
                        representante1.FirstOrDefault().notariaPersoneria +
                        ", y " + representante2.FirstOrDefault().notariaPersoneria +
                        " respectivamente"
                        , options
                    );
                    doc.Range.Replace("#otorgada/s#", "otorgadas", options);


                    //Datos como AVAl1
                    if (representante1.FirstOrDefault().fiador)
                    {
                        if (cantidadAvales > 0)
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", ", y don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                ", y " +
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        else
                        {
                            doc.Range.Replace("#NOMBREAVAL2#", "don(ña) " + representante2.FirstOrDefault().nombre.ToUpper(), options);
                            doc.Range.Replace("#DomicilioAval2#",
                                representante2.FirstOrDefault().nombreCalle +
                                " " + representante2.FirstOrDefault().numeroCalle +
                                " " + representante2.FirstOrDefault().letraCalle +
                                ", " + representante2.FirstOrDefault().comuna +
                                ", " + representante2.FirstOrDefault().ciudad
                                , options
                            );
                        }
                        cantidadAvales += 1;
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREAVAL2#", "", options);
                    }
                }
                else
                {
                    doc.Range.Replace("#YPor#", "", options);
                    doc.Range.Replace("#NombreRepresentante2#", "", options);
                    doc.Range.Replace("#DatosRepresentante2#", "", options);
                    doc.Range.Replace("#Todos/DomiciliadosEn#", "domiciliado en", options);
                    doc.Range.Replace("#ambos#", "", options);
                    //doc.Range.Replace("#de/delosrepesentantes#", "del representante", options);
                    //doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                    //doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                    if (rutR1 > 0)
                    {
                        doc.Range.Replace("#FechaPersoneria#", funcionesUtiles.convertirFechaEnPalabras((DateTime)representante1.FirstOrDefault().fechaPersoneria), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                    }

                    doc.Range.Replace("#otorgada/s#", "otorgada", options);
                    doc.Range.Replace("#NOMBREAVAL2#", "", options);
                }
                //FindAndReplace(wordApp, "#DatosMinimosRepresentantes#", datosRepresentantesMinimos);
                doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper()
                        , options
                    );
                    if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#YPorRep#", ", y por don(ña) ", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper()
                            , options
                        );
                        if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                        if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                        if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                        if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#YPorRep#", "", options);
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                }

                //Datos Conyuge 1
                using (var repC = representante1.FirstOrDefault())
                {
                    if (rutR1 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE1#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge1#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge1#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu1#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge1#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge1#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge1#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza1#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE1#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge1#", "", options);
                        doc.Range.Replace("#CasadoConyuge1#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu1#", "", options);
                        doc.Range.Replace("#ProfesionConyuge1#", "", options);
                        doc.Range.Replace("#RutConyuge1#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge1#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza1#", "", options);
                    }
                }

                //Datos Conyuge 2
                using (var repC = representante2.FirstOrDefault())
                {
                    if (rutR2 > 0)
                    {
                        if (repC.idEstadoCivil == 1)
                        {
                            doc.Range.Replace("#NOMBRECONYUGE2#", "don(ña) " + repC.nombreConyuge, options);
                            doc.Range.Replace("#NacionalidadConyuge2#", ", " + repC.nacionalidadConyuge, options);
                            doc.Range.Replace("#CasadoConyuge2#", ", " + repC.estadoCivil + " con el constituyente ", options);
                            doc.Range.Replace("#NombreRepresentanteConyu2#", "don(ña) " + repC.nombre, options);
                            doc.Range.Replace("#ProfesionConyuge2#", ", " + repC.profesionConyuge, options);
                            doc.Range.Replace("#RutConyuge2#", ", cédula nacional de identidad número " + repC.rutConyugePalabra + ", y de su mismo domicilio", options);
                            doc.Range.Replace("#AcreditaIdentidadConyuge2#", ", quien acredita su identidad con la cédula antes citada y expone: Que para todos los efectos legales, autoriza a su cónyuge ", options);
                            doc.Range.Replace("#ParaConstituirLaFianza2#", ", para constituir la fianza y codeuda solidaria que se conviene en la presente escritura, cuyos términos declara conocer y aceptar en todas sus partes", options);
                        }
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBRECONYUGE2#", "", options);
                        doc.Range.Replace("#NacionalidadConyuge2#", "", options);
                        doc.Range.Replace("#CasadoConyuge2#", "", options);
                        doc.Range.Replace("#NombreRepresentanteConyu2#", "", options);
                        doc.Range.Replace("#ProfesionConyuge2#", "", options);
                        doc.Range.Replace("#RutConyuge2#", "", options);
                        doc.Range.Replace("#AcreditaIdentidadConyuge2#", "", options);
                        doc.Range.Replace("#ParaConstituirLaFianza2#", "", options);
                    }
                }

                if (rutR1 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente1#", representante1.FirstOrDefault().nombre, options);
                }
                if (rutR2 > 0)
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "                " + representante2.FirstOrDefault().nombre, options);
                    doc.Range.Replace("#AmbosPP#", "ambos pp.", options);
                }
                else
                {
                    doc.Range.Replace("#FirmaNombreRepresentanteCliente2#", "", options);
                    doc.Range.Replace("#AmbosPP#", "", options);
                }

                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().notaria + " de " + notario.FirstOrDefault().ciudadNotaria, options);

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string dsN255DocPrivadoENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";
            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }


                string plantilla = "";

                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "DS.N255.MandatoEspecialIrrevocable.DocPrivado.Natural.docx";
                }
                else
                {
                    plantilla = "DS.N255.MandatoEspecialIrrevocable.DocPrivado.Juridica.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", comuna de " + sucursal.FirstOrDefault().comuna + ", ciudad de " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NOMBRECLIENTE#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#DatosCliente#",
                    ", " + cliente.FirstOrDefault().nacionalidadPersonaNatural +
                    ", " + cliente.FirstOrDefault().estadoCivilPersonaNatural +
                    ", " + cliente.FirstOrDefault().profesionPersonaNatural
                    , options
                );
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                //doc.Range.Replace("#RutClientePalabras#", cliente.FirstOrDefault().rutPalabra, options);
                //doc.Range.Replace("#RutRepresentante1#", representante1.FirstOrDefault().rutPalabra, options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#FechaContratoMarco#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().nombreNotario + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                var datosRepresentantes = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre +
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#RutRepresentante1#",
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y por " + representante2.FirstOrDefault().nombre +
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#RutRepresentante2#",
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        //FindAndReplace(wordApp, "#TodosDomiciliados#", "todos domiciliados");
                        doc.Range.Replace("#ambos#", " ambos ", options);
                        doc.Range.Replace("#la/sFirma/s#", " las Firmas ", options);
                        doc.Range.Replace("#y#", " y ", options);
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        //FindAndReplace(wordApp, "#TodosDomiciliados#", "domiciliado");
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                        doc.Range.Replace("#y#", "", options);
                        doc.Range.Replace("#ambos#", "", options);
                        doc.Range.Replace("#la/sFirma/s#", " la Firma ", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#RutRepresentante2#", "", options);
                    }
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);

                }


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper(), options);
                    //if (!string.IsNullOrEmpty(repP.estadoCivil)) datosRepresentantePenta += ", " + repP.estadoCivil;
                    //if (!string.IsNullOrEmpty(repP.profesion)) datosRepresentantePenta += ", " + repP.profesion;
                    //if (!string.IsNullOrEmpty(repP.nacionalidad)) datosRepresentantePenta += ", " + repP.nacionalidad;
                    //if (!string.IsNullOrEmpty(repP.rutPalabras)) datosRepresentantePenta += ",  cédula de identidad número " + repP.rutPalabras;
                    datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            ", y por " +
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper(), options);

                        datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);

                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", "", options);

                    }
                }

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


                /* DocumentBuilder builder = new DocumentBuilder(doc);
                 builder.MoveToBookmark("ListadoDocumentos");
                 builder.ParagraphFormat.ClearFormatting();
                 builder.StartTable();
                 builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                 builder.CellFormat.VerticalAlignment = CellVerticalAlignment.Center;
                 //builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(70);
                 //builder.CellFormat.Width = 100;

                 builder.CellFormat.HorizontalMerge = CellMerge.None;
                 builder.InsertCell();
                 builder.CellFormat.Shading.BackgroundPatternColor = Color.FromArgb(255, 192, 0);//(255,12,1);
                 builder.Write("BENEFICIARIO");
                 builder.InsertCell();
                 builder.Write("RUT");
                 builder.InsertCell();
                 builder.Write("CERTIFICADO");
                 builder.InsertCell();
                 builder.Write("MONTO");
                 builder.EndRow();

                 int x = 0;
                 foreach (OperacionDetalleDTO operacionD in operacionDetalle)
                 {
                     x += 1;
                     builder.InsertCell();
                     builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(41.7);
                     //builder.Write(x.ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Center;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(17.6);
                     //builder.Write(operacionD.fechaVcto.ToShortDateString().ToString());
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(19.2);
                     //builder.Write(string.Format("{0:N2}", operacionD.valorCesion));
                     builder.Write(string.Format("{0:N0}", operacionD.numDocto));
                     builder.InsertCell();
                     builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                     builder.CellFormat.PreferredWidth = PreferredWidth.FromPercent(21.3);
                     builder.Write(string.Format("{0:N2}", operacionD.valorDoctoEnMoneda));
                     builder.EndRow();
                 }
                 builder.EndTable();
                 */

                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }

        public string dsN49DocPrivadoCreditoENCURSE(string formato, int rutCliente, int idSimulaOperacion, int? rutR1, int? rutR2)
        {
            string resultado = "";

            try
            {
                IEnumerable<ClienteDTO> cliente = new List<ClienteDTO>();
                cliente = gestorDeContratos.obtenerCliente(rutCliente);

                IEnumerable<RepresentanteClienteDTO> representante1 = new List<RepresentanteClienteDTO>();
                representante1 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR1);

                IEnumerable<RepresentanteClienteDTO> representante2 = new List<RepresentanteClienteDTO>();
                representante2 = gestorDeContratos.obtenerRepresentanteCliente(rutCliente, rutR2);

                IEnumerable<OperacionResumenDTO> operacionResumen = new List<OperacionResumenDTO>();
                operacionResumen = gestorDeContratos.obtenerOperacionResumen(idSimulaOperacion);

                IEnumerable<OperacionDetalleDTO> operacionDetalle = new List<OperacionDetalleDTO>();
                operacionDetalle = gestorDeContratos.obtenerOperacionDetalle(idSimulaOperacion);

                IEnumerable<SucursalDTO> sucursal = new List<SucursalDTO>();
                sucursal = gestorDeContratos.obtenerSucursal(cliente.FirstOrDefault().idSucursal);

                IEnumerable<NotariaDTO> notario = new List<NotariaDTO>();
                notario = gestorDeContratos.obtenerNotaria(cliente.FirstOrDefault().idNotaria);

                IEnumerable<RepresentantePentaDTO> representantePenta1 = new List<RepresentantePentaDTO>();
                representantePenta1 = gestorDeContratos.obtenerRepresentantePenta(sucursal.FirstOrDefault().rutRepresentantePenta1);

                IEnumerable<RepresentantePentaDTO> representantePenta2 = new List<RepresentantePentaDTO>();
                if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                {
                    representantePenta2 = gestorDeContratos.obtenerRepresentantePenta((int)sucursal.FirstOrDefault().rutRepresentantePenta2);
                }


                if (rutR1 > 0)
                {
                    if (representante1.FirstOrDefault().idEstadoCivil == 1 &&
                        (string.IsNullOrEmpty(representante1.FirstOrDefault().rutConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().dvConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().nombreConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().nacionalidadConyuge.ToString())
                        || string.IsNullOrEmpty(representante1.FirstOrDefault().profesionConyuge.ToString())
                        )
                    )
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante1.FirstOrDefault().nombre.ToUpper() + "</strong> es casado y faltan datos de su conyuge!</p>");
                    }
                }
                if (rutR2 > 0)
                {
                    if (representante2.FirstOrDefault().idEstadoCivil == 1 &&
                        (string.IsNullOrEmpty(representante2.FirstOrDefault().rutConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().dvConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().nombreConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().nacionalidadConyuge.ToString())
                        || string.IsNullOrEmpty(representante2.FirstOrDefault().profesionConyuge.ToString())
                        )
                    )
                    {
                        resultado += WebUtility.HtmlDecode("<p>El representante <strong>" + representante2.FirstOrDefault().nombre.ToUpper() + "</strong> es casado y faltan datos de su conyuge!</p>");
                    }
                }
                if (resultado.Length > 0)
                {
                    return resultado;
                }


                string plantilla = "";

                //Si el cliente es persona natural
                if (cliente.FirstOrDefault().esPersonaNatural)
                {
                    plantilla = "DS.N49.MandatoEspecialIrrevocableyContratoCesionCreditos.DocPrivado.Natural.docx";
                }
                else
                {
                    plantilla = "DS.N49.MandatoEspecialIrrevocableyContratoCesionCreditos.DocPrivado.Juridica.docx";
                }

                string dirPlantillas = ConfigurationManager.AppSettings["plantillasContratos"];
                string dirDocumentosTemp = Server.MapPath(@"~/Plantillas/TempFiles/").ToString();

                string fileName = (dirPlantillas + plantilla);

                FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                Aspose.Words.Document doc = new Aspose.Words.Document(file);
                FindReplaceOptions options = new FindReplaceOptions(FindReplaceDirection.Forward);

                doc.Range.Replace("#Sucursal#", sucursal.FirstOrDefault().nombre, options);
                doc.Range.Replace("#DomicilioSucursal#", sucursal.FirstOrDefault().direccion + ", comuna de " + sucursal.FirstOrDefault().comuna + ", ciudad de " + sucursal.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#NOMBRECLIENTE#", cliente.FirstOrDefault().razonSocial.ToUpper(), options);
                doc.Range.Replace("#RutCliente#", string.Format("{0:N0}", cliente.FirstOrDefault().rut) + "-" + cliente.FirstOrDefault().dv.ToUpper(), options);
                doc.Range.Replace("#DomicilioCliente#", cliente.FirstOrDefault().nombreCalle + " " + cliente.FirstOrDefault().numeroCalle + " " + cliente.FirstOrDefault().letraCalle + ", comuna de " + cliente.FirstOrDefault().comuna + ", ciudad de " + cliente.FirstOrDefault().ciudad, options);
                doc.Range.Replace("#FechaContratoMarco#", ((DateTime)cliente.FirstOrDefault().fechaFirmaContrato).ToString("dd' de 'MMMM' de 'yyyy"), options);
                doc.Range.Replace("#NotariaSucursal#", notario.FirstOrDefault().nombreNotario + " de " + notario.FirstOrDefault().ciudadNotaria, options);


                var datosRepresentantes = "";
                if (rutR1 > 0)
                {
                    datosRepresentantes += representante1.FirstOrDefault().nombre +
                        ", " + representante1.FirstOrDefault().nacionalidad +
                        ", " + representante1.FirstOrDefault().estadoCivil +
                        ", " + representante1.FirstOrDefault().profesion +
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper();
                    doc.Range.Replace("#NombreRepresentante1#", representante1.FirstOrDefault().nombre.ToUpper(), options);
                    doc.Range.Replace("#RutRepresentante1#",
                        ", C.I. N° " + string.Format("{0:N0}", representante1.FirstOrDefault().rutRepresentante) + "-" + representante1.FirstOrDefault().dvRepresentante.ToUpper()
                        , options
                    );

                    if (rutR2 > 0)
                    {
                        datosRepresentantes += ", y por " + representante2.FirstOrDefault().nombre +
                            ", " + representante2.FirstOrDefault().nacionalidad +
                            ", " + representante2.FirstOrDefault().estadoCivil +
                            ", " + representante2.FirstOrDefault().profesion +
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper();
                        doc.Range.Replace("#NombreRepresentante2#", representante2.FirstOrDefault().nombre.ToUpper(), options);
                        doc.Range.Replace("#RutRepresentante2#",
                            ", C.I. N° " + string.Format("{0:N0}", representante2.FirstOrDefault().rutRepresentante) + "-" + representante2.FirstOrDefault().dvRepresentante.ToUpper()
                            , options
                        );
                        
                        doc.Range.Replace("#ambos#", " ambos ", options);
                        doc.Range.Replace("#y#", " y ", options);
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "Las personerías de los repesentantes", options);
                        doc.Range.Replace("#la/sFirma/s#", " las Firmas ", options);
                        doc.Range.Replace("#FechaPersoneria#",
                            ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy") +
                            " y de " +
                            ((DateTime)representante2.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy")
                            , options
                        );
                        doc.Range.Replace("#NotariaPersoneria#",
                            representante1.FirstOrDefault().notariaPersoneria +
                            ", y " + representante2.FirstOrDefault().notariaPersoneria +
                            " respectivamente"
                            , options
                        );
                        doc.Range.Replace("#otorgada/s#", "otorgadas", options);
                    }
                    else
                    {
                        doc.Range.Replace("#La/sPersonería/sDe/l/los/Repesentantes#", "La personería del repesentante", options);
                        doc.Range.Replace("#FechaPersoneria#", ((DateTime)representante1.FirstOrDefault().fechaPersoneria).ToString("dd' de 'MMMM' de 'yyyy"), options);
                        doc.Range.Replace("#NotariaPersoneria#", representante1.FirstOrDefault().notariaPersoneria, options);
                        doc.Range.Replace("#otorgada/s#", "otorgada", options);
                        doc.Range.Replace("#y#", "", options);
                        doc.Range.Replace("#ambos#", "", options);
                        doc.Range.Replace("#la/sFirma/s#", " la Firma ", options);
                        doc.Range.Replace("#NombreRepresentante2#", "", options);
                        doc.Range.Replace("#RutRepresentante2#", "", options);
                    }
                    doc.Range.Replace("#DatosRepresentantes#", datosRepresentantes, options);

                }


                //Datos RepresentantePenta 1
                using (var repP = representantePenta1.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    doc.Range.Replace("#NOMBREREPRESENTANTEPENTA1#",
                        repP.nombres.ToUpper() +
                        " " + repP.apPaterno.ToUpper() +
                        " " + repP.apMaterno.ToUpper(), options);
                    datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                    doc.Range.Replace("#DatosRepresentantePenta1#", datosRepresentantePenta, options);
                }

                //Datos RepresentantePenta 2
                using (var repP = representantePenta2.FirstOrDefault())
                {
                    var datosRepresentantePenta = "";

                    if (sucursal.FirstOrDefault().rutRepresentantePenta2 > 0)
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#",
                            ", y por " +
                            repP.nombres.ToUpper() +
                            " " + repP.apPaterno.ToUpper() +
                            " " + repP.apMaterno.ToUpper(), options);

                        datosRepresentantePenta += ", C.N.I. N° " + string.Format("{0:N0}", repP.rut) + "-" + repP.dv.ToUpper();
                        doc.Range.Replace("#DatosRepresentantePenta2#", datosRepresentantePenta, options);
                    }
                    else
                    {
                        doc.Range.Replace("#NOMBREREPRESENTANTEPENTA2#", "", options);
                        doc.Range.Replace("#DatosRepresentantePenta2#", "", options);
                    }
                }

                using (var operacionR = operacionResumen.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaOperacion#", operacionR.fechaOtorgamiento.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#MONTODOCUMENTOS#", string.Format("{0:N2}", operacionR.ValDoctoEnMoneda), options);
                    doc.Range.Replace("#TOTALOPERACION–DIFERENCIADEPRECIO#", string.Format("{0:N2}", operacionR.valorCesion), options);
                    doc.Range.Replace("#AGirarEjec#", string.Format("{0:N2}", operacionR.AGirarEjec), options);
                    doc.Range.Replace("#SaldoPendiente#", string.Format("{0:N2}", operacionR.saldoPendiente), options);
                }

                using (var operacionD = operacionDetalle.FirstOrDefault())
                {
                    doc.Range.Replace("#FechaVencimientoOperacion#", operacionD.fechaVcto.ToString("dd' de 'MMMM' de 'yyyy"), options);
                    doc.Range.Replace("#NumeroTotalDocumentos#", string.Format("{0:N0}", operacionDetalle.Count()), options);

                }


               
                string fileSaveAs = rutCliente + "_" + System.Reflection.MethodBase.GetCurrentMethod().Name;
                string fileSaveAsPath = "";
                int totalPaginasDocumento = 0;
                if (formato.ToUpper() == "WORD")
                {
                    fileSaveAs = fileSaveAs + ".docx";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/ms-word";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else if (formato.ToUpper() == "PDF")
                {
                    fileSaveAs = fileSaveAs + ".pdf";
                    fileSaveAsPath = dirDocumentosTemp + fileSaveAs;

                    doc.Save(fileSaveAsPath);

                    Response.Clear();
                    Response.ContentType = "Application/pdf";
                    Response.AppendHeader("Content-Disposition", "inline; filename=" + fileSaveAs);
                    Response.TransmitFile(fileSaveAsPath);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    resultado = "FORMATO NO HABILITADO";
                }

                //Se registra contrato en la tabla log
                LogContratoController logC = new LogContratoController();
                logC.LogContrato_Agregar("", rutCliente, System.Reflection.MethodBase.GetCurrentMethod().Name + " (" + formato.ToUpper() + ")");

                file.Close();
            }
            catch (Exception ex)
            {
                resultado = ex.ToString();
            }

            return resultado;
        }
    }
}
