﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using GestorDeContratos.ServiceLayer.ViewsDTOs;
using GestorDeContratos.WebService.Clases;


namespace GestorDeContratos.WebService
{
    /// <summary>
    /// Descripción breve de WSGenerarContrato
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WSGenerarContrato : System.Web.Services.WebService
    {


        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        
        [WebMethod(Description = "Generar Contrato")]
        
        public GenerarContrato GenerarContratoPDF(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            
            GenerarContrato obtenerC = new GenerarContrato(datosContrato, cliente, firmante, datosOperacion);
            return obtenerC;
           
        }

        [WebMethod(Description = "Marcar Contrato Como Firmado")]
        public FirmarContrato FirmarContratoGestor(int idInstancia)
        {
            FirmarContrato contratoFirmado = new FirmarContrato(idInstancia);
            return contratoFirmado;
        }
         
        
        public class GenerarContrato
        {
            public int codigoResultado = 0;
            public string descripcionResultado = "";
            public GestorContratos contrato = new GestorContratos();

            public GenerarContrato() { }

            public GenerarContrato(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
            {
                try
                {
                    if (datosContrato.idContrato == 7)
                    {
                        if (this.contrato.ValidarOperacion(datosOperacion.idSimulaOper))
                        {
                            if (this.contrato.GenerarContratoPDF(datosContrato, cliente, firmante, datosOperacion))
                                descripcionResultado = "REGISTRO ENCONTRADO";
                            else
                            {
                                codigoResultado = 1;
                                descripcionResultado = "REGISTRO NO ENCONTRADO";
                            }
                        }
                        else
                        {
                            codigoResultado = 1;
                            descripcionResultado = "LA OPERACIÓN NO EXISTE EN GVE";
                        }
                    }
                    else
                    {
                        if (this.contrato.GenerarContratoPDF(datosContrato, cliente, firmante, datosOperacion))
                            descripcionResultado = "REGISTRO ENCONTRADO";
                        else
                        {
                            codigoResultado = 1;
                            descripcionResultado = "REGISTRO NO ENCONTRADO";
                        }
                    }
                }
                catch (Exception ex)
                {
                    codigoResultado = 1;
                    descripcionResultado = ex.Message;
                    if (ex.Source != null)
                        descripcionResultado += "(IOException source: " + ex.Source + ")";
                }
            }
        }

        public class FirmarContrato
        {
            public int codigoResultado = 0;
            public string descripcionResultado = "";

            FirmaDeContrato contrato = new FirmaDeContrato();
     

            public FirmarContrato() { }

            public FirmarContrato(int idInstancia)
            {
                try
                {
                   
                    if (this.contrato.MarcarContratoFirmado(idInstancia))
                        descripcionResultado = "CONTRATO FIRMADO CORRECTAMENTE";
                    else
                    {
                        codigoResultado = 1;
                        descripcionResultado = "SE PRODUJO UN ERROR AL INTENTAR MARCAR EL CONTRATO COMO FIRMADO";
                    }

                    
                }
                catch (Exception ex)
                {
                    codigoResultado = 1;
                    descripcionResultado = ex.Message;
                    if (ex.Source != null)
                        descripcionResultado += "(IOException source: " + ex.Source + ")";
                }
            }

        }
        
    }
}
