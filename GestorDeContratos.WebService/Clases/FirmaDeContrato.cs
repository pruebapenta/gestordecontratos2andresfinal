﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GestorDeContratos.Web.Controllers;

namespace GestorDeContratos.WebService.Clases
{
    public class FirmaDeContrato
    {

        public bool MarcarContratoFirmado(int idInstancia)
        {
            bool ejecutado = false;
            try
            {

                string respuesta;
                ContratosWSController controladorContrato = new ContratosWSController();
                respuesta = controladorContrato.MarcarContratoFirmado(idInstancia);

                ejecutado = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ejecutado;
        }
        
    }
}