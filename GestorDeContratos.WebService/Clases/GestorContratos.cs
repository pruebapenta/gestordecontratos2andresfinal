﻿
namespace GestorDeContratos.WebService.Clases
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using GestorDeContratos.ServiceLayer.ViewsDTOs;
    using GestorDeContratos.Web.Controllers;



    // TODO: cree métodos que incluyan la lógica de su aplicación.
    
    public class GestorContratos 
    {
        public GestorDeContratos.WebService.Clases.Contrato contrato;

        public bool ValidarOperacion(int idSimulaOper)
        {
            bool ejecutado = false;
            try
            {
                ContratosWSController controladorContrato = new ContratosWSController();

                ejecutado = controladorContrato.ValidarOperacion(idSimulaOper);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ejecutado;

        }

        public bool GenerarContratoPDF(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            bool ejecutado = false;
            try
            {
               
                contrato = new GestorDeContratos.WebService.Clases.Contrato();
               

                switch (datosContrato.idContrato)
                {
                    case 1: //Set Legal NoPep
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = SetLegalNoPep(datosContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    case 2: //Set Legal SiPep
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = SetLegalSiPep(datosContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    case 3: //contrato marco inst privado con resp
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoMarcoInstPrivConResp(datosContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    case 4: //contrato marco inst privado sin resp
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoMarcoInstPrivSinResp(datosContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    case 5:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoMarcoSinResponsabilidad(datosContrato.idContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    case 6:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoCesionFirmaFisica(datosContrato.idContrato, cliente, firmante, datosOperacion, "pdf");
                        ejecutado = true;
                    break;
                    case 7:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoCesionFirmaElectronica(datosContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    case 8:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoCesionCobranzaDelegada(datosContrato.idContrato, cliente, firmante, datosOperacion, "pdf");
                        ejecutado = true;
                    break;
                    case 9:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = CartaDepositoTercero(datosContrato.idContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    case 10:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        Tuple<bool, string> resultado = FichaAvalistaFirmaFisica(datosContrato.idContrato, cliente, firmante, datosOperacion);
                        this.contrato.contratoBase64 = resultado.Item2;
                        ejecutado = resultado.Item1;
                    break;
                    case 11:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        Tuple<bool, string> resultadoC = FichaAvalistaFirmaElectronica(datosContrato.idContrato, cliente, firmante, datosOperacion);
                        this.contrato.contratoBase64 = resultadoC.Item2;
                        ejecutado = resultadoC.Item1;
                    break;
                    case 12:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = FichaPEPPositivo(datosContrato.idContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    case 13:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = CartaGuia(datosContrato.idContrato);
                        ejecutado = true;
                    break;
                    case 14:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = FichaBeneficiarioFinal(datosContrato.idContrato);
                        ejecutado = true;
                    break;
                    case 16:
                        datosContrato.idContrato = 6;
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoCesionFirmaFisica(datosContrato.idContrato, cliente, firmante, datosOperacion, "docx");
                        ejecutado = true;
                    break;
                    case 17:
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoCesionCobranzaDelegada(datosContrato.idContrato, cliente, firmante, datosOperacion, "docx");
                        ejecutado = true;
                    break;
                    case 18: //contrato factoring con responsabilidad
                        this.contrato = new GestorDeContratos.WebService.Clases.Contrato();
                        this.contrato.contratoBase64 = ContratoFactoringConResponsabilidad(datosContrato.idContrato, cliente, firmante, datosOperacion);
                        ejecutado = true;
                    break;
                    default:
                        this.contrato.contratoBase64 = "Documento no disponible";
                        ejecutado = false;
                    break;

                }
                
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ejecutado;
        }

        public string SetLegalNoPep(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.SetLegalNoPep(datosContrato, cliente, firmante, datosOperacion);

            return Contrato;
        }

        public string SetLegalSiPep(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.SetLegalSiPep(datosContrato, cliente, firmante, datosOperacion);

            return Contrato;
        }

        public string ContratoMarcoInstPrivConResp(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.ContratoMarcoInstPrivConResp(datosContrato, cliente, firmante, datosOperacion);

            return Contrato;
        }

        public string ContratoMarcoInstPrivSinResp(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.ContratoMarcoInstPrivSinResp(datosContrato, cliente, firmante, datosOperacion);
            return Contrato;
        }

        public string ContratoMarcoSinResponsabilidad(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.ContratoMarcoSinResponsabilidad(idContrato, cliente, firmante, datosOperacion);
            return Contrato;
        }

        public string ContratoCesionFirmaFisica(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion, string formato)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.ContratoCesionFirmaFisica(idContrato, cliente, firmante, datosOperacion, formato);
            return Contrato;
        }

        public string ContratoCesionFirmaElectronica(DatosContratoWSDTO datosContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.ContratoCesionFirmaElectronica(datosContrato, cliente, firmante, datosOperacion);
            return Contrato;
        }

        public string ContratoCesionCobranzaDelegada(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion, string formato)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.ContratoCesionCobrazaDelegada(idContrato, cliente, firmante, datosOperacion, formato);
            return Contrato;
        }

        public string CartaDepositoTercero(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.CartaDepositoTercero(idContrato, cliente, firmante, datosOperacion);
            return Contrato;
        }

        public Tuple<bool,string> FichaAvalistaFirmaFisica(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            bool ejecutado;
            ContratosWSController controladorContrato = new ContratosWSController();
            Tuple<bool,string> respuesta = controladorContrato.FichaAvalistaFirmaFisica(idContrato, cliente, firmante, datosOperacion);
            ejecutado = respuesta.Item1;
            Contrato = respuesta.Item2;
            return Tuple.Create(ejecutado,Contrato);
        }
        public Tuple<bool, string> FichaAvalistaFirmaElectronica(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            bool ejecutado;
            ContratosWSController controladorContrato = new ContratosWSController();
            Tuple<bool, string> respuesta = controladorContrato.FichaAvalistaFirmaElectronica(idContrato, cliente, firmante, datosOperacion);
            ejecutado = respuesta.Item1;
            Contrato = respuesta.Item2;
            return Tuple.Create(ejecutado, Contrato);
        }
        public string FichaPEPPositivo(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.FichaPEPPositivo(idContrato, cliente, firmante, datosOperacion);
            return Contrato;
        }

        public string CartaGuia(int idContrato)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.CartaGuia(idContrato);
            return Contrato;
        }
        public string FichaBeneficiarioFinal(int idContrato)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.FichaBeneficiarioFinal(idContrato);
            return Contrato;
        }

        public string ContratoFactoringConResponsabilidad(int idContrato, ClienteWSDTO cliente, FirmanteWSDTO firmante, DatosOperacionWSDTO datosOperacion)
        {
            string Contrato;
            ContratosWSController controladorContrato = new ContratosWSController();
            Contrato = controladorContrato.ContratoFactoringConResponsabilidad(idContrato, cliente, firmante, datosOperacion);

            return Contrato;
        }
    }
}


